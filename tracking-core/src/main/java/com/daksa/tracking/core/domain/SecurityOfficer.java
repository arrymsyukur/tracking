package com.daksa.tracking.core.domain;

import com.daksa.tracking.core.model.SecurityOfficerModel;
import com.daksa.tracking.webutil.json.JsonDateAdapter;
import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@Table(schema = "public", name = "security_officer")
@Entity
@NamedQueries({
    @NamedQuery(name = "SecurityOfficer.findBySsoUserId", query = "SELECT e FROM SecurityOfficer e WHERE e.ssoUser.id = :ssoUserId")
    ,
    @NamedQuery(name = "SecurityOfficer.findByName", query = "SELECT e FROM SecurityOfficer e WHERE e.name = :employeeName")
})
public class SecurityOfficer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(length = 32)
    private String id;

    @Column(length = 64, nullable = false)
    private String name;

    @XmlTransient
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "officer_type", nullable = false)
    private OfficerType officerType;

    @Column(name = "officer_type", insertable = false, updatable = false)
    private String officerTypeId;

    @OneToOne(optional = false)
    @JoinColumn(name = "sso_user_id", nullable = false)
    @XmlTransient
    private MobileSsoUser ssoUser;

    @Column(name = "sso_user_id", insertable = false, updatable = false)
    private String ssoUserId;

    @Temporal(TemporalType.DATE)
    @XmlJavaTypeAdapter(JsonDateAdapter.class)
    @Column(name = "birth_date", nullable = false)
    private Date birthDate;

    @Enumerated(EnumType.STRING)
    @Column
    private UserStatus status;

    @Column(name = "salary")
    private BigDecimal salary;

    public SecurityOfficer() {
        this.id = IDGen.generate();
        this.status = UserStatus.PENDING;
        this.salary = BigDecimal.ZERO;
    }

    public SecurityOfficer(SecurityOfficerModel model) {
        this();
        this.name = model.getName();
        this.birthDate = model.getBirthDate();
        this.salary = model.getSalary();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MobileSsoUser getSsoUser() {
        return ssoUser;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public UserStatus getStatus() {
        return status;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSsoUser(MobileSsoUser ssoUser) {
        this.ssoUser = ssoUser;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public String getUsername() {
        return ssoUserId;
    }

    public void setUsername(String username) {
        this.ssoUserId = username;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecurityOfficer)) {
            return false;
        }
        SecurityOfficer other = (SecurityOfficer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public String toString() {
        return "com.daksa.tracking.core.domain.Employee[ id=" + id + " ]";
    }

    public OfficerType getOfficerType() {
        return officerType;
    }

    public void setOfficerType(OfficerType officerType) {
        this.officerType = officerType;
        this.officerTypeId = officerType.getId();
    }

    public String getOfficerTypeId() {
        return officerTypeId;
    }

    public void setOfficerTypeId(String officerTypeId) {
        this.officerTypeId = officerTypeId;
    }

}
