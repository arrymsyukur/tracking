package com.daksa.tracking.core.domain;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@NamedQueries({@NamedQuery(name = "Shift.findByDateShift",query = "SELECT s FROM Shift s WHERE s.branchId = :branchId AND s.startDateShift >= :checkTime AND s.endDateShift <= :checkTime")})

@Table(name = "shift", schema = "public")
public class Shift implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 32)
    private String id;

    @Column(name = "start_date_shift")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date startDateShift;

    @Column(name = "end_date_shift")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date endDateShift;

    @XmlTransient
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(columnDefinition = "branch_id")
    private Branch branch;

    @Column(name = "branch_id", insertable = false, updatable = false)
    private String branchId;

    public Shift() {
        this.id = IDGen.generate();
    }

    public Shift(Date startDateShift, Date endDateShift, Branch branch) {
        this();
        this.startDateShift = startDateShift;
        this.endDateShift = endDateShift;
        this.branch = branch;
        this.branchId = branch.getId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartDateShift() {
        return startDateShift;
    }

    public void setStartDateShift(Date startDateShift) {
        this.startDateShift = startDateShift;
    }

    public Date getEndDateShift() {
        return endDateShift;
    }

    public void setEndDateShift(Date endDateShift) {
        this.endDateShift = endDateShift;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

}
