package com.daksa.tracking.core.repository;

import com.daksa.tracking.core.domain.SecurityOfficer;
import com.daksa.tracking.core.domain.ShiftOfficer;
import com.daksa.tracking.core.model.OfficerLoginModel;
import com.daksa.tracking.webutil.persistence.DataTable;
import com.daksa.tracking.webutil.persistence.FilterGenerator;
import com.daksa.tracking.webutil.persistence.FilterItem;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class ShiftOfficerRepository extends JpaRepository<ShiftOfficer, String> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(ShiftOfficerRepository.class);

    @Inject
    private EntityManager entityManager;
    private DataTable<ShiftOfficer> dataTable;

    public ShiftOfficerRepository() {
	super(ShiftOfficer.class);
    }

    @PostConstruct
    public void init() {
	dataTable = new DataTable<>(entityManager, ShiftOfficer.class);
    }

    public TableResult<ShiftOfficer> findByFilters(TableParam param) {
	return dataTable.load(param, new FilterGenerator() {
	    @Override
	    public FilterItem createFilterItem(String fieldName, Object valueStr) {
		FilterItem filterItem = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		switch (fieldName) {
		    case "startDate": {
			try {
			    filterItem = new FilterItem("checkingTimestamp", ">=", sdf.parse(valueStr.toString()));
			} catch (ParseException ex) {
			    filterItem = new FilterItem("checkingTimestamp", ">=", new Date());
			}
		    }
		    break;
		    case "endDate": {
			try {
			    filterItem = new FilterItem("checkingTimestamp", "<=", sdf.parse(valueStr.toString()));
			} catch (ParseException ex) {
			    filterItem = new FilterItem("checkingTimestamp", "<=", new Date());
			}
		    }
		    break;
		    case "checked": {
			filterItem = FilterItem.booleanFilter(fieldName, valueStr);
		    }
		    break;
		}
		return filterItem;
	    }

	});
    }

    public ShiftOfficer findByOfficerIdandShiftId(String officerId, String shiftId) {
	try {
	    TypedQuery query = entityManager.createNamedQuery("ShiftOfficer.findByofficerIdandShiftId", ShiftOfficer.class);
	    query.setParameter("officerId", officerId);
	    query.setParameter("shiftId", shiftId);
	    query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
	    return (ShiftOfficer) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}
    }

    public TableResult<SecurityOfficer> findByShiftId(String shiftId) {
	try {
	    TypedQuery query = entityManager.createNamedQuery("ShiftOfficer.findByShiftId", ShiftOfficer.class);
	    query.setParameter("shiftId", shiftId);
	    List<ShiftOfficer> shiftOfficers = query.getResultList();
	    List<SecurityOfficer> securityOfficers = new ArrayList<>();
	    for (ShiftOfficer shiftOfficer : shiftOfficers) {
		securityOfficers.add(shiftOfficer.getSecurityOfficer());
	    }
	    return new TableResult<>(securityOfficers, Long.valueOf(securityOfficers.size()));
	} catch (NoResultException e) {
	    return new TableResult<>();
	}
    }

    @Override
    public EntityManager getEntityManager() {
	return entityManager;
    }

    @Transactional
    public void deleteShiftOfficer(String officerId, String shiftId) {
	ShiftOfficer shiftOfficer = findByOfficerIdandShiftId(officerId, shiftId);
	delete(shiftOfficer);
    }
}
