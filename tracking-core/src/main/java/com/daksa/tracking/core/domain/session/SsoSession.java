package com.daksa.tracking.core.domain.session;


import com.daksa.tracking.core.domain.MobileSsoUser;
import com.daksa.tracking.webutil.json.Json;
import com.daksa.tracking.webutil.util.IDGen;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@Table(schema = "portal", name = "sso_session")
@NamedQueries({
	@NamedQuery(name = "BO.SsoSession.invalidateExpiredSession", query = "DELETE FROM SsoSession s WHERE s.expiryTimestamp <= :timestamp")
})
@Entity
public class SsoSession implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 32)
	private String id;

	@XmlTransient
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "sso_user_id", nullable = false)
	private MobileSsoUser ssoUser;

	@Column(name = "sso_user_id", insertable = false, updatable = false)
	private String ssoUserId;
	
	@Column(name = "username", length = 64)
	private String username;
	
	@Column(name = "full_name", length = 64)
	private String fullName;

	@Column(name = "email", length = 64)
	private String email;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp", nullable = false)
	private Date createdTimestamp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_access_timestamp", nullable = false)
	private Date lastAccessTimestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expiry_timestamp", nullable = false)
	private Date expiryTimestamp;
	
	@Column(name = "expiry_minutes")
	private int expiryMinutes;
	
	@Column(name = "session_data", columnDefinition = "TEXT")
	private String sessionData;

	protected SsoSession() {
	}

	public SsoSession(MobileSsoUser ssoUser, Date createdTimestamp, int expiryMinutes) {
		this.id = IDGen.generate();
		this.ssoUser = ssoUser;
		this.ssoUserId = ssoUser.getId();
		this.username = ssoUser.getUsername();
		this.fullName = ssoUser.getFullName();
		this.email = ssoUser.getEmail();
		this.createdTimestamp = createdTimestamp;
		this.lastAccessTimestamp = createdTimestamp;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(createdTimestamp);
		cal.add(Calendar.MINUTE, expiryMinutes);
		this.expiryTimestamp = cal.getTime();
		this.expiryMinutes = expiryMinutes;
	}

	public String getId() {
		return id;
	}

	public MobileSsoUser getSsoUser() {
		return ssoUser;
	}

	public String getSsoUserId() {
		return ssoUserId;
	}

	public Date getLastAccessTimestamp() {
		return lastAccessTimestamp;
	}

	public void setLastAccessTimestamp(Date lastAccessTimestamp) {
		this.lastAccessTimestamp = lastAccessTimestamp;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(lastAccessTimestamp);
		cal.add(Calendar.MINUTE, expiryMinutes);
		this.expiryTimestamp = cal.getTime();
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public Date getExpiryTimestamp() {
		return expiryTimestamp;
	}

	public int getExpiryMinutes() {
		return expiryMinutes;
	}
	
	public boolean isExpiry(Date now) {
		return expiryTimestamp.getTime() <= now.getTime();
	}

	public void setSessionData(SessionData sessionData) throws JsonProcessingException {
		this.sessionData = Json.getWriter().writeValueAsString(sessionData);
	}

	public SessionData getSessionData() throws IOException {
		if (sessionData != null) {
			return Json.getReader()
					.forType(SessionData.class)
					.readValue(sessionData);
		} else {
			return null;
		}
	}	

	public String getUsername() {
		return username;
	}

	public String getFullName() {
		return fullName;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 79 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SsoSession other = (SsoSession) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

}
