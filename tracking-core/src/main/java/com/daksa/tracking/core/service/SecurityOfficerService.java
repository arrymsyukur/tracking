package com.daksa.tracking.core.service;

import com.daksa.tracking.core.domain.EmployeeTrackingLog;
import com.daksa.tracking.core.domain.SecurityOfficer;
import com.daksa.tracking.core.domain.OfficerType;
import com.daksa.tracking.core.domain.Shift;
import com.daksa.tracking.core.domain.ShiftOfficer;
import com.daksa.tracking.core.domain.MobileSsoUser;
import com.daksa.tracking.core.domain.UserStatus;
import com.daksa.tracking.core.model.OfficerLoginModel;
import com.daksa.tracking.core.model.SecurityOfficerModel;
import com.daksa.tracking.core.model.ShiftAssignModel;
import com.daksa.tracking.core.repository.SecurityOfficerRepository;
import com.daksa.tracking.core.repository.OfficerTypeRepository;
import com.daksa.tracking.core.repository.ShiftOfficerRepository;
import com.daksa.tracking.core.repository.ShiftRepository;
import com.daksa.tracking.core.repository.MobileSsoUserRepository;
import com.daksa.tracking.webutil.persistence.TableResult;
import com.daksa.tracking.webutil.rest.RestException;
import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Stateless
public class SecurityOfficerService {

    private static final Logger LOG = LoggerFactory.getLogger(SecurityOfficerService.class);
    @Inject
    private SecurityOfficerRepository securityOfficerRepository;
    @Inject
    private OfficerTypeRepository officerTypeRepository;
    @Inject
    private MobileSsoUserRepository ssoUserRepository;
    @Inject
    private ShiftRepository shiftRepository;
    @Inject
    private ShiftOfficerRepository shiftOfficerRepository;

    public SecurityOfficer registerEmployee(SecurityOfficerModel model) throws RestException {
	OfficerType officerType = officerTypeRepository.find(model.getOfficerTypeId());
	if (officerType == null) {
	    throw new RestException("Employee Type Not Found");
	}
	SecurityOfficer employee = new SecurityOfficer(model);

	MobileSsoUser ssoUser = ssoUserRepository.findByUsername(model.getUsername());
	if (ssoUser != null) {
	    throw new RestException("Username already exist");
	}

	ssoUser = ssoUserRepository.findByEmail(model.getUsername());
	if (ssoUser != null) {
	    throw new RestException("Email already exist");
	}

	ssoUser = new MobileSsoUser(model.getUsername(), model.getName(), model.getEmail(), model.getPassword(), new Date());
	ssoUser.setStatus(UserStatus.ACTIVE);

	employee.setOfficerType(officerType);
	employee.setStatus(UserStatus.ACTIVE);
	employee.setUsername(ssoUser.getUsername());
	employee.setSsoUser(ssoUser);

	ssoUserRepository.store(ssoUser);
	securityOfficerRepository.store(employee);

	LOG.info("#Register Employee Success#");
	return employee;

    }

    public boolean assignShift(ShiftAssignModel shiftAssignModel) throws RestException {
	LOG.info("#Assign Shift#");

	Shift shift = shiftRepository.find(shiftAssignModel.getShiftId());
	if (shift == null) {
	    throw new RestException("Shift Not Found");
	}
	for (String officerId : shiftAssignModel.getOfficerId()) {
	    SecurityOfficer officer = securityOfficerRepository.find(officerId);
	    if (officer == null) {
		continue;
	    }
	    ShiftOfficer shiftOfficer = shiftOfficerRepository.findByOfficerIdandShiftId(officerId, shiftAssignModel.getShiftId());
	    if (shiftOfficer == null) {
		shiftOfficer = new ShiftOfficer(officer, shift);
		shiftOfficerRepository.store(shiftOfficer);
	    }
	}
	LOG.info("#Assign Shift Success#");
	return true;
    }

    public SecurityOfficerModel findById(String id) throws RestException {
	SecurityOfficer securityOfficer = securityOfficerRepository.find(id);
	if (securityOfficer == null) {
	    throw new RestException("Security Officer Not Found");
	}
	SecurityOfficerModel securityOfficerModel = new SecurityOfficerModel(securityOfficer);
	return securityOfficerModel;
    }

    public SecurityOfficer updateSecurityOfficer(SecurityOfficerModel editedSecurityOfficer) throws RestException {
	SecurityOfficer newOfficer = securityOfficerRepository.find(editedSecurityOfficer.getId());
	if (newOfficer == null) {
	    throw new RestException("Security Officer Not Found");
	}

	if (StringUtils.isNotEmpty(editedSecurityOfficer.getName())) {
	    newOfficer.setName(editedSecurityOfficer.getName());
	}
	if (StringUtils.isNotEmpty(editedSecurityOfficer.getOfficerTypeId())) {
	    OfficerType officerType = officerTypeRepository.find(editedSecurityOfficer.getOfficerTypeId());
	    if (officerType == null) {
		throw new RestException("Officer Type Not Found");
	    }
	    newOfficer.setOfficerTypeId(officerType.getId());
	    newOfficer.setOfficerType(officerType);
	}
	if (editedSecurityOfficer.getSalary() != null) {
	    newOfficer.setSalary(editedSecurityOfficer.getSalary());
	}
	if (editedSecurityOfficer.getBirthDate() != null) {
	    newOfficer.setBirthDate(editedSecurityOfficer.getBirthDate());
	}
	if (editedSecurityOfficer.getStatus() != null) {
	    newOfficer.setStatus(editedSecurityOfficer.getStatus());
	}

	securityOfficerRepository.update(newOfficer);
	return newOfficer;
    }

    public ShiftOfficer doCheckIn(OfficerLoginModel model) throws RestException {
	Shift shift = shiftRepository.findByDateShift(model.getBranchId(), model.getCheckingTimestamp());
	if (shift == null) {
	    throw new RestException("Shift not Found");
	}
	ShiftOfficer officer = shiftOfficerRepository.findByOfficerIdandShiftId(model.getOfficerId(), shift.getId());
	if (officer == null) {
	    throw new RestException("Shift for This Officer not Found");
	}

	officer.setChecked(Boolean.TRUE);
	officer.setCheckingTimestamp(model.getCheckingTimestamp());
	shiftOfficerRepository.update(officer);

	return officer;
    }
}
