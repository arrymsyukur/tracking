package com.daksa.tracking.core.domain;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Table(schema = "public", name = "mobile_sso_user", indexes = {
    @Index(name = "mobile_sso_user_username_idx", columnList = "username", unique = true)
    ,
	@Index(name = "mobile_sso_user_full_name_idx", columnList = "full_name", unique = false)
    ,
	@Index(name = "mobile_sso_user_email_idx", columnList = "email", unique = true)
})
@NamedQueries({
    @NamedQuery(name = "MobileSsoUser.findByUsername", query = "SELECT s FROM MobileSsoUser s WHERE s.username = :username")
    ,
    @NamedQuery(name = "MobileSsoUser.findByEmail", query = "SELECT s FROM MobileSsoUser s WHERE s.email = :email")
})
@Entity
public class MobileSsoUser implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final int HASH_ITERATION = 2;
    private static final String HASH_ALGO = "SHA-256";

    @Id
    @Column(length = 32)
    private String id;

    @Column(name = "username", length = 64, nullable = false)
    private String username;

    @XmlTransient
    @Column(name = "password_hash", length = 64, nullable = false)
    private String passwordHash;

    @XmlTransient
    @Column(name = "password_salt", length = 32, nullable = false)
    private String passwordSalt;

    @Column(name = "full_name", length = 64, nullable = false)
    private String fullName;

    @Column(name = "email", length = 64, nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 16)
    private UserStatus status;

    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_timestamp")
    private Date registrationTimestamp;

    protected MobileSsoUser() {
        this.id = IDGen.generate();
    }

    public MobileSsoUser(String username,
            String fullName,
            String email,
            String password,
            Date registrationTimestamp) {
        this();
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.registrationTimestamp = registrationTimestamp;
        this.status = UserStatus.PENDING;
        setPassword(password);
    }

    public boolean verifyPassword(String password) {
        return hashPassword(password).equals(passwordHash);
    }

    private String hashPassword(String password) {
        try {
            byte[] saltBytes = Hex.decodeHex(passwordSalt.toCharArray());
            return hassPassword(password, saltBytes);
        } catch (DecoderException e) {
            throw new RuntimeException(e);
        }
    }

    private String hassPassword(String password, byte[] saltBytes) {
        try {
            byte[] passwordBytes = password.getBytes();
            MessageDigest digest = MessageDigest.getInstance(HASH_ALGO);
            digest.update(saltBytes);
            byte[] tokenHash = digest.digest(passwordBytes);
            int iterations = HASH_ITERATION - 1;
            for (int i = 0; i < iterations; i++) {
                digest.reset();
                tokenHash = digest.digest(tokenHash);
            }
            String tokenHashHex = Hex.encodeHexString(tokenHash);
            return tokenHashHex;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public final void setPassword(String password) {
        Random random = new Random();
        byte[] saltBytes = new byte[16];
        random.nextBytes(saltBytes);

        this.passwordHash = hassPassword(password, saltBytes);
        this.passwordSalt = Hex.encodeHexString(saltBytes);
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public Date getRegistrationTimestamp() {
        return registrationTimestamp;
    }
}
