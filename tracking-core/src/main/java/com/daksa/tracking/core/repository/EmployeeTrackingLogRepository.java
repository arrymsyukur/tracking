package com.daksa.tracking.core.repository;

import com.daksa.tracking.core.domain.EmployeeTrackingLog;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class EmployeeTrackingLogRepository extends JpaRepository<EmployeeTrackingLog, String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;

    public EmployeeTrackingLogRepository() {
        super(EmployeeTrackingLog.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
