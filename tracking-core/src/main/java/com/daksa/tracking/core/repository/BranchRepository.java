package com.daksa.tracking.core.repository;

import com.daksa.tracking.core.domain.Branch;
import com.daksa.tracking.webutil.persistence.DataTable;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class BranchRepository extends JpaRepository<Branch, String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;
    private DataTable<Branch> dataTable;

    public BranchRepository() {
        super(Branch.class);
    }

    @PostConstruct
    public void init() {
        dataTable = new DataTable<>(entityManager, Branch.class);
    }

    public TableResult<Branch> findByFilters(TableParam param) {
        return dataTable.load(param);
    }

    public Branch findByEmail(String email) {
        try {
            TypedQuery query = entityManager.createNamedQuery("Branch.findByEmail", Branch.class);
            query.setParameter("email", email);
            return (Branch) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
