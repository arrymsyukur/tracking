package com.daksa.tracking.core.model;

import com.daksa.tracking.core.domain.SecurityOfficer;
import com.daksa.tracking.core.domain.UserStatus;
import com.daksa.tracking.webutil.json.JsonDateAdapter;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public class SecurityOfficerModel {

    private String id;
    private String name;
    private String officerTypeId;
    @XmlJavaTypeAdapter(JsonDateAdapter.class)
    private Date birthDate;
    private BigDecimal salary;
    private String username;
    private String password;
    private String email;
    private UserStatus status;

    public SecurityOfficerModel() {
    }

    public SecurityOfficerModel(SecurityOfficer securityOfficer) {
        this.id = securityOfficer.getId();
        this.name = securityOfficer.getName();
        this.officerTypeId = securityOfficer.getOfficerTypeId();
        this.birthDate = securityOfficer.getBirthDate();
        this.salary = securityOfficer.getSalary();
        this.username = securityOfficer.getSsoUser().getUsername();
        this.email = securityOfficer.getSsoUser().getEmail();
        this.status = securityOfficer.getStatus();
    }

    public String getName() {
        return name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getOfficerTypeId() {
        return officerTypeId;
    }

    public void setOfficerTypeId(String officerTypeId) {
        this.officerTypeId = officerTypeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

}
