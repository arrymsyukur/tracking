package com.daksa.tracking.core.repository;

import com.daksa.tracking.core.domain.Shift;
import com.daksa.tracking.webutil.persistence.DataTable;
import com.daksa.tracking.webutil.persistence.FilterGenerator;
import com.daksa.tracking.webutil.persistence.FilterItem;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.apache.commons.codec.digest.Sha2Crypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class ShiftRepository extends JpaRepository<Shift, String> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(ShiftRepository.class);

    @Inject
    private EntityManager entityManager;
    private DataTable<Shift> dataTable;

    public ShiftRepository() {
	super(Shift.class);
    }

    @PostConstruct
    public void init() {
	dataTable = new DataTable<>(entityManager, Shift.class);
    }

    public TableResult<Shift> findByFilters(TableParam param) {
	return dataTable.load(param, new FilterGenerator() {
	    @Override
	    public FilterItem createFilterItem(String fieldName, Object valueStr) {
		FilterItem filterItem = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		switch (fieldName) {
		    case "startDateShift": {
			try {
			    filterItem = new FilterItem("startDateShift", ">=", sdf.parse(valueStr.toString()));
			} catch (ParseException ex) {
			    filterItem = new FilterItem("startDateShift", ">=", new Date());
			}
		    }
		    break;
		    case "endDateShift": {
			try {
			    filterItem = new FilterItem("endDateShift", "<=", sdf.parse(valueStr.toString()));
			} catch (ParseException ex) {
			    filterItem = new FilterItem("endDateShift", "<=", new Date());
			}
		    }
		    break;

		}
		return filterItem;
	    }
	});
    }

    public Shift findByDateShift(String branchId, Date checkTime) {
	try {
	    TypedQuery<Shift> typedQuery = entityManager.createNamedQuery("Shift.findByDateShift", Shift.class);
	    typedQuery.setParameter("checkTime", checkTime);
	    typedQuery.setParameter("branchId", branchId);
	    return typedQuery.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	}

    }

    @Override
    public EntityManager getEntityManager() {
	return entityManager;
    }

}
