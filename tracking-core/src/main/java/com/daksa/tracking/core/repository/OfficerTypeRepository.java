package com.daksa.tracking.core.repository;

import com.daksa.tracking.core.domain.OfficerType;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class OfficerTypeRepository extends JpaRepository<OfficerType, String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;

    public OfficerTypeRepository() {
        super(OfficerType.class);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
