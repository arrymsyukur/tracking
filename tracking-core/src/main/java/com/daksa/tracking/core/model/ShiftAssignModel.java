package com.daksa.tracking.core.model;

import java.util.List;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public class ShiftAssignModel {

    private List<String> officerId;
    private String shiftId;

    public ShiftAssignModel() {
    }

    public List<String> getOfficerId() {
	return officerId;
    }

    public void setOfficerId(List<String> officerId) {
	this.officerId = officerId;
    }

    public String getShiftId() {
	return shiftId;
    }

    public void setShiftId(String shiftId) {
	this.shiftId = shiftId;
    }

}
