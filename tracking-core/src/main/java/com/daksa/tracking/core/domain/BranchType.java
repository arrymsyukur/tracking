package com.daksa.tracking.core.domain;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public enum BranchType {
    POS, OFFICE, OTHER
}
