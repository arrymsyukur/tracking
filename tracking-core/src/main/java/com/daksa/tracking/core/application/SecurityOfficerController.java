package com.daksa.tracking.core.application;

import com.daksa.tracking.core.domain.OfficerType;
import com.daksa.tracking.core.domain.SecurityOfficer;
import com.daksa.tracking.core.domain.ShiftOfficer;
import com.daksa.tracking.core.model.OfficerLoginModel;
import com.daksa.tracking.core.model.SecurityOfficerModel;
import com.daksa.tracking.core.model.ShiftAssignModel;
import com.daksa.tracking.core.repository.OfficerTypeRepository;
import com.daksa.tracking.core.repository.SecurityOfficerRepository;
import com.daksa.tracking.core.service.SecurityOfficerService;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import com.daksa.tracking.webutil.rest.RestException;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("securityOfficer")
public class SecurityOfficerController {

    private static final Logger LOG = LoggerFactory.getLogger(SecurityOfficerController.class);
    @Inject
    private SecurityOfficerService securityOfficerService;
    @Inject
    private SecurityOfficerRepository securityOfficerRepository;
    @Inject
    private OfficerTypeRepository officerTypeRepository;

    @POST
    public SecurityOfficer registerSecurityOfficer(SecurityOfficerModel model) throws RestException {
	LOG.debug("##Create securityOfficer##");
	return securityOfficerService.registerEmployee(model);
    }

    @GET
    public TableResult<SecurityOfficer> findByFilters(@Context UriInfo uriInfo) {
	return securityOfficerRepository.findByfilters(new TableParam(uriInfo.getQueryParameters()));
    }

    @PUT
    public SecurityOfficer updateSecurityOfficer(SecurityOfficerModel securityOfficer) throws RestException {
	LOG.info("##Update Security Officer##");
	return securityOfficerService.updateSecurityOfficer(securityOfficer);
    }

    @GET
    @Path("{id}")
    public SecurityOfficerModel findById(@PathParam("id") String id) throws RestException {
	return securityOfficerService.findById(id);
    }

    @POST
    @Path("assign")
    public Boolean assignShift(ShiftAssignModel model) throws RestException {
	LOG.debug("##Assign security Officer shift##");
	return securityOfficerService.assignShift(model);
    }

    @POST
    @Path("checkIn")
    public ShiftOfficer checkInShift(OfficerLoginModel model) throws RestException {
	LOG.debug("##Assign security Officer shift##");
	return securityOfficerService.doCheckIn(model);
    }

    @GET
    @Path("officerType")
    public TableResult<OfficerType> findAll() {
	TableResult<OfficerType> tableResult = new TableResult<>();
	List<OfficerType> officerTypes = officerTypeRepository.findAll();
	tableResult.setData(officerTypes);
	tableResult.setRowCount(officerTypes == null ? 0 : Long.valueOf(officerTypes.size()));
	return tableResult;
    }

}
