package com.daksa.tracking.core.service;

import com.daksa.tracking.core.domain.Branch;
import com.daksa.tracking.core.domain.SecurityOfficer;
import com.daksa.tracking.core.domain.UserStatus;
import com.daksa.tracking.core.model.BranchRegistrationModel;
import com.daksa.tracking.core.model.OfficerLoginModel;
import com.daksa.tracking.core.repository.BranchRepository;
import com.daksa.tracking.core.repository.SecurityOfficerRepository;
import com.daksa.tracking.core.repository.EmployeeTrackingLogRepository;
import com.daksa.tracking.webutil.rest.RestException;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Stateless
public class BranchService implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(BranchService.class);
    private static final long serialVersionUID = 1L;
    @Inject
    private BranchRepository branchRepository;
    @Inject
    private SecurityOfficerRepository employeeRepository;
    @Inject
    private EmployeeTrackingLogRepository trackingLogRepository;

    public Branch createBranch(BranchRegistrationModel model) throws RestException {
	Branch branch = branchRepository.findByEmail(model.getEmail());
	if (branch != null) {
	    throw new RestException("Branch Email already Exist");
	}

	branch = new Branch(model);
	branchRepository.store(branch);
	LOG.info("##Register Branch Success");
	return branch;
    }

    public Branch updateBranch(Branch editedBranch) throws RestException {
	Branch newBranch = branchRepository.find(editedBranch.getId());
	if (newBranch == null) {
	    throw new RestException("Branch Not Found");
	}

	if (StringUtils.isNotEmpty(editedBranch.getBranchName())) {
	    newBranch.setBranchName(editedBranch.getBranchName());
	}
	if (StringUtils.isNotEmpty(editedBranch.getAddress())) {
	    newBranch.setAddress(editedBranch.getAddress());
	}
	if (editedBranch.getBranchType() != null) {
	    newBranch.setBranchType(editedBranch.getBranchType());
	}

	branchRepository.update(newBranch);
	return newBranch;
    }

}
