package com.daksa.tracking.core.domain.session;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
public enum UriMatchingStrategy {
	REGEX, FIXED
}
