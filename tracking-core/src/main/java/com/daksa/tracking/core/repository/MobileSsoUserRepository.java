package com.daksa.tracking.core.repository;

import com.daksa.tracking.core.domain.MobileSsoUser;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class MobileSsoUserRepository extends JpaRepository<MobileSsoUser, String> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private EntityManager entityManager;

    public MobileSsoUserRepository() {
        super(MobileSsoUser.class);
    }

    public MobileSsoUser findByUsername(String username) {
        try {
            TypedQuery query = entityManager.createNamedQuery("MobileSsoUser.findByUsername", MobileSsoUser.class);
            query.setParameter("username", username);
            query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
            return (MobileSsoUser) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    public MobileSsoUser findByEmail(String email) {
        try {
            TypedQuery query = entityManager.createNamedQuery("MobileSsoUser.findByEmail", MobileSsoUser.class);
            query.setParameter("email", email);
            query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
            return (MobileSsoUser) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
