package com.daksa.tracking.core.model;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public class OfficerLoginModel {

    private String officerId;
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date checkingTimestamp;
    private String branchId;

    public OfficerLoginModel() {
    }

    public String getBranchId() {
	return branchId;
    }

    public void setBranchId(String branchId) {
	this.branchId = branchId;
    }

    public String getOfficerId() {
	return officerId;
    }

    public void setOfficerId(String officerId) {
	this.officerId = officerId;
    }

    public Date getCheckingTimestamp() {
	return checkingTimestamp;
    }

    public void setCheckingTimestamp(Date checkingTimestamp) {
	this.checkingTimestamp = checkingTimestamp;
    }

}
