package com.daksa.tracking.core.service;

import com.daksa.tracking.core.domain.Branch;
import com.daksa.tracking.core.domain.Shift;
import com.daksa.tracking.core.repository.BranchRepository;
import com.daksa.tracking.core.repository.ShiftRepository;
import com.daksa.tracking.webutil.rest.RestException;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Stateless
public class ShiftService implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Inject
    private ShiftRepository shiftRepository;
    @Inject
    private BranchRepository branchRepository;
 
    
    public Shift createShift(Shift shift) throws RestException {
	Branch branch = branchRepository.find(shift.getBranchId());
	
	if (branch == null) {
	    throw new RestException("Branch Not Found");
	}
	Shift newShift = new Shift(shift.getStartDateShift(), shift.getEndDateShift(), branch);
	shiftRepository.store(newShift);
	return newShift;
    }
    
    public Shift updateShift(Shift oldShift) throws RestException {
	Shift newShift = shiftRepository.find(oldShift.getId());
	if (newShift == null) {
	    throw new RestException("Shift Not Found");
	}
	
	Branch branch = branchRepository.find(oldShift.getBranchId());
	if (branch == null) {
	    throw new RestException("Branch Not Found");
	}
	
	if (oldShift.getStartDateShift() != null) {
	    newShift.setStartDateShift(oldShift.getStartDateShift());
	}
	if (oldShift.getEndDateShift() != null) {
	    newShift.setEndDateShift(oldShift.getEndDateShift());
	}
	if (StringUtils.isNotEmpty(oldShift.getBranchId())) {
	    newShift.setBranchId(oldShift.getBranchId());
	    newShift.setBranch(branch);
	}
	
	shiftRepository.update(newShift);
	return newShift;
    }

}
