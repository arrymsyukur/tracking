package com.daksa.tracking.core.application;

import com.daksa.tracking.core.domain.Shift;
import com.daksa.tracking.core.repository.ShiftOfficerRepository;
import com.daksa.tracking.core.repository.ShiftRepository;
import com.daksa.tracking.core.service.ShiftService;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import com.daksa.tracking.webutil.rest.RestException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("shift")
public class ShiftController {

    private static final Logger LOG = LoggerFactory.getLogger(ShiftController.class);
    @Inject
    private ShiftService shiftService;
    @Inject
    private ShiftRepository shiftRepository;

    @POST
    public Shift createShift(Shift model) throws RestException {
	LOG.info("##Create Shift##");
	return shiftService.createShift(model);
    }

    @PUT
    public Shift updateShift(Shift model) throws RestException {
	LOG.info("##Update Shift##");
	return shiftService.updateShift(model);
    }

    @GET
    public TableResult<Shift> findByFilters(@Context UriInfo uriInfo) {
	LOG.info("##View Shift##");
	return shiftRepository.findByFilters(new TableParam(uriInfo.getQueryParameters()));
    }

}
