package com.daksa.tracking.core.domain;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@NamedQueries({
    @NamedQuery(name = "ShiftOfficer.findByofficerIdandShiftId", query = "SELECT s from ShiftOfficer s WHERE s.officerId = :officerId AND s.shiftId = :shiftId")
    ,
    @NamedQuery(name = "ShiftOfficer.findByShiftId", query = "SELECT s from ShiftOfficer s WHERE s.shiftId = :shiftId")})
@Table(name = "shift_officer", schema = "public")
public class ShiftOfficer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(length = 32)
    private String id;

    @OneToOne
    @JoinColumn(name = "officer_id", referencedColumnName = "id")
    private SecurityOfficer securityOfficer;

    @Column(name = "officer_id", insertable = false, updatable = false)
    private String officerId;

    @XmlTransient
    @OneToOne
    @JoinColumn(name = "shift_id", referencedColumnName = "id")
    private Shift shift;

    @Column(name = "shift_id", insertable = false, updatable = false)
    private String shiftId;

    @Column(name = "branch_name")
    private String branchName;

    @Column(name = "checked")
    private Boolean checked;

    @Temporal(TemporalType.TIMESTAMP)
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    @Column(name = "created_timestamp")
    private Date createdTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    @Column(name = "checking_timestamp")
    private Date checkingTimestamp;

    public ShiftOfficer() {
	this.id = IDGen.generate();
	this.checked = false;
    }

    public ShiftOfficer(SecurityOfficer securityOfficer, Shift shift) {
	this();
	this.securityOfficer = securityOfficer;
	this.officerId = securityOfficer.getId();
	this.shift = shift;
	this.shiftId = shift.getId();
	this.branchName = shift.getBranch() == null ? null : shift.getBranch().getBranchName();
	this.createdTimestamp = new Date();
    }

    public String getId() {
	return id;
    }

    public SecurityOfficer getSecurityOfficer() {
	return securityOfficer;
    }

    public String getOfficerId() {
	return officerId;
    }

    public Shift getShift() {
	return shift;
    }

    public String getShiftId() {
	return shiftId;
    }

    public void setSecurityOfficer(SecurityOfficer securityOfficer) {
	this.securityOfficer = securityOfficer;
    }

    public void setOfficerId(String officerId) {
	this.officerId = officerId;
    }

    public void setShift(Shift shift) {
	this.shift = shift;
    }

    public void setShiftId(String shiftId) {
	this.shiftId = shiftId;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	hash += (id != null ? id.hashCode() : 0);
	return hash;
    }

    @Override
    public boolean equals(Object object) {
	// TODO: Warning - this method won't work in the case the id fields are not set
	if (!(object instanceof ShiftOfficer)) {
	    return false;
	}
	ShiftOfficer other = (ShiftOfficer) object;
	if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "com.daksa.tracking.core.domain.ShiftOfficer[ id=" + id + " ]";
    }

    public Boolean getChecked() {
	return checked;
    }

    public void setChecked(Boolean checked) {
	this.checked = checked;
    }

    public String getBranchName() {
	return branchName;
    }

    public void setBranchName(String branchName) {
	this.branchName = branchName;
    }

    public Date getCreatedTimestamp() {
	return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
	this.createdTimestamp = createdTimestamp;
    }

    public Date getCheckingTimestamp() {
	return checkingTimestamp;
    }

    public void setCheckingTimestamp(Date checkingTimestamp) {
	this.checkingTimestamp = checkingTimestamp;
    }

}
