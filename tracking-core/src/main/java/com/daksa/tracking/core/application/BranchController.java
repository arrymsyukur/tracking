package com.daksa.tracking.core.application;

import com.daksa.tracking.core.domain.Branch;
import com.daksa.tracking.core.model.BranchRegistrationModel;
import com.daksa.tracking.core.model.SecurityOfficerModel;
import com.daksa.tracking.core.repository.BranchRepository;
import com.daksa.tracking.core.service.BranchService;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import com.daksa.tracking.webutil.rest.RestException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("branch")
public class BranchController {

    private static final Logger LOG = LoggerFactory.getLogger(BranchController.class);
    @Inject
    private BranchService branchService;
    @Inject
    private BranchRepository branchRepository;

    @POST
    public Branch createBranch(BranchRegistrationModel model) throws RestException {
	LOG.info("##Create Branch##");
	return branchService.createBranch(model);
    }

    @GET
    public TableResult<Branch> findByFilters(@Context UriInfo uriInfo) {
	LOG.info("##View Branch##");
	return branchRepository.findByFilters(new TableParam(uriInfo.getQueryParameters()));
    }

    @GET
    @Path("{id}")
    public Branch findById(@PathParam("id") String id) throws RestException {
	return branchRepository.find(id);
    }

    @PUT
    public Branch updateBranch(Branch branch) throws RestException {
	LOG.info("##Create Branch##");
	return branchService.updateBranch(branch);
    }
}
