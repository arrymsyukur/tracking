package com.daksa.tracking.core.domain;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement
@Entity
@Table(name = "employee_tracking_log", schema = "public")
public class EmployeeTrackingLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id")
    private SecurityOfficer employee;

    @Column(name = "employee_id", insertable = false, updatable = false)
    private String employeeId;

    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "branch_id")
    private Branch branch;

    @Column(name = "branch_id", insertable = false, updatable = false)
    private String branchId;

    @Column(name = "login_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date loginTimestamp;

    @Column(name = "logout_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date logoutTimestamp;

    @Column(name = "employee_login_status", nullable = false)
    private Boolean employeeLogin;

    public EmployeeTrackingLog() {
        this.id = IDGen.generate();
    }

    public EmployeeTrackingLog(SecurityOfficer employee, Branch branch, Date loginTimestamp) {
        this();
        this.employee = employee;
        this.employeeId = employee.getId();
        this.branch = branch;
        this.loginTimestamp = loginTimestamp;
        this.employeeLogin = true;
    }

    public SecurityOfficer getEmployee() {
        return employee;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public Branch getBranch() {
        return branch;
    }

    public String getBranchId() {
        return branchId;
    }

    public Date getLoginTimestamp() {
        return loginTimestamp;
    }

    public Date getLogoutTimestamp() {
        return logoutTimestamp;
    }

    public Boolean getEmployeeLogin() {
        return employeeLogin;
    }

    public void setLoginTimestamp(Date loginTimestamp) {
        this.loginTimestamp = loginTimestamp;
    }

    public void setLogoutTimestamp(Date logoutTimestamp) {
        this.logoutTimestamp = logoutTimestamp;
    }

    public void setEmployeeLogin(Boolean employeeLogin) {
        this.employeeLogin = employeeLogin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmployeeTrackingLog)) {
            return false;
        }
        EmployeeTrackingLog other = (EmployeeTrackingLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.daksa.tracking.core.domain.EmployeeTrackingLog[ id=" + id + " ]";
    }

}
