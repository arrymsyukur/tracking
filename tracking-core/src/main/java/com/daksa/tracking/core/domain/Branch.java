package com.daksa.tracking.core.domain;

import com.daksa.tracking.core.model.BranchRegistrationModel;
import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Entity
@Table(name = "branch", schema = "public")
@NamedQueries({
    @NamedQuery(name = "Branch.findByEmail", query = "SELECT b FROM Branch b WHERE b.email = :email")})
public class Branch implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 32)
    private String id;

    @Column(name = "branch_name", nullable = false, length = 64)
    private String branchName;

    @Column(length = 128)
    private String address;

    @Column(name = "branch_email", length = 64, nullable = false)
    private String email;

    @Column(name = "branch_status")
    @Enumerated(EnumType.STRING)
    private BranchStatus branchStatus;

    @Column(name = "branch_type")
    @Enumerated(EnumType.STRING)
    private BranchType branchType;

    @XmlTransient
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinTable(schema = "public", name = "officer_branch",
            joinColumns = @JoinColumn(name = "branch_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id", referencedColumnName = "id"),
            indexes = {
                @Index(name = "officer_branch_ux", columnList = "branch_id, employee_id", unique = true)})
    private List<SecurityOfficer> loginEmployee;

    @Column(length = 32,name = "est_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date establishmentTimestamp;

    public Branch() {
        this.id = IDGen.generate();
        this.branchStatus = BranchStatus.INACTIVE;
    }

    public Branch(String id, String branchName, String address, String email, BranchStatus branchStatus, BranchType branchType) {
        this();
        this.branchName = branchName;
        this.address = address;
        this.email = email;
        this.branchType = branchType;
    }

    public Branch(BranchRegistrationModel model) {
        this();
        this.branchName = model.getBranchName();
        this.address = model.getAddress();
        this.email = model.getEmail();
        this.branchType = model.getBranchType();
        this.establishmentTimestamp = new Date();
    }

    public String getId() {
        return id;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public BranchStatus getBranchStatus() {
        return branchStatus;
    }

    public BranchType getBranchType() {
        return branchType;
    }

    public List<SecurityOfficer> getLoginEmployee() {
        return loginEmployee == null ? new ArrayList<SecurityOfficer>() : loginEmployee;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBranchStatus(BranchStatus branchStatus) {
        this.branchStatus = branchStatus;
    }

    public void setBranchType(BranchType branchType) {
        this.branchType = branchType;
    }

    public void setLoginEmployee(List<SecurityOfficer> loginEmployee) {
        this.loginEmployee = loginEmployee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Branch)) {
            return false;
        }
        Branch other = (Branch) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.daksa.tracking.core.domain.Branch[ id=" + id + " ]";
    }

    public Date getEstablishmentTimestamp() {
        return establishmentTimestamp;
    }

    public void setEstablishmentTimestamp(Date establishmentTimestamp) {
        this.establishmentTimestamp = establishmentTimestamp;
    }

}
