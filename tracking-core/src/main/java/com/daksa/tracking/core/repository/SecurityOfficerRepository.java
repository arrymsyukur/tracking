package com.daksa.tracking.core.repository;

import com.daksa.tracking.core.domain.SecurityOfficer;
import com.daksa.tracking.webutil.persistence.DataTable;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class SecurityOfficerRepository extends JpaRepository<SecurityOfficer, String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;

    private DataTable<SecurityOfficer> dataTable;

    public SecurityOfficerRepository() {
        super(SecurityOfficer.class);
    }

    @PostConstruct
    public void init() {
        dataTable = new DataTable<>(entityManager, SecurityOfficer.class);
    }

    public TableResult<SecurityOfficer> findByfilters(TableParam param) {
        return dataTable.load(param);
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
