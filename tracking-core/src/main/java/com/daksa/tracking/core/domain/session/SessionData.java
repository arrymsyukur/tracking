package com.daksa.tracking.core.domain.session;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class SessionData {

	private List<Host> hostList = new ArrayList<>();
	private List<Permission> permissionList = new ArrayList<>();

	public List<Host> getHostList() {
		return hostList;
	}

	public void setHostList(List<Host> hostList) {
		this.hostList = hostList;
	}

	public List<Permission> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(List<Permission> permissionList) {
		this.permissionList = permissionList;
	}

	public List<Permission> getPermissionByHostId(String hostId) {
		List<Permission> permissions = new ArrayList<>();
		if (permissionList != null) {
			for (Permission p : permissionList) {
				if (p.getHostId().equals(hostId)) {
					permissions.add(p);
				}
			}
		}
		return permissions;
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Permission {

		private String id;
		private String name;
		private String uriPattern;
		private String hostId;
		private UriMatchingStrategy matchingStrategy;
		private String menu;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUriPattern() {
			return uriPattern;
		}

		public void setUriPattern(String uriPattern) {
			this.uriPattern = uriPattern;
		}

		public String getHostId() {
			return hostId;
		}

		public void setHostId(String hostId) {
			this.hostId = hostId;
		}

		public UriMatchingStrategy getMatchingStrategy() {
			return matchingStrategy;
		}

		public void setMatchingStrategy(UriMatchingStrategy matchingStrategy) {
			this.matchingStrategy = matchingStrategy;
		}

		public String getMenu() {
			return menu;
		}

		public void setMenu(String menu) {
			this.menu = menu;
		}

		public boolean isAllowed(String requestUri) {
			boolean allowed = false;
			if (uriPattern != null) {
				switch (matchingStrategy) {
					case FIXED:
						allowed = uriPattern.equals(requestUri);
						break;
					case REGEX:
						Pattern pattern = Pattern.compile(uriPattern);
						Matcher matcher = pattern.matcher(requestUri);
						allowed = matcher.matches();
						break;
				}
			}
			return allowed;
		}
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Host {

		private String id;
		private String name;
		private int orderNumber;
		private String url;
		private HostStatus status;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getOrderNumber() {
			return orderNumber;
		}

		public void setOrderNumber(int orderNumber) {
			this.orderNumber = orderNumber;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public HostStatus getStatus() {
			return status;
		}

		public void setStatus(HostStatus status) {
			this.status = status;
		}

	}
}
