/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.core.application;

import com.daksa.tracking.core.domain.SecurityOfficer;
import com.daksa.tracking.core.domain.ShiftOfficer;
import com.daksa.tracking.core.repository.ShiftOfficerRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("shiftOfficer")
public class ShiftOfficerController implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ShiftOfficerController.class);

    @Inject
    private ShiftOfficerRepository shiftOfficerRepository;

    @GET
    public TableResult<ShiftOfficer> findByFilters(@Context UriInfo uriInfo) {
	LOG.info("##Find All Shift Officer##");
	return shiftOfficerRepository.findByFilters(new TableParam(uriInfo.getQueryParameters()));
    }

    @DELETE
    public boolean deleteOfficerShift(@QueryParam("officerId") String officerId, @QueryParam("shiftId") String shiftId) {
	LOG.info("##Delete Shift##");
	shiftOfficerRepository.deleteShiftOfficer(officerId,shiftId);
	return true;
    }

    @GET
    @Path("officers")
    public TableResult<SecurityOfficer> findOfficerShift(@QueryParam("shiftId") String shiftId) {
	LOG.info("##View Shift##");
	return shiftOfficerRepository.findByShiftId(shiftId);
    }
}
