package com.daksa.tracking.core.domain;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public enum UserStatus {
	ACTIVE, BLOCKED, PENDING, LOCKED, REJECTED, EXPIRED
}
