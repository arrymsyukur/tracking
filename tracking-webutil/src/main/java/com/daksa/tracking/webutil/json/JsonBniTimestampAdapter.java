package com.daksa.tracking.webutil.json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author nafhul
 */
public class JsonBniTimestampAdapter extends XmlAdapter<String, Date> {

	private final static String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

	@Override
	public Date unmarshal(String v) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.parse(v);
	}

	@Override
	public String marshal(Date v) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.format(v);
	}
    
}
