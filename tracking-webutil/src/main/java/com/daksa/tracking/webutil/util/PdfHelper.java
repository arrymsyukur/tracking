package com.daksa.tracking.webutil.util;

import java.nio.file.FileSystems;

/**
 *
 * @author Puji Rohmat
 */
public class PdfHelper {
    public static final String REPO_PDF_BORROWER = System.getProperty("user.home")
            + FileSystems.getDefault().getSeparator()
            + "oliviap2p"
            + FileSystems.getDefault().getSeparator()
            + "pdf"
            + FileSystems.getDefault().getSeparator()
            + "borrower"
            + FileSystems.getDefault().getSeparator();
    
    public static final String REPO_PDF_LENDER = System.getProperty("user.home")
            + FileSystems.getDefault().getSeparator()
            + "oliviap2p"
            + FileSystems.getDefault().getSeparator()
            + "pdf"
            + FileSystems.getDefault().getSeparator()
            + "lender"
            + FileSystems.getDefault().getSeparator();
    
}
