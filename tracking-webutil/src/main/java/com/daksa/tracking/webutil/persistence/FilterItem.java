package com.daksa.tracking.webutil.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class FilterItem {

    public static final String DEFAULT_LOGICAL_OPERATOR = "AND";
    
	private final String operator;
    private String logicalOperator;
	private final String operand;
	private final Object value;
    private List<FilterItem> additionalFilter;

	public FilterItem(String operand, String operator, Object value) {
		this.operator = operator;
		this.operand = operand;
		this.value = value;
        this.additionalFilter = new ArrayList<>();
	}

	public String getOperator() {
		return operator;
	}

	public String getOperand() {
		return operand;
	}

	public Object getValue() {
		return value;
	}

    public String getLogicalOperator() {
		return logicalOperator;
	}

	public void setLogicalOperator(String logicalOperator) {
		this.logicalOperator = logicalOperator;
	}

	public List<FilterItem> getAdditionalFilter() {
		return additionalFilter;
	}

	public void setAdditionalFilter(List<FilterItem> additionalFilter) {
		this.additionalFilter = additionalFilter;
	}
    
	public static FilterItem booleanFilter(String operand, Object value) {
		String valueStr = "" + value;
		return new FilterItem(operand, " = ", Boolean.valueOf(valueStr));
	}

	public static FilterItem stringLikeFilter(String operand, Object value) {
		String valueStr = "" + value;
		return new FilterItem(
				"lower(" + operand + ")",
				"LIKE",
				"%" + valueStr.toLowerCase(Locale.getDefault()) + "%");
	}
    public static FilterItem stringLikeFrontFilter(String operand, Object value) {
		String valueStr = "" + value;
		return new FilterItem(
				"lower(" + operand + ")",
				"LIKE",
				valueStr.toLowerCase(Locale.getDefault()) + "%");
	}
	

	public static FilterItem stringMatchFilter(String operand, Object value) {
		String valueStr = "" + value;
		return new FilterItem(operand, " = ", valueStr);
	}
    
    public static FilterItem objectMatchFilter(String operand, Object value) {
		return new FilterItem(operand, " = ", value);
	}
    
    public static FilterItem isNullFilter(String operand) {
		return new FilterItem(operand, " IS NULL ", null);
	}

	public static FilterItem isNotNullFilter(String operand) {
		return new FilterItem(operand, " IS NOT NULL ", null);
	}
}
