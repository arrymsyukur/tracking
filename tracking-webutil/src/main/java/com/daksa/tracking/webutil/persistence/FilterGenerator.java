package com.daksa.tracking.webutil.persistence;

/**
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public interface FilterGenerator {

    FilterItem createFilterItem(String fieldName, Object valueStr);
}
