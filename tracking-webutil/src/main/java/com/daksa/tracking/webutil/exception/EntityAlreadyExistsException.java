package com.daksa.tracking.webutil.exception;

import com.daksa.tracking.webutil.rest.RestException;


/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class EntityAlreadyExistsException extends RestException {

	private static final long serialVersionUID = 1L;

	public EntityAlreadyExistsException(String prefix, String message) {
		super(400, "E2", message, prefix);
	}

	public EntityAlreadyExistsException(String message) {
		this(null, message);
	}

	public EntityAlreadyExistsException() {
		this("Entity already exists");
	}

}
