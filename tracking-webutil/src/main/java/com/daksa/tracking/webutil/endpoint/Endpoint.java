package com.daksa.tracking.webutil.endpoint;

import java.io.Serializable;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public interface Endpoint extends Serializable {
	
	EndpointResponse send(EndpointRequest request) throws EndpointException;

	String getHostId();
}
