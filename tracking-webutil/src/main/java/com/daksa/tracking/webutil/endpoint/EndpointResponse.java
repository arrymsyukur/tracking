package com.daksa.tracking.webutil.endpoint;

import java.util.Map;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public interface EndpointResponse {

	String getRequestId();
	
	String getCorrelationId();
	
	Map<String, Object> getProperties();
	
	byte[] getContent();
	
	String getContentType();
}
