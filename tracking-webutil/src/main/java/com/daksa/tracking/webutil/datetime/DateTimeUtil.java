package com.daksa.tracking.webutil.datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import javax.enterprise.context.ApplicationScoped;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@ApplicationScoped
public class DateTimeUtil {

    private final static Logger LOG = LoggerFactory.getLogger(DateTimeUtil.class);
    private final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public Date now() {
        return new Date();
    }

    public Calendar nowCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }

    public static Date makeDate(int day, int month, int year) throws ParseException {
        Locale locale = new Locale("en", "US");
        TimeZone timezone = TimeZone.getDefault();
        Calendar calendar = GregorianCalendar.getInstance(timezone, locale);
        if (month == 2) {
            if (isKabisat(year) && day > 29) {
                calendar.set(year, month, 29);
            } else if (isKabisat(year) && day <= 29) {
                calendar.set(year, month, day);
            } else if (!isKabisat(year) && day > 28) {
                calendar.set(year, month, 28);
            } else if (!isKabisat(year) && day <= 28) {
                calendar.set(year, month, day);
            }
        } else {
            calendar.set(year, month, day);
        }
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.parse(sdf.format(calendar.getTime()));
    }

    public static int getMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) + 1;
    }

    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    public static int getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DATE);
    }

    public static boolean isKabisat(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            return true;
        }
        return false;
    }

    public static int calculateDayBetweenTwoDate(Date start, Date end) {
        DateTime dt1 = new DateTime(start);
        DateTime dt2 = new DateTime(end);

        int difference = Days.daysBetween(dt1, dt2).getDays();
        if (difference < 0) {
            difference = difference * -1;
        }
        return difference;
    }

    public static int addWeekForPayment(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -7);
        return calendar.get(Calendar.DATE);
    }

    public static Date minYear(Integer min) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(DateTimeUtil.getYear(new Date()) - min, DateTimeUtil.getMonth(new Date()), DateTimeUtil.getDay(new Date()));
        return calendar.getTime();

    }
    
    public static Calendar endThisDay(Date startDate) {
		Calendar normalizedCalendar = Calendar.getInstance();
		normalizedCalendar.setTime(startDate);		
		normalizedCalendar.set(Calendar.HOUR_OF_DAY, 23);
		normalizedCalendar.set(Calendar.MINUTE, 59);
		normalizedCalendar.set(Calendar.SECOND, 59);
		normalizedCalendar.set(Calendar.MILLISECOND, 59);

		return normalizedCalendar;
	}
	
	public static Calendar startThisDay(Date startDate) {
		Calendar normalizedCalendar = Calendar.getInstance();
		normalizedCalendar.setTime(startDate);		
		normalizedCalendar.set(Calendar.HOUR_OF_DAY, 0);
		normalizedCalendar.set(Calendar.MINUTE, 0);
		normalizedCalendar.set(Calendar.SECOND, 0);
		normalizedCalendar.set(Calendar.MILLISECOND, 0);
		
		return normalizedCalendar;
	}
    
    public static Date changeDate(Date theDate, int change){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(theDate);
        calendar.add(Calendar.DATE, change);
        return calendar.getTime();
    }
    
    public static Date changeMonth(Date theDate, int change){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(theDate);
        calendar.add(Calendar.MONTH, change);
        return calendar.getTime();
    }
}
