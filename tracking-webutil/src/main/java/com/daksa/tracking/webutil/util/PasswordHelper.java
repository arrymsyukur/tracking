/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.webutil.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
public class PasswordHelper {
    private final static Logger LOG = LoggerFactory.getLogger(PasswordHelper.class);
    
    public static boolean checkPassword(String password) {
        String regex = "((?=.*\\d)(?=.*[A-Z]).{8,})";  
        return password.matches(regex);
    }
}
