package com.daksa.tracking.webutil.exception;

import com.daksa.tracking.webutil.rest.RestException;


/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class EntityNotExistsException extends RestException {

	private static final long serialVersionUID = 1L;

	public EntityNotExistsException(String prefix, String message) {
		super(400, "E1", message, prefix);
	}

	public EntityNotExistsException(String message) {
		this(null, message);
	}

	public EntityNotExistsException() {
		this("Entity does not exist");
	}
	
}
