package com.daksa.tracking.webutil.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class IDGen {
	
	public static String generate() {
		return UUID.randomUUID().toString().replace("-", "");
	}
    
    public static String generateTrxId(String virtualAccount) {
        //Max length 14
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date now = new Date();
        //Setelah concat, max length = 30
        return sdf.format(now).concat(virtualAccount);
    }
    
//    public static String generateCustomerReferenceNumber() {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//        String now = sdf.format(new Date());
//        Random rand = new Random(System.currentTimeMillis());
//        Integer randNumber = rand.nextInt(900) + 100;
//        return now.concat(randNumber.toString());
//    }
}
