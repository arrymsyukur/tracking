package com.daksa.tracking.webutil.util;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.nio.file.FileSystems;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Berry
 */
public class ImageHelper {

    private final static Logger LOG = LoggerFactory.getLogger(ImageHelper.class);
    public static final String REPO_FILE = System.getProperty("user.home")
            + FileSystems.getDefault().getSeparator()
            + "oliviap2p"
            + FileSystems.getDefault().getSeparator()
            + "upload"
            + FileSystems.getDefault().getSeparator();

    public static BufferedImage createResizedCopy(Image originalImage, int scaledWidth, int scaledHeight, boolean preserveAlpha) {
        LOG.info("resizing...");
        int imageType = preserveAlpha ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, imageType);
        Graphics2D g = scaledBI.createGraphics();
        if (preserveAlpha) {
            g.setComposite(AlphaComposite.Src);
        }
        g.drawImage(originalImage, 0, 0, scaledWidth, scaledHeight, null);
        g.dispose();
        return scaledBI;
    }
}
