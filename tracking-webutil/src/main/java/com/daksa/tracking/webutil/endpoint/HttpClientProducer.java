package com.daksa.tracking.webutil.endpoint;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@ApplicationScoped
public class HttpClientProducer {
	
	private static final Logger LOG = LoggerFactory.getLogger(HttpClientProducer.class);
	private HttpClient httpClient;

	@PostConstruct
	public void init() {
		LOG.info("init");
		httpClient = new HttpClient(new SslContextFactory());
		try {
			httpClient.start();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	@PreDestroy
	public void close() {
		LOG.info("close");
		try {
			httpClient.stop();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	@Produces
	@Dependent
	public HttpClient getHttpClient() {
		return httpClient;
	}
}
