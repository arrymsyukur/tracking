/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.webutil.util;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Berry
 */
@ApplicationScoped
public class FormatStringUtil {
    public static String separateDashString(String text) {
        if (Strings.isNullOrEmpty(text)) {
            return "-";
        }
        Iterable<String> result = Splitter.fixedLength(4).split(text);
        String[] parts = Iterables.toArray(result, String.class);
        
        String formatText = "";
        int iterator = 0;        
        for (String s : parts) {
            formatText = formatText.concat(s);
            if (iterator < parts.length - 1) {
                formatText = formatText.concat("-");
            }
            iterator++;
        }
        return formatText;
    }
}
