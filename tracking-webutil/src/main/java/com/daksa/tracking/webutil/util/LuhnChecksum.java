package com.daksa.tracking.webutil.util;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class LuhnChecksum {
	
	public static String addChecksum(String token) {
		int[] numbers = toArrayInt(token);
		int sum = 0;
		boolean multiply = true;
		for (int i = numbers.length - 1; i >= 0; i--) {
			int x = numbers[i];
			if (multiply) {
				x = x * 2;
			}
			multiply = !multiply;
			int sumDigit = sumDigits(x);
			sum += sumDigit;
		}
		int checksum = (sum * 9) % 10;
		return token + checksum;
	}

	private static int[] toArrayInt(String tokenStr) {
		int n = tokenStr.length();
		int[] numbers = new int[n];
		for (int i = 0; i < n; i++) {
			numbers[i] = tokenStr.charAt(i) - '0';
		}
		return numbers;
	}

	private static int sumDigits(int x) {
		int digits = x;
		int sum = 0;
		while (digits > 0) {
			sum += digits % 10;
			digits = digits / 10;
		}
		return sum;
	}
	
}
