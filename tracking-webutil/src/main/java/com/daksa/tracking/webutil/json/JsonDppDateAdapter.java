/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.webutil.json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 *
 * @author Berry
 */
public class JsonDppDateAdapter extends XmlAdapter<String, Date> {

	private final static String DATE_FORMAT = "dd-MM-yyyy";

	@Override
	public Date unmarshal(String v) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.parse(v);
	}

	@Override
	public String marshal(Date v) throws Exception {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.format(v);
	}
}
