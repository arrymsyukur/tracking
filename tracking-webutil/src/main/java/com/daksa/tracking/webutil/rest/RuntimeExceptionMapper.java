package com.daksa.tracking.webutil.rest;

import javax.ws.rs.ext.Provider;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@Provider
public class RuntimeExceptionMapper extends DefaultExceptionMapper<RuntimeException> {

}
