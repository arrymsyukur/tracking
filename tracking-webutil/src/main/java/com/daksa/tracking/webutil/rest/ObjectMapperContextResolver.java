package com.daksa.tracking.webutil.rest;

import com.daksa.tracking.webutil.json.Json;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Ginan
 */
@Provider
public class ObjectMapperContextResolver implements ContextResolver<ObjectMapper> {

	@Override
	public ObjectMapper getContext(Class<?> type) {
		return Json.getInstance().getObjectMapper();
	}
}
