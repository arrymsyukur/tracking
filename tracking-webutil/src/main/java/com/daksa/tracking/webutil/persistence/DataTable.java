package com.daksa.tracking.webutil.persistence;

import com.google.common.base.Strings;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 * @param <E>
 */
public class DataTable<E> {

    private static final Logger LOG = LoggerFactory.getLogger(DataTable.class);
    
	private final EntityManager entityManager;
	private final String entityVar;
	private final String querySelect;
	private final String queryCount;
	private final String queryGroupBySelect;
	private final String queryGroupByCount;
	private final Map<String, Object> additionalParams;
    private String entityName;

	public DataTable(EntityManager entityManager, Class<E> entityClass) {
		this(entityManager, entityClass, null, null);
	}

	public DataTable(EntityManager entityManager,
			String entityVar,
			String selectQuery,
			String countQuery,
			String selectGroupBy,
			String countGroupBy,
			Map<String, Object> additionalParams) {
		this.entityManager = entityManager;
		this.entityVar = entityVar;
		this.querySelect = selectQuery;
		this.queryCount = countQuery;
		this.queryGroupBySelect = selectGroupBy;
		this.queryGroupByCount = countGroupBy;
		this.additionalParams = additionalParams;
	}

	public DataTable(EntityManager entityManager, Class<E> entityClass, String selectGroupBy, String countGroupBy) {
		this.entityManager = entityManager;
		String entitySimpleName = entityClass.getSimpleName();
        this.entityName = entityClass.getName();
		this.entityVar = generateEntityVar(entitySimpleName);

		// SELECT-FROM-WHERE
		this.querySelect = "SELECT " + entityVar
				+ " FROM " + entityName + " " + entityVar
				+ " WHERE TRUE = TRUE";

		// SELECT-COUNT-FROM-WHERE
		this.queryCount = "SELECT COUNT(" + entityVar + ")"
				+ " FROM " + entityName + " " + entityVar
				+ " WHERE TRUE = TRUE";

		this.queryGroupBySelect = selectGroupBy;
		this.queryGroupByCount = countGroupBy;
		this.additionalParams = null;
	}

	@SuppressWarnings("unchecked")
	public TableResult<E> load(TableParam paginatedParam, FilterGenerator filterGenerator) {
		StringBuilder filterBuilder = new StringBuilder(64);
		List<Object> params = new ArrayList<>();
		Map<String, Object> filters = paginatedParam.getFilters();
		if (filters != null && !filters.isEmpty()) {
			int i = 0;
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				Object value = entry.getValue();
				FilterItem filterItem = null;
				if (filterGenerator != null) {
					filterItem = filterGenerator.createFilterItem(entry.getKey(), value);
				}
				if (filterItem == null) {
					if (isFieldRemoved(entry.getKey())) {
						continue;
					}
					filterItem = FilterItem.stringLikeFilter(queryVar(entry.getKey()), value);
				}

				// AND (operand operator ?i)
				filterBuilder.append(buildFilterItem(filterItem, i));
				if (filterItem.getValue() != null) {
					params.add(filterItem.getValue());
					i++;
				}

				if (filterItem.getAdditionalFilter() != null && !filterItem.getAdditionalFilter().isEmpty()) {
					for (FilterItem f : filterItem.getAdditionalFilter()) {
						filterBuilder.append(buildFilterItem(f, i));
						if (f.getValue() != null) {
							params.add(f.getValue());
							i++;
						}
					}
					filterBuilder.append(closingQueryArgument()); // closing bracket
				}
				filterBuilder.append(closingQueryArgument()); // closing bracket
			}
		}

		StringBuilder sortingBuilder = new StringBuilder();
		if (paginatedParam.getSortField() != null && !paginatedParam.getSortField().isEmpty()) {
			if (sortingBuilder.length() > 0) {
				sortingBuilder.append(", ");
			}
			sortingBuilder.append(queryVar(paginatedParam.getSortField()))
					.append(" ")
					.append(paginatedParam.getSortOrder() == TableParam.SortOrder.DESCENDING ? "DESC" : "ASC");
		}

		String sorting = "";
		if (sortingBuilder.length() > 0) {
			sorting = " ORDER BY " + sortingBuilder.toString();
		}

		String filterStr = filterBuilder.toString();
		String queryFindStr = querySelect + filterStr + (queryGroupBySelect != null ? (" GROUP BY " + queryGroupBySelect) : "") + sorting;
		String queryCountStr = queryCount + filterStr + (queryGroupByCount != null ? (" GROUP BY " + queryGroupByCount) : "");

		Query queryFindGenerated = entityManager.createQuery(queryFindStr);
		TypedQuery<Long> queryCountGenerated = entityManager.createQuery(queryCountStr, Long.class);

		if (paginatedParam.getLimit() != null) {
			queryFindGenerated.setMaxResults(paginatedParam.getLimit());
		}
		if (paginatedParam.getOffset() != null) {
			queryFindGenerated.setFirstResult(paginatedParam.getOffset());
		}

		int i = 0;
		for (Object param : params) {
			queryFindGenerated.setParameter(entityVar + i, param);
			queryCountGenerated.setParameter(entityVar + i, param);
			i++;
		}

		if (additionalParams != null) {
			for (Map.Entry<String, Object> entry : additionalParams.entrySet()) {
				Object value = entry.getValue();
				if (value != null && value instanceof Date) {
					TemporalType temporalType = entry.getKey().toLowerCase().contains("date") 
							? TemporalType.DATE : TemporalType.TIMESTAMP;
					queryFindGenerated.setParameter(entry.getKey(), (Date) value, temporalType);
					queryCountGenerated.setParameter(entry.getKey(), (Date) value, temporalType);
				} else {
					queryFindGenerated.setParameter(entry.getKey(), value);
					queryCountGenerated.setParameter(entry.getKey(), value);
				}
			}
		}

        List<E> result = null;
        LOG.debug("isCountOnly : {}", paginatedParam.isCountOnly()); 
		if (!paginatedParam.isCountOnly()) {
			result = queryFindGenerated.getResultList();
		}	
		Long count;
		try {
			count = queryCountGenerated.getSingleResult();
		} catch (NoResultException e) {
			count = 0L;
		}

		return new TableResult<>(result, count);
	}

	public TableResult<E> load(TableParam paginatedParam) {
		return load(paginatedParam, null);
	}

	public String queryVar(String param) {
		// TODO: Sanitize
		if (param.startsWith(":")) {
			return param.substring(1);
		} else {
			return entityVar + "." + param;
		}
	}

	private String generateEntityVar(String entityName) {
		return entityName.substring(0, 1).toLowerCase(Locale.getDefault()) + entityName.substring(1);
	}
    
    private boolean isFieldRemoved(String key) {
		try {
			Class clazz = Class.forName(entityName);
			Field field = clazz.getDeclaredField(key);			
			return false;
		} catch (NoSuchFieldException e) {
			return true;
		} catch (ClassNotFoundException | SecurityException e) {
			LOG.error(e.getMessage(), e);
			return true;
		}
	}

	private String buildFilterItem(FilterItem filterItem, int parameterNumber) {
		StringBuilder builder = new StringBuilder();
		builder.append(" ")
				.append(!Strings.isNullOrEmpty(filterItem.getLogicalOperator()) ? filterItem.getLogicalOperator() : FilterItem.DEFAULT_LOGICAL_OPERATOR)
				.append(" (") // here is the open query argument
				.append(filterItem.getOperand())
				.append(" ").append(filterItem.getOperator());
		if (filterItem.getValue() != null) {
			builder.append(" (:").append(entityVar).append(parameterNumber).append(")");
		}
		return builder.toString();
	}

	private String closingQueryArgument() {
		return ")";
	}
}
