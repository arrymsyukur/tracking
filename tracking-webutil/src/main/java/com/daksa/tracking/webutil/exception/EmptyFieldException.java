package com.daksa.tracking.webutil.exception;

import com.daksa.tracking.webutil.rest.RestException;


/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class EmptyFieldException extends RestException {

	private static final long serialVersionUID = 1L;

	public EmptyFieldException(String prefix, String message) {
		super(400, "F1", message, prefix);
	}

	public EmptyFieldException(String message) {
		this(null, message);
	}

	public EmptyFieldException() {
		this("Mandatory field cannot be empty");
	}
	
}
