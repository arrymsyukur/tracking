package com.daksa.tracking.webutil.endpoint;

import javax.enterprise.util.AnnotationLiteral;
import javax.inject.Named;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class NamedLiteral extends AnnotationLiteral<Named> implements Named {

	private static final long serialVersionUID = 1L;
	private final String value;

	public NamedLiteral(String value) {
		this.value = value;
	}

	@Override
	public String value() {
		return value;
	}

}
