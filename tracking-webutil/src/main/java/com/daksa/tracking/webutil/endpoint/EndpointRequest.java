package com.daksa.tracking.webutil.endpoint;

import java.util.Map;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public interface EndpointRequest {
	
	String getRequestId();
	
	String getCorrelationId();
	
	String getResource();
			
	String getMethod();
	
	Map<String, Object> getProperties();
	
	byte[] getContent();
	
	String getContentType();
    
    Map<String, Object> getHeaders();
}
