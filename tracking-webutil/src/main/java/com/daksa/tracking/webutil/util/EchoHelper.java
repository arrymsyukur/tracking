package com.daksa.tracking.webutil.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Nafhul A
 */
@RequestScoped
public class EchoHelper {

    private final static Logger LOG = LoggerFactory.getLogger(EchoHelper.class);

    public Map<String, Object> echo(HttpServletRequest request, String serviceName) {
        LOG.info("echo");
        Enumeration<String> i = request.getParameterNames();
        Map<String, Object> result = new HashMap<>();
        while (i.hasMoreElements()) {
            String name = i.nextElement();
            String value = request.getParameter(name);
            result.put(name, value);
            LOG.debug("{}: {}", name, value);
        }
        result.put(serviceName, "OK GAN!");
        return result;
    }

    public Map<String, Object> echoPost(Map<String, Object> request, String name) {
        LOG.info("echoPost");
        Map<String, Object> result = new HashMap<>();
        for (Map.Entry<String, Object> entry : request.entrySet()) {
            result.put(entry.getKey(), entry.getValue());
            LOG.info("{}: {}");
        }
        result.put(name, "OK GAN!");
        return result;
    }

    public String echoPost1(String text, String name) {
        LOG.info("echoPost");
        LOG.debug("text: {}", text);
        return "Hello World! " + name;
    }
}
