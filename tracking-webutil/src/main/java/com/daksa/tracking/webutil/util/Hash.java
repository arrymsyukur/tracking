package com.daksa.tracking.webutil.util;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Ginan
 */
public class Hash {

	public static final String SHA_1 = "SHA-1";
	public static final String SHA_256 = "SHA-256";
	public static final String SHA_512 = "SHA-512";
	public static final String MD5 = "MD5";
	private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

	public static String generateHash(String algo, String text) {
		return generateHash(algo, text.getBytes());
	}

	public static String generateHash(String algo, byte[] b) {
		try {
			StringBuilder result = new StringBuilder();
			MessageDigest md = MessageDigest.getInstance(algo);
			md.update(b);
			byte[] hash = md.digest();
			for (int i = 0; i < hash.length; i++) {
				result.append(String.format("%02x", hash[i]));
			}
			return result.toString();
		} catch (NoSuchAlgorithmException e) {
			System.err.println(e.getMessage());
			return null;
		}
	}

	public static String calculateRFC2104HMAC(String data, String key)
			throws java.security.SignatureException {
		String result = "";
		try {
			// get an hmac_sha1 key from the raw key bytes
			SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);

			// get an hmac_sha1 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
			mac.init(signingKey);

			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(data.getBytes());

			// base64-encode the hmac
			Base64 base64 = new Base64();
			result = new String(base64.encodeBase64(rawHmac));

		} catch (IllegalStateException | InvalidKeyException | NoSuchAlgorithmException e) {
			throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
		}
		return result;
	}
}
