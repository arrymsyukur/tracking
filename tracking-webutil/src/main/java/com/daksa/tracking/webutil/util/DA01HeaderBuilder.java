package com.daksa.tracking.webutil.util;

import com.daksa.tracking.webutil.json.Json;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.security.SignatureException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Nafhul A
 */
public final class DA01HeaderBuilder {

    private final static Logger LOG = LoggerFactory.getLogger(DA01HeaderBuilder.class);
    public static final String WEB_API_KEY = "786de9f94d6d440baf1e2ec6852dc8a4";
    public static final String WEB_SECRET_KEY = "E6tXIdfNT3ALQvZQi4TT5fIE0O1jke6l";

    //private String password;
    private static String hashAlgo;
    private static boolean useHash;

    public static Map<String, Object> generateDA01Headers(String resource, String method, Object param) throws JsonProcessingException, SignatureException {
        Map<String, Object> headers = new HashMap<>();
        headers.put("Date", DA01HeaderBuilder.getHeaderDate());
        headers.put("Authorization", DA01HeaderBuilder.generateAuthDA01(resource, method, Json.getWriter().writeValueAsString(param), MediaType.APPLICATION_JSON,
                WEB_API_KEY, WEB_SECRET_KEY, headers.get("Date").toString()));
        return headers;
    }

    public static String getHeaderDate() {
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        return dateFormat.format(new Date());
    }

    public static String generateAuthDA01(String canonicalPath, String method, String content, String contentType, String username, String password, String httpDate) throws SignatureException {
        StringBuilder signatureBuilder = new StringBuilder();
        signatureBuilder.append(method).append("\n");
        if (method.equalsIgnoreCase("POST") || method.equalsIgnoreCase("PUT")) {
            if (content != null && content.length() > 0) {
                signatureBuilder.append(Hash.generateHash(Hash.MD5, content)).append("\n");
            }
            if (contentType != null) {
                signatureBuilder.append(contentType).append("\n");
            }
        }
        signatureBuilder.append(httpDate).append("\n");
        signatureBuilder.append(canonicalPath);
        String signatureDigest = signatureBuilder.toString();
        LOG.debug("Authorization: {}", Hash.calculateRFC2104HMAC(signatureDigest, generatePassword(password)));
        return AuthType.DA01 + " " + username + ":"
                + Hash.calculateRFC2104HMAC(signatureDigest, generatePassword(password));
    }

    private static String generatePassword(String password) {
        String pass;
        if (useHash) {
            pass = Hash.generateHash(hashAlgo, password.getBytes());
        } else {
            pass = password;
        }
        return pass;
    }
}
