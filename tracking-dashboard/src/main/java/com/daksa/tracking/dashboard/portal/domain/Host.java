package com.daksa.tracking.dashboard.portal.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Table(schema = "portal", name = "host", indexes = {
    @Index(name = "host_api_key_idx", columnList = "api_key", unique = true)
    ,@Index(name = "host_status_idx", columnList = "status"),})
@NamedQueries({
    @NamedQuery(name = "BO.Host.findByApiKey", query = "SELECT h FROM Host h WHERE h.apiKey = :apiKey")
    ,@NamedQuery(name = "BO.Host.findAll", query = "SELECT h FROM Host h")
})
@Entity
public class Host implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 32)
    private String id;

    @Column(length = 64, nullable = false)
    private String name;

    @Column(name = "api_key", length = 64)
    private String apiKey;

    @Column(name = "secret_key", length = 64)
    private String secretKey;

    @Column(length = 255, nullable = false)
    private String url;

    @Column(name = "order_number", nullable = false)
    private int orderNumber;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 32, nullable = false)
    private HostStatus status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public HostStatus getStatus() {
        return status;
    }

    public void setStatus(HostStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Host other = (Host) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
