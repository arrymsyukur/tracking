package com.daksa.tracking.dashboard.web.table;



import com.daksa.tracking.dashboard.portal.domain.UserPermission;
import com.daksa.tracking.webutil.persistence.LocalDataTable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class UserPermissionTable extends LocalDataTable<UserPermission> {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;

	private String roleId;

	public UserPermissionTable() {
		super(UserPermission.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getRowKey(UserPermission object) {
		return object.getId();
	}

	@Override
	public UserPermission getRowData(String rowKey) {
		UserPermission permission =  entityManager.find(UserPermission.class, rowKey);
		return permission;
	}
	
	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

}
