package com.daksa.tracking.dashboard.portal.application;

import com.daksa.tracking.dashboard.portal.domain.UserPermission;
import com.daksa.tracking.dashboard.portal.domain.UserRole;
import com.daksa.tracking.dashboard.portal.repository.UserRoleRepository;
import com.daksa.tracking.dashboard.portal.service.UserRoleService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class RoleController implements Serializable {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(AuthController.class);

    @Inject
    private FacesContext facesContext;
    @Inject
    private RequestContext requestContext;
    @Inject
    private UserRoleService userRoleService;
    @Inject
    private UserRoleRepository userRoleRepository;

    private UserRole selectedUserRole;
    private List<UserPermission> rolePermissions = new ArrayList<>();
    private String roleId;
    private String roleName;

    @PostConstruct
    public void init() {
        selectedUserRole = null;
    }

    public void updateRole() {
        try {
            selectedUserRole.setPermissions(rolePermissions);
            userRoleService.updateRole(selectedUserRole);
            resetUpdateRole();
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Role has been updated"));
        } catch (Exception e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Update Role", e.getMessage()));
        }
    }

    public void deleteRole(UserRole role) {
        try {
            userRoleService.deleteRole(role);
            resetUpdateRole();
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Role has been deleted"));
        } catch (Exception e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Delete Role", e.getMessage()));
        }
    }

    public void resetUpdateRole() {
        requestContext.reset("main-form");
        selectedUserRole = null;
        rolePermissions = null;

    }

    public void createRole() {
        try {
            UserRole userRole = new UserRole();
            userRole.setName(roleName);
            userRole.setPermissions(rolePermissions);
            userRoleService.createRole(userRole);
            resetCreateRole();
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Role has been created"));
        } catch (Exception e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Create Role", e.getMessage()));
        }
    }

    public void resetCreateRole() {
        //requestContext.reset("roleForm");
        roleId = null;
        roleName = null;
        rolePermissions = null;
    }

    public UserRole getSelectedUserRole() {
        return selectedUserRole;
    }

    public void setSelectedUserRole(UserRole selectedUserRole) {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent("editRoleForm:userPermissionsTable");
        dataTable.setFirst(0);
        this.selectedUserRole = selectedUserRole;
        this.rolePermissions = userRoleRepository.getRolePermissions(selectedUserRole.getId());
    }

    public void selectUserRole(UserRole selectedUserRole) {
        this.selectedUserRole = selectedUserRole;
        this.rolePermissions = userRoleRepository.getRolePermissions(selectedUserRole.getId());
    }

    public List<UserPermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(List<UserPermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
