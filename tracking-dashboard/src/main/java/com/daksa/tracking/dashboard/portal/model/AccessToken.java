package com.daksa.tracking.dashboard.portal.model;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
public class AccessToken {

	private String token;
	private String callback;
	private String ssoSessionId;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCallback() {
		return callback;
	}

	public void setCallback(String callback) {
		this.callback = callback;
	}

	public String getSsoSessionId() {
		return ssoSessionId;
	}

	public void setSsoSessionId(String ssoSessionId) {
		this.ssoSessionId = ssoSessionId;
	}
}
