package com.daksa.tracking.dashboard.infrastructure;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Zulkhair Abdullah Daim
 */
@Dependent
public class WebResourceProducer {

	@Produces
	@RequestScoped
	public FacesContext produceFacesContext() {
		return FacesContext.getCurrentInstance();
	}
    
    @Produces
	@RequestScoped
	public RequestContext getRequestContext() {
		return RequestContext.getCurrentInstance();
	}
}
