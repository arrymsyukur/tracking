package com.daksa.tracking.dashboard.portal.application;

import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import com.daksa.tracking.dashboard.portal.service.SsoService;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@Named
@ViewScoped
public class SsoSessionController implements Serializable {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(SsoSessionController.class);

    @Inject
    private SsoService ssoService;

    @PostConstruct
    public void init() {
    }

    public void invalidate(SsoSession ssoSession) {
        ssoService.invalidateSession(ssoSession.getId());
    }
}
