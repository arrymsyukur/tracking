package com.daksa.tracking.dashboard.web.table;

import com.daksa.tracking.dashboard.model.web.BranchResponseModel;
import com.daksa.tracking.webutil.endpoint.Endpoint;
import com.daksa.tracking.webutil.endpoint.NamedLiteral;
import com.daksa.tracking.webutil.persistence.RestDataTable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.inject.Instance;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Named
@ViewScoped
public class BranchTable extends RestDataTable<BranchResponseModel> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String CORE_ENDPOINT = "core";

    @Inject
    private Instance<Endpoint> endpointInstance;

    public BranchTable() {
        super(BranchResponseModel.class);
    }

    @Override
    public List<BranchResponseModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (sortField == null) {
            sortField = "branchName";
            sortOrder = SortOrder.ASCENDING;
        }

        return super.load(first, pageSize, sortField, sortOrder, filters);
    }

    @Override
    protected Endpoint getEndpoint() {
        return endpointInstance.select(new NamedLiteral(CORE_ENDPOINT)).get();
    }

    @Override
    protected String getResource() {
        return "/branch";
    }

    @Override
    protected Map<String, Object> getAdditionalParams() {
        return new HashMap<>();
    }

    @Override
    protected Map<String, Object> getAdditionalHeaders() {
        return null;
    }

}
