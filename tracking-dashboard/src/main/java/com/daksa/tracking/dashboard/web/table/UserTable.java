package com.daksa.tracking.dashboard.web.table;


import com.daksa.tracking.dashboard.portal.domain.SsoUser;
import com.daksa.tracking.webutil.persistence.LocalDataTable;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class UserTable extends LocalDataTable<SsoUser> {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;
	
	public UserTable() {
		super(SsoUser.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public Object getRowKey(SsoUser object) {
		return object.getId();
	}

	@Override
	public SsoUser getRowData(String rowKey) {
		SsoUser user = entityManager.find(SsoUser.class, rowKey);
		return user;
	}
}
