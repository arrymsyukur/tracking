package com.daksa.tracking.dashboard.infrastructure;


import com.daksa.tracking.dashboard.portal.domain.SessionData;
import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import com.daksa.tracking.dashboard.portal.repository.ConfigRepository;
import com.daksa.tracking.dashboard.portal.service.SsoService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@RequestScoped
public class SsoFilter implements Filter {

	private final static Logger LOG = LoggerFactory.getLogger(SsoFilter.class);
	private static final String FACES_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<partial-response><redirect url=\"%s\"></redirect></partial-response>";

	@Inject
	private ConfigRepository configRepository;
	@Inject
	private SsoService ssoService;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest servletRequest = (HttpServletRequest) request;
		HttpServletResponse servletResponse = (HttpServletResponse) response;
		LOG.debug("doFilter (method: '{}', URI: '{}')",
				servletRequest.getMethod(), servletRequest.getRequestURI());
		String ssoSessionId = getSsoSessionId(servletRequest);
		if (ssoSessionId != null) {
			SsoSession ssoSession = ssoService.getSession(ssoSessionId, new Date());
			if (ssoSession != null) {
				SessionData sessionData = ssoSession.getSessionData();
				boolean allowed = false;
				String hostId = configRepository.getHostId();
				List<SessionData.Permission> permissions = sessionData.getPermissionByHostId(hostId);
				Iterator<SessionData.Permission> i = sessionData.getPermissionList().iterator();
				String parsedUri = getUri(servletRequest);
				while (!allowed && i.hasNext()) {
					SessionData.Permission permission = i.next();
					allowed = permission.isAllowed(parsedUri);
				}
				if (allowed) {
					servletRequest.setAttribute("ssoSession", ssoSession);
					servletRequest.setAttribute("userPermissions", permissions);
					servletRequest.setAttribute("hostList", sessionData.getHostList());
					chain.doFilter(request, response);
				} else {
					servletResponse.sendError(403);
				}
			} else {
				LOG.debug("ssoSession is null, redirect to login");
				redirectToLogin(servletRequest, servletResponse);
			}
		} else {
			LOG.debug("ssoSessionId is null, redirect to login");
			redirectToLogin(servletRequest, servletResponse);
		}
	}

	@Override
	public void destroy() {
	}

	private String getUri(HttpServletRequest servletRequest) {
		if (servletRequest.getContextPath() != null && !servletRequest.getContextPath().equals("/")) {
			return servletRequest.getRequestURI().substring(servletRequest.getContextPath().length());
		} else {
			return servletRequest.getRequestURI();
		}
	}

	private void redirectToLogin(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
		if ("partial/ajax".equals(servletRequest.getHeader("Faces-Request"))) {
			servletResponse.setContentType("text/xml");
			servletResponse.setCharacterEncoding("UTF-8");
			try (PrintWriter writer = servletResponse.getWriter()) {
				writer.printf(FACES_REDIRECT_XML, servletRequest.getContextPath() + "/portal/auth/login.xhtml");
			}
		} else {
			servletRequest.getSession().setAttribute("returnUrl", servletRequest.getRequestURI());
			servletResponse.sendRedirect(servletRequest.getContextPath() + "/portal/auth/login.xhtml");
		}
	}

	private String getSsoSessionId(HttpServletRequest servletRequest) {
		String ssoSessionId = null;
		if (servletRequest.getCookies() != null) {
			for (Cookie cookie : servletRequest.getCookies()) {
				if (cookie.getName().equals(configRepository.getSsoCookieName())) {
					ssoSessionId = cookie.getValue();
					break;
				}
			}
		}
		return ssoSessionId;
	}

}
