package com.daksa.tracking.dashboard.portal.domain;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public enum ActivityType {

    LOGIN("Login"),
    LOGOUT("Logout"),
    ADD_PRODUCT("Add produk"),
    ADD_PRODUCT_FAIL("Add produk fail"),
    ADD_LENDER_PRODUCT("Add lender produk"),
    ADD_LENDER_PRODUCT_FAIL("Add lender produk fail"),
    REQUEST_UPDATE_PRODUCT("Request update produk"),
    REQ_UPDATE_PRODUCT_FAIL("Update produk fail"),
    REQ_UPDATE_LENDER_PRODUCT("Request update lender produk"),
    REQ_UPDATE_LENDER_PRODUCT_FAIL("Update lender produk fail"),
    REQ_UPDATE_COMPANY_PRODUCT("Req update company produk"),
    REQ_UPDATE_COMPANY_PRODUCT_FAIL("Req update company produk fail"),
    DEACTIVE_PRODUCT("Deactive produk"),
    DEACTIVE_PRODUCT_FAIL("Deactive produk fail"),
    REACTIVE_PRODUCT("Deactive produk"),
    REACTIVE_PRODUCT_FAIL("Deactive produk fail"),
    APPROVE_PRODUCT("Approve product"),
    APPROVE_PRODUCT_FAIL("Approve product fail"),
    APPROVE_LENDER_PRODUCT("Approve lender product"),
    APPROVE_LENDER_PRODUCT_FAIL("Approve lender product fail"),
    REJECT_PRODUCT("Reject product"),
    REJECT_PRODUCT_FAIL("Reject product fail"),
    REJECT_LENDER_PRODUCT("Reject lender product"),
    REJECT_LENDER_PRODUCT_FAIL("Reject lender product fail"),
    APPROVE_COMPANY("Approve company"),
    APPROVE_EDIT_COMPANY("Approve edit company"),
    APPROVE_EDIT_COMPANY_FAIL("Approve edit company fail"),
    REJECT_EDIT_COMPANY("Reject edit company"),
    REJECT_EDIT_COMPANY_FAIL("Reject edit company fail"),    
    APPROVE_COMPANY_FAIL("Approve company fail"),
    UPDATE_COMPANY("Update company"),
    UPDATE_COMPANY_FAIL("Update company fail"),
    REJECT_COMPANY("Reject company"),
    REJECT_COMPANY_FAIL("Reject company fail"),
    REGISTER_COMPANY("Reject company"),
    REGISTER_COMPANY_FAIL("Reject company fail"),
    APPROVE_LENDER("Approve lender"),
    APPROVE_LENDER_FAIL("Approve lender fail"),
    REJECT_LENDER("Approve lender fail"),
    REJECT_LENDER_FAIL("Approve lender fail"),
    DOWNLOAD_COMPANY_DATA("Download company data"),
    DOWNLOAD_COMPANY_DATA_FAIL("Download company data fail"),
    APPROVE_GL("Approve GL"),
    APPROVE_GL_FAIL("Approve GL fail"),
    REJECT_APPROVE_GL("Reject Approve GL"),
    REJECT_APPROVE_GL_FAIL("Reject Approve GL fail"),
    APPROVE_EDIT_GL("Approve Edit GL"),
    APPROVE_EDIT_GL_FAIL("Approve Edit GL fail"),
    REJECT_EDIT_GL("Reject Approve Edit GL"),
    REJECT_EDIT_GL_FAIL("Reject Approve Edit GL fail"),
    MANUAL_POSTING("Manual posting"),
    MANUAL_POSTING_FAIL("Manual posting fail"),
    APPROVE_MANUAL_POSTING("Approve manual posting"),
    APPROVE_MANUAL_POSTING_FAIL("Approve manual posting fail"),
    REJECT_MANUAL_POSTING("Reject manual posting"),
    REJECT_MANUAL_POSTING_FAIL("Reject manual posting fail"),
    APPROVE_ACCOUNT("Approve account"),
    APPROVE_ACCOUNT_FAIL("Approve account fail"),
    REJECT_ACCOUNT("Reject account"),
    REJECT_ACCOUNT_FAIL("Reject account fail"),
    SET_ESCROW("Set escrow"),
    SET_ESCROW_FAIL("Set escrow fail"),
    APPROVE_SET_ESCROW("Approve set escrow"),
    APPROVE_SET_ESCROW_FAIL("Approve set escrow fail"),
    REJECT_SET_ESCROW("Reject set escrow"),
    REJECT_SET_ESCROW_FAIL("Reject set escrow fail"),
    ADD_COMPANY_PRODUCT_PAIR("Add company product pair"),
    ADD_COMPANY_PRODUCT_PAIR_FAIL("Add company product pair"),
    APPROVE_COMPANY_PRODUCT("Approve company product pair"),
    APPROVE_COMPANY_PRODUCT_FAIL("Approve company product fail"),
    REJECT_COMPANY_PRODUCT("Reject company product pair"),
    REJECT_COMPANY_PRODUCT_FAIL("Reject company product fail")
    ;

    private final String label;

    private ActivityType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
