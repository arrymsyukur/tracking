package com.daksa.tracking.dashboard.portal.repository;



import com.daksa.tracking.dashboard.portal.domain.UserActivity;
import com.daksa.tracking.webutil.datetime.DateTimeUtil;
import com.daksa.tracking.webutil.persistence.DataTable;
import com.daksa.tracking.webutil.persistence.FilterItem;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@Dependent
public class UserActivityRepository extends JpaRepository<UserActivity, String> implements Serializable{
    private static final Logger LOG = LoggerFactory.getLogger(UserActivityRepository.class);

	@Inject
	private EntityManager entityManager;
    private DataTable<UserActivity> dataTable;
    
    @PostConstruct
	public void init() {
		dataTable = new DataTable<>(entityManager, UserActivity.class);
	}
	
	public UserActivityRepository() {
		super(UserActivity.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
    public TableResult<UserActivity> findByFilters(TableParam param) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");      
        return dataTable.load(param, (String fieldName, Object valueStr) -> {
			FilterItem result = null;
			switch (fieldName) {
				case "startActivityTimestamp":
					try {                        
                        result = new FilterItem(dataTable.queryVar("activityTimestamp"), ">=", DateTimeUtil.startThisDay(sdf.parse(valueStr.toString())).getTime());
                    } catch (ParseException e) {
                        LOG.error(e.getMessage(), e);
                        return null;
                    }          
					break;
                case "endActivityTimestamp":
					try {                        
                        result = new FilterItem(dataTable.queryVar("activityTimestamp"), "<=", DateTimeUtil.endThisDay(sdf.parse(valueStr.toString())).getTime());
                    } catch (ParseException e) {
                        LOG.error(e.getMessage(), e);
                        return null;
                    }        
					break;
                case "userFullName":
                    result = FilterItem.stringLikeFilter(fieldName, valueStr.toString());
					break;    
			}
			return result;
		});
    }        
}
