package com.daksa.tracking.dashboard.portal.application;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@ViewScoped
@Named
public class TestEmailController implements Serializable {

	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(TestEmailController.class);

	@Inject
	private FacesContext facesContext;
//	@Inject
//	private EmailSenderService emailSenderService;
	
	private String to;
	private String subject;
	private String body;
	private String from;

	public void send() {
		LOG.info("send");
		try {
			//emailSenderService.sendEmailSync(from, to, subject, body);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "E-Mail has been sent successfully"));
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Send E-Mail", e.getMessage()));
		}
	}
	
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}	

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}
	
	
}
