package com.daksa.tracking.dashboard.portal.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Table(schema = "portal", name = "user_permission")
@Entity
public class UserPermission implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 64)
	private String id;

	@Column(length = 64)
	private String name;

	@Column(name = "uri_pattern", length = 255)
	private String uriPattern;

	@Column(name = "host_id", insertable = false, updatable = false)
	private String hostId;

	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "host_id")
	private Host host;

	@Enumerated(EnumType.STRING)
	@Column(name = "matching_strategy", length = 32)
	private UriMatchingStrategy matchingStrategy;

	@Column(name = "menu", length = 255)
	private String menu;

	public UserPermission() {
	}

	
	public UserPermission(String id, String name, String hostId, String menu) {
		this.id = id;
		this.name = name;
		this.hostId = hostId;
		this.menu = menu;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUriPattern() {
		return uriPattern;
	}

	public void setUriPattern(String uri) {
		this.uriPattern = uri;
	}

	public Host getHost() {
		return host;
	}

	public void setHost(Host host) {
		this.host = host;
		if (host != null) {
			this.hostId = host.getId();
		} else {
			this.hostId = null;
		}
	}

	public String getHostId() {
		return hostId;
	}

	public UriMatchingStrategy getMatchingStrategy() {
		return matchingStrategy;
	}

	public void setMatchingStrategy(UriMatchingStrategy matchingStrategy) {
		this.matchingStrategy = matchingStrategy;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 79 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UserPermission other = (UserPermission) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}
}
