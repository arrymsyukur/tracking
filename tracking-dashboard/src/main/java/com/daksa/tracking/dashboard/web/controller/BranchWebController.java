package com.daksa.tracking.dashboard.web.controller;

import com.daksa.tracking.dashboard.application.CoreService;
import com.daksa.tracking.dashboard.model.web.BranchRegistrationModel;
import com.daksa.tracking.dashboard.model.web.BranchResponseModel;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Named
@ViewScoped
public class BranchWebController implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(BranchWebController.class);

    @Inject
    private FacesContext facesContext;
    @Inject
    private CoreService coreService;

    private BranchRegistrationModel registrationModel;
    private List<String> branchTypes;
    private BranchResponseModel editModel;

    public void init() {
        clear();
        branchTypes = new ArrayList<>();
        branchTypes.add("POS");
        branchTypes.add("OFFICE");
        branchTypes.add("OTHER");
    }

    public void clear() {
        registrationModel = new BranchRegistrationModel();
        editModel = new BranchResponseModel();
    }

    public void createBranch() {
        try {
            coreService.createBranch(registrationModel);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Create Branch Success"));
            clear();
        } catch (EndpointException ex) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
        } catch (JsonProcessingException ex) {
            LOG.error("Error Parsing Json : " + ex.getMessage());
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Create Brance Failed"));
        }
    }

    public void updateBranch() {
        try {
            coreService.updateBranch(editModel);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Edit Branch Success"));
            clear();
        } catch (EndpointException ex) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
        } catch (JsonProcessingException ex) {
            LOG.error("Error Parsing Json : " + ex.getMessage());
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Edit Brance Failed"));
        }
    }

    public void setSelectedBranch(BranchResponseModel model) {
        editModel = model;
    }

    public BranchRegistrationModel getRegistrationModel() {
        return registrationModel;
    }

    public void setRegistrationModel(BranchRegistrationModel registrationModel) {
        this.registrationModel = registrationModel;
    }

    public List<String> getBranchTypes() {
        return branchTypes;
    }

    public void setBranchTypes(List<String> branchTypes) {
        this.branchTypes = branchTypes;
    }

    public BranchResponseModel getEditModel() {
        return editModel;
    }

    public void setEditModel(BranchResponseModel editModel) {
        this.editModel = editModel;
    }

}
