package com.daksa.tracking.dashboard.web.table;

import com.daksa.tracking.dashboard.model.web.SecurityOfficerResponseModel;
import com.daksa.tracking.webutil.endpoint.Endpoint;
import com.daksa.tracking.webutil.endpoint.NamedLiteral;
import com.daksa.tracking.webutil.persistence.RestDataTable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.ObjDoubleConsumer;
import javax.enterprise.inject.Instance;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Named
@ViewScoped
public class OfficerTable extends RestDataTable<SecurityOfficerResponseModel> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String CORE_ENDPOINT = "core";

    @Inject
    private Instance<Endpoint> endpointInstance;

    public OfficerTable() {
	super(SecurityOfficerResponseModel.class);
    }

    @Override
    public List<SecurityOfficerResponseModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
	if (sortField == null) {
	    sortField = "name";
	    sortOrder = SortOrder.ASCENDING;
	}

	return super.load(first, pageSize, sortField, sortOrder, filters);
    }

    @Override
    protected Endpoint getEndpoint() {
	return endpointInstance.select(new NamedLiteral(CORE_ENDPOINT)).get();
    }

    @Override
    protected String getResource() {
	return "/securityOfficer";
    }

    @Override
    protected Map<String, Object> getAdditionalParams() {
	return new HashMap<>();
    }

    @Override
    protected Map<String, Object> getAdditionalHeaders() {
	return null;
    }

    @Override
    public Object getRowKey(SecurityOfficerResponseModel object) {
	return object.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SecurityOfficerResponseModel getRowData(String rowKey) {
	Map<String, Object> filters = new HashMap<>();
	filters.put("id", rowKey);
	List<SecurityOfficerResponseModel> result = load(0, 1, null, null, filters);
	return result.get(0);
    }

}
