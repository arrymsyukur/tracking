/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.dashboard.model;

/**
 *
 * @author Berry
 */
public class EchoResponse {
    private String className;
    private Boolean isServiceUp;
    private Boolean isDatabaseUp;
    private String serviceName;
    private String exception;

    public EchoResponse() {
        this.className = this.getClass().getSimpleName();
        this.serviceName = "BACKOFFICE-WEB";
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Boolean getIsServiceUp() {
        return isServiceUp;
    }

    public void setIsServiceUp(Boolean isServiceUp) {
        this.isServiceUp = isServiceUp;
    }

    public Boolean getIsDatabaseUp() {
        return isDatabaseUp;
    }

    public void setIsDatabaseUp(Boolean isDatabaseUp) {
        this.isDatabaseUp = isDatabaseUp;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
