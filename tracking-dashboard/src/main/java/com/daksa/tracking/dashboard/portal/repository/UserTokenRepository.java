package com.daksa.tracking.dashboard.portal.repository;

import com.daksa.tracking.dashboard.portal.domain.UserToken;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@RequestScoped
public class UserTokenRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;

	public UserToken find(String id) {
		if (id != null) {
			return entityManager.find(UserToken.class, id);
		} else {
			return null;
		}
	}
}
