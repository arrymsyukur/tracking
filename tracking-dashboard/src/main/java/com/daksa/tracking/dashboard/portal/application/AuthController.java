package com.daksa.tracking.dashboard.portal.application;

import com.daksa.tracking.dashboard.infrastructure.CaptchaManageBean;
import com.daksa.tracking.dashboard.portal.domain.ActivityType;
import com.daksa.tracking.dashboard.portal.domain.Host;
import com.daksa.tracking.dashboard.portal.domain.SessionData;
import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import com.daksa.tracking.dashboard.portal.domain.SsoUser;
import com.daksa.tracking.dashboard.portal.domain.UserPermission;
import com.daksa.tracking.dashboard.portal.domain.UserStatus;
import com.daksa.tracking.dashboard.portal.model.AccessToken;
import com.daksa.tracking.dashboard.portal.model.UserActivityEvent;
import com.daksa.tracking.dashboard.portal.repository.ConfigRepository;
import com.daksa.tracking.dashboard.portal.repository.SsoSessionRepository;
import com.daksa.tracking.dashboard.portal.repository.SsoUserRepository;
import com.daksa.tracking.dashboard.portal.service.SsoService;
import com.daksa.tracking.dashboard.portal.service.UserService;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class AuthController implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);
    private static final int DEFAULT_SESSION_TIMEMOUT_MINUTES = 15;

    @Inject
    private FacesContext facesContext;
    @Inject
    private ConfigRepository configRepository;
    @Inject
    private SsoService ssoService;
    @Inject
    @Any
    private SsoUserRepository ssoUserRepository;
    @Inject
    private Event<UserActivityEvent> userActivityEvent;   
    @Inject
    private UserService userService;    
    @Inject
    private SsoSessionRepository ssoSessionRepository;
    @Inject
    private CaptchaManageBean captchaManageBean;

    private AccessToken accessToken;
    private SsoSession ssoSession;

    private String username;
    private String password;
    private boolean keepLogin;   
    private String email;  
    private String captcha;

    public void init() throws IOException {
        LOG.info("init (viewId: '{}')", facesContext.getViewRoot().getViewId());
        String token = facesContext.getExternalContext().getRequestParameterMap().get("token");
        if (token != null && !token.isEmpty()) {
            //accessToken = ssoService.getAccessToken(token);
        }

        if (ssoSession != null) { // user already loggedin
            if (accessToken != null) { // redirected from service provider
                LOG.debug("redirect to: '{}'", accessToken.getCallback());
                accessToken.setSsoSessionId(ssoSession.getId());
                facesContext.getExternalContext().redirect(accessToken.getCallback());
            } else {
                redirectToHome();
            }
        }
        
        generateCaptcha();
    }
    
    public void generateCaptcha() {
        LOG.debug("generate token captcha . . .");        
        captchaManageBean.generateToken((HttpSession) facesContext.getExternalContext().getSession(true));
        captcha = null;
    }

    public void forgotPassword() {
        if (ssoUserRepository.findUserByEmail(email) != null) {
            try {
                userService.forgotPassword(email);
                facesContext.addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Your Password has been changed, Please check your email"));
                email = null;

            } catch (JsonProcessingException | EndpointException ex) {
                LOG.debug(ex.getMessage(), ex);
                facesContext.addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Error When Send Email"));
            }
        } else {
            facesContext.addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Email Not Found"));
        }
    }

    public void login() throws IOException {
		LOG.info("login");
        if (!Strings.isNullOrEmpty(captcha)) {
            SsoUser user = ssoUserRepository.findUserByUsername(username);
            boolean captchaSuccess = false;
            LOG.debug("check captcha . . .");
            LOG.debug("captcha : " + captcha);            
            LOG.debug("valid captcha : " + captchaManageBean.getToken((HttpSession) facesContext.getExternalContext().getSession(false)));
            captchaSuccess = captcha.equals(captchaManageBean.getToken((HttpSession) facesContext.getExternalContext().getSession(false)));
            LOG.debug("captchaSuccess : " + captchaSuccess);
            if (captchaSuccess) {
                if (user != null && user.verifyPassword(password)) {
                    if (user.getStatus() == UserStatus.ACTIVE) {
                        if (ssoSession != null) {
                            ssoService.invalidateSession(ssoSession.getId());
                            ssoSession = null;
                        }
                        Date now = new Date();
                        int timemoutSeconds = keepLogin
                                ? configRepository.getSsoCookieTimeoutMinutes()
                                : DEFAULT_SESSION_TIMEMOUT_MINUTES;

                        List<Host> hostList = ssoUserRepository.getUserHostList(user.getId());
                        SessionData sessionData = new SessionData();
                        sessionData.setHostList(new ArrayList<>());
                        for (Host host : hostList) {
                            SessionData.Host h = new SessionData.Host();
                            h.setId(host.getId());
                            h.setName(host.getName());
                            h.setOrderNumber(host.getOrderNumber());
                            h.setStatus(host.getStatus());
                            h.setUrl(host.getUrl());
                            sessionData.getHostList().add(h);
                        }

                        List<UserPermission> permissionList = ssoUserRepository.getUserPermissions(user.getId());
                        sessionData.setPermissionList(new ArrayList<>());
                        for (UserPermission permission : permissionList) {
                            SessionData.Permission p = new SessionData.Permission();
                            p.setId(permission.getId());
                            p.setHostId(permission.getHostId());
                            p.setMatchingStrategy(permission.getMatchingStrategy());
                            p.setMenu(permission.getMenu());
                            p.setName(permission.getName());
                            p.setUriPattern(permission.getUriPattern());
                            sessionData.getPermissionList().add(p);
                        }

                        ssoSession = new SsoSession(user, now, timemoutSeconds);
                        ssoSession.setSessionData(sessionData);
                        ssoService.createSession(ssoSession, now);

                        Map<String, Object> cookieProperties = new HashMap<>();
                        cookieProperties.put("path", getCookiePath());
                        cookieProperties.put("httpOnly", true);
                        if (keepLogin) {
                            cookieProperties.put("maxAge", configRepository.getSsoCookieTimeoutMinutes() * 60);
                        }
                        facesContext.getExternalContext().addResponseCookie(
                                configRepository.getSsoCookieName(), ssoSession.getId(), cookieProperties);
                        facesContext.addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_INFO, "Login Success", "Login Success"));
                        userActivityEvent.fire(new UserActivityEvent.Builder()
                                .activityType(ActivityType.LOGIN)
                                .ssoSession(ssoSession)
                                .build());
                        if (accessToken != null) {
                            LOG.debug("redirect to: '{}'", accessToken.getCallback());
                            accessToken.setSsoSessionId(ssoSession.getId());
                            facesContext.getExternalContext().redirect(accessToken.getCallback());
                        } else {
                            String returnUrl = (String) facesContext.getExternalContext().getSessionMap().get("returnUrl");
                            if (returnUrl != null && !returnUrl.isEmpty()) {
                                LOG.debug("redirect to: '{}'", returnUrl);
                                System.out.println(returnUrl);
                                facesContext.getExternalContext().redirect(returnUrl);
                            } else {
                                LOG.debug("redirect to home");
                                redirectToHome();
                            }
                        }
                    } else {
                        generateCaptcha();
                        captcha = "";
                        facesContext.addMessage(null,
                                new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Gagal", "User belum aktif"));
                    }
                } else {
                    generateCaptcha();
                    captcha = "";
                    facesContext.addMessage(null,
                            new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Gagal", "Username atau password salah"));
                }
            } else {
                generateCaptcha();
                captcha = "";
                LOG.debug("invalid captcha . . .");
                facesContext.addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Gagal", "Captcha yang anda masukan salah"));                
            }
        } else {
            generateCaptcha();
            captcha = "";
            facesContext.addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Gagal", "Captcha tidak boleh kosong"));
        }				
	}

    public void logout() throws IOException {
        LOG.info("logout");
        Cookie cookie = (Cookie) facesContext.getExternalContext().getRequestCookieMap().get(
                configRepository.getSsoCookieName());                
        if (cookie != null) {
            String userSessionId = cookie.getValue();            
            ssoSession = ssoSessionRepository.find(userSessionId);
            LOG.debug("Session Id: {}", ssoSession.getId()); 
            userActivityEvent.fire(new UserActivityEvent.Builder()
                    .activityType(ActivityType.LOGOUT)
                    .ssoSession(ssoSession)
                    .build());
            ssoService.invalidateSession(userSessionId);

            Map<String, Object> cookieProperties = new HashMap<>();
            cookieProperties.put("path", getCookiePath());
            cookieProperties.put("maxAge", 0);
            cookieProperties.put("httpOnly", true);

            facesContext.getExternalContext().invalidateSession();
            facesContext.getExternalContext().addResponseCookie(
                    configRepository.getSsoCookieName(), userSessionId, cookieProperties);            
        }
        facesContext.getExternalContext().invalidateSession();
        redirectToHome();
    }

    private void redirectToHome() {
        facesContext.getApplication().getNavigationHandler().handleNavigation(
                facesContext, null, "/index.xhtml?faces-redirect=true");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isKeepLogin() {
        return keepLogin;
    }

    public void setKeepLogin(boolean keepLogin) {
        this.keepLogin = keepLogin;
    }

    private String getCookiePath() {
        String contextPath = facesContext.getExternalContext().getRequestContextPath();
        if (StringUtils.isEmpty(contextPath)) {
            contextPath = "/";
        }
        return contextPath;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

}
