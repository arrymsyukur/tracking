package com.daksa.tracking.dashboard.application;

import com.daksa.tracking.dashboard.model.web.BranchRegistrationModel;
import com.daksa.tracking.dashboard.model.web.BranchResponseModel;
import com.daksa.tracking.dashboard.model.web.OfficerTypeModel;
import com.daksa.tracking.dashboard.model.web.SecurityOfficerRegistrationModel;
import com.daksa.tracking.dashboard.model.web.SecurityOfficerResponseModel;
import com.daksa.tracking.dashboard.model.web.ShiftAssignModel;
import com.daksa.tracking.dashboard.model.web.ShiftOfficerModel;
import com.daksa.tracking.dashboard.model.web.ShiftResponseModel;
import com.daksa.tracking.webutil.endpoint.Endpoint;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.daksa.tracking.webutil.endpoint.EndpointRequestBuilder;
import com.daksa.tracking.webutil.endpoint.NamedLiteral;
import com.daksa.tracking.webutil.json.Json;
import com.daksa.tracking.webutil.json.JsonResponseReader;
import com.daksa.tracking.webutil.persistence.TableResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@RequestScoped
public class CoreService {

    private static final String TRACKING_CORE_ENDPOINT = "core";
    private static final Logger LOG = LoggerFactory.getLogger(CoreService.class);

    @Inject
    private Instance<Endpoint> endpointInstance;
    private Endpoint endpoint;

    @PostConstruct
    public void init() {
	endpoint = endpointInstance.select(new NamedLiteral(TRACKING_CORE_ENDPOINT)).get();
    }

    public BranchResponseModel createBranch(BranchRegistrationModel model) throws JsonProcessingException, EndpointException {
	LOG.info("##Create New Branch##");
	BranchResponseModel response;
	try {
	    response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/branch")
		    .method("POST")
		    .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
		    .build())).getContentObject(BranchResponseModel.class);
	    return response;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	}

    }

    public BranchResponseModel updateBranch(BranchResponseModel model) throws JsonProcessingException, EndpointException {
	LOG.info("##Update Branch##");
	BranchResponseModel response;
	try {
	    response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/branch")
		    .method("PUT")
		    .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
		    .build())).getContentObject(BranchResponseModel.class);
	    return response;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	}

    }

    public List<BranchResponseModel> findAllBranch() throws IOException, EndpointException {
	LOG.info("##Find All Branch ##");
	try {
	    String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/branch")
		    .method("GET")
		    .build())).getContentAsString();
	    TableResult<BranchResponseModel> result = Json.getReader()
		    .forType(new TypeReference<TableResult<BranchResponseModel>>() {
		    })
		    .readValue(response);

	    return result.getData();
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	} catch (IOException ex) {
	    throw new IOException("Error When Parsing Data");
	}
    }

    public BranchResponseModel findBranchById(String id) throws IOException, EndpointException {
	LOG.info("##Find Branch By Id##");
	try {
	    String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/branch/".concat(id))
		    .method("GET")
		    .build())).getContentAsString();
	    BranchResponseModel result = Json.getReader().
		    forType(BranchResponseModel.class).
		    readValue(response);

	    return result;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	} catch (IOException ex) {
	    throw new IOException("Error When Parsing Data");
	}
    }

    public SecurityOfficerResponseModel addOfficer(SecurityOfficerRegistrationModel model) throws JsonProcessingException, EndpointException {
	LOG.info("##Create New Officer##");
	SecurityOfficerResponseModel response;
	try {
	    response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/securityOfficer")
		    .method("POST")
		    .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
		    .build())).getContentObject(SecurityOfficerResponseModel.class);
	    return response;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	}

    }

    public SecurityOfficerResponseModel updateOfficer(SecurityOfficerResponseModel model) throws JsonProcessingException, EndpointException {
	LOG.info("##Update Officer##");
	SecurityOfficerResponseModel response;
	try {
	    response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/securityOfficer")
		    .method("PUT")
		    .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
		    .build())).getContentObject(SecurityOfficerResponseModel.class);
	    return response;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	}

    }

    public List<OfficerTypeModel> findAllOfficerType() throws IOException, EndpointException {
	LOG.info("##Find All Officer Type##");
	try {
	    String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/securityOfficer/officerType")
		    .method("GET")
		    .build())).getContentAsString();
	    TableResult<OfficerTypeModel> result = Json.getReader()
		    .forType(new TypeReference<TableResult<OfficerTypeModel>>() {
		    })
		    .readValue(response);

	    return result.getData();
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	} catch (IOException ex) {
	    throw new IOException("Error When Parsing Data");
	}
    }

    public SecurityOfficerResponseModel findOfficerById(String id) throws IOException, EndpointException {
	LOG.info("##Find Officer By Id##");
	try {
	    String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/securityOfficer/".concat(id))
		    .method("GET")
		    .build())).getContentAsString();
	    SecurityOfficerResponseModel result = Json.getReader().
		    forType(SecurityOfficerResponseModel.class).
		    readValue(response);

	    return result;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	} catch (IOException ex) {
	    throw new IOException("Error When Parsing Data");
	}
    }

    public List<SecurityOfficerResponseModel> findOfficerShift(String shiftId) throws IOException, EndpointException {
	LOG.info("##Find Officer By Shift ##");
	try {
	    String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/shiftOfficer/officers")
		    .method("GET")
		    .property("shiftId", shiftId)
		    .build())).getContentAsString();
	    TableResult<SecurityOfficerResponseModel> result = Json.getReader()
		    .forType(new TypeReference<TableResult<SecurityOfficerResponseModel>>() {
		    })
		    .readValue(response);

	    return result.getData();
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	} catch (IOException ex) {
	    throw new IOException("Error When Parsing Data");
	}
    }

    public void removeOfficerShift(String officerId, String shiftId) throws IOException, EndpointException {
	LOG.info("##Remove Shift Officer##");
	try {
	    String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/shiftOfficer")
		    .property("officerId", officerId)
		    .property("shiftId", shiftId)
		    .method("DELETE")
		    .build())).getContentAsString();

	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());

	}
    }

    public ShiftResponseModel addShift(ShiftResponseModel model) throws JsonProcessingException, EndpointException {
	LOG.info("##Create New Officer##");
	ShiftResponseModel response;
	try {
	    response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/shift")
		    .method("POST")
		    .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
		    .build())).getContentObject(ShiftResponseModel.class);
	    return response;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	}

    }

    public ShiftResponseModel updateShift(ShiftResponseModel model) throws JsonProcessingException, EndpointException {
	LOG.info("##Update Shift##");
	ShiftResponseModel response;
	try {
	    response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/shift")
		    .method("PUT")
		    .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
		    .build())).getContentObject(ShiftResponseModel.class);
	    return response;
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	}

    }

    public void assignShift(ShiftAssignModel model) throws JsonProcessingException, EndpointException {
	LOG.info("##Assign Shift##");
	String response;
	try {
	    response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
		    .resource("/securityOfficer/assign")
		    .method("POST")
		    .content(Json.getWriter().writeValueAsBytes(model), MediaType.APPLICATION_JSON)
		    .build())).getContentAsString();
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage(), ex);
	    throw new EndpointException(ex.getResponseCode(), ex.getMessage());
	}

    }
    
    }
