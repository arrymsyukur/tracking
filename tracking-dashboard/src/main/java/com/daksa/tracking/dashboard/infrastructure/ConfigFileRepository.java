package com.daksa.tracking.dashboard.infrastructure;

import java.io.*;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Nafhul A
 */
@ApplicationScoped
public class ConfigFileRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(ConfigFileRepository.class);
//    private final static String CONFIG = "/opt/wildfly-10.1.0.Final/domain/configuration/dooet-core/config.properties"; //DOMAIN
    private final static String CONFIG = System.getProperty("jboss.server.config.dir")
			+ File.separator + "tracking-dashboard" + File.separator + "config.properties";
    private Properties properties;

    public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {

    }

    @PostConstruct
    public void init() {
        LOG.info("init");
        File file = new File(CONFIG);
        LOG.debug("config : {}", CONFIG);
        properties = new Properties();
        if (file.exists()) {
            LOG.info("Load config file at: {}", file.getAbsolutePath());
            try (FileInputStream input = new FileInputStream(file)) {
                properties.load(input);
            } catch (IOException e) {
                LOG.error("Error when load file config : " + e.getMessage());
                LOG.error("Error when load file config : " + e);
                throw new RuntimeException();
            }
        } else {
            LOG.info("Config file not found, create default config at: {}", file.getAbsolutePath());
            properties.setProperty(ConfigFileKey.TRACKING_CORE_URL.getConfigId(), "http://localhost:5003/tracking-core/rest");            
            file.getParentFile().mkdirs();
            try (FileOutputStream output = new FileOutputStream(file)) {
                properties.store(output, "Configuration File");
            } catch (IOException e) {
                LOG.error("Error when create file config : " + e.getMessage());
                LOG.error("Error when create file config : " + e);
                throw new RuntimeException();
            }
        }
    }

    public String getConfigValue(ConfigFileKey configKey) {
        return properties.getProperty(configKey.getConfigId());
    }
}
