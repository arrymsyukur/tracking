package com.daksa.tracking.dashboard.infrastructure.web;


import com.daksa.tracking.webutil.endpoint.Endpoint;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.daksa.tracking.webutil.endpoint.EndpointRequestBuilder;
import com.daksa.tracking.webutil.endpoint.EndpointResponse;
import com.daksa.tracking.webutil.json.Json;
import com.daksa.tracking.webutil.persistence.TableResult;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 * @param <E>
 */
public abstract class RestDataTable<E> extends LazyDataModel<E> {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(RestDataTable.class);

    private Class<?> dataClass;

    public RestDataTable() {
    }

    public RestDataTable(Class<?> dataClass) {
	this.dataClass = dataClass;
    }

    @Override
    public List<E> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
	Map<String, Object> properties = new HashMap<>();
	properties.putAll(filters);
	properties.put("limit", pageSize);
	properties.put("offset", first);
	if (sortField != null) {
	    properties.put("sortField", sortField);
	}
	if (sortOrder != null) {
	    properties.put("sortOrder", sortOrder.toString());
	}
	for (Map.Entry<String, Object> entry : getAdditionalParams().entrySet()) {
	    properties.put(entry.getKey().replaceAll("\\.", "-"), entry.getValue().toString());
	}
	try {
	    Endpoint endpoint = getEndpoint();
	    EndpointResponse response = endpoint.send(new EndpointRequestBuilder()
		    .resource(getResource())
		    .method("GET")
		    .properties(properties)
		    .build());
	    ObjectReader objectReader;
	    if (dataClass != null) {
		JavaType javaType = Json.getInstance().getObjectMapper()
			.getTypeFactory()
			.constructParametricType(TableResult.class, dataClass);
		objectReader = Json.getReader().forType(javaType);
	    } else {
		objectReader = Json.getReader()
			.forType(new TypeReference<TableResult<E>>() {
			});
	    }
	    TableResult<E> result = objectReader.readValue(response.getContent());
	    setRowCount(result.getRowCount().intValue());
	    return result.getData();
	} catch (IOException | EndpointException e) {
	    LOG.error(e.getMessage(), e);
	    return null;
	}
    }

    protected abstract Endpoint getEndpoint();

    protected abstract String getResource();

    protected abstract Map<String, Object> getAdditionalParams();

}
