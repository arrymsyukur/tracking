/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.dashboard.model.messaging;

/**
 *
 * @author Berry
 */
public enum EmailType {
    LENDER_BID,
    LOAN_FULFILL,
    LOAN_DISBURSE,
    PAYMENT,
    WEB_USER_ACTIVATION,
    FORGOT_PASSWORD
}
