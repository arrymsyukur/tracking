package com.daksa.tracking.dashboard.portal.domain;

import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@Table(schema = "portal", name = "user_activity", indexes = {
	@Index(name = "user_activity_sso_user_id", columnList = "sso_user_id"),
	@Index(name = "user_activity_trail_type", columnList = "activity_type")
})
@Entity
public class UserActivity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 32)
	private String id;

	@Column(name = "sso_user_id", length = 32, nullable = false)
	private String ssoUserId;
	
	@Column(name = "user_full_name", length = 64, nullable = false)
	private String userFullName;

	@Enumerated(EnumType.STRING)
	@Column(name = "activity_type", length = 32, nullable = false)
	private ActivityType activityType;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "activity_timestamp")
	private Date activityTimestamp;
	
	@Column(name = "reference_name", length = 64)
	private String referenceName;
	
	@Column(name = "reference_value", length = 64)
	private String referenceValue;

	protected UserActivity() {
	}

	public UserActivity(String ssoUserId, String userFullName, ActivityType activityType, Date activityTimestamp) {
		this.id = IDGen.generate();
		this.ssoUserId = ssoUserId;
		this.userFullName = userFullName;
		this.activityType = activityType;
		this.activityTimestamp = activityTimestamp;
	}

	public String getId() {
		return id;
	}

	public String getSsoUserId() {
		return ssoUserId;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public Date getActivityTimestamp() {
		return activityTimestamp;
	}

	public String getReferenceName() {
		return referenceName;
	}

	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	public String getReferenceValue() {
		return referenceValue;
	}

	public void setReferenceValue(String referenceValue) {
		this.referenceValue = referenceValue;
	}
	
	@Override
	public int hashCode() {
		int hash = 5;
		hash = 31 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UserActivity other = (UserActivity) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

}
