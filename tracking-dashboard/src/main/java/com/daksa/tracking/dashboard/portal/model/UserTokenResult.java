package com.daksa.tracking.dashboard.portal.model;

import com.daksa.tracking.dashboard.portal.domain.UserToken;



/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
public class UserTokenResult {

	private UserToken userToken;
	private String entryCode;

	public UserToken getUserToken() {
		return userToken;
	}

	public void setUserToken(UserToken userToken) {
		this.userToken = userToken;
	}

	public String getEntryCode() {
		return entryCode;
	}

	public void setEntryCode(String entryCode) {
		this.entryCode = entryCode;
	}

}
