package com.daksa.tracking.dashboard.portal.service;

import com.daksa.tracking.dashboard.portal.domain.Host;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Stateless
public class HostService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager entityManager;
	
	public void createHost(Host host) {
		entityManager.persist(host);
	}
	
	public Host editHost(Host host) {
		return entityManager.merge(host);
	}
	
	public void deleteHost(Host host) {
		entityManager.remove(entityManager.merge(host));
	}
	
}
