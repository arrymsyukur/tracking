package com.daksa.tracking.dashboard.portal.domain;

/**
 *
 * @author DS-PRG-21
 */
public enum UserStatus {
	ACTIVE, BLOCKED, PENDING, LOCKED, REJECTED, EXPIRED
}
