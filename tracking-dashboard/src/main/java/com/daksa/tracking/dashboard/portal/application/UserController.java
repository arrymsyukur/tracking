package com.daksa.tracking.dashboard.portal.application;

import com.daksa.tracking.dashboard.portal.domain.SsoUser;
import com.daksa.tracking.dashboard.portal.domain.UserRole;
import com.daksa.tracking.dashboard.portal.domain.UserStatus;
import com.daksa.tracking.dashboard.portal.repository.SsoUserRepository;
import com.daksa.tracking.dashboard.portal.repository.UserRoleRepository;
import com.daksa.tracking.dashboard.portal.service.UserService;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class UserController implements Serializable {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Inject
    private FacesContext facesContext;
    @Inject
    private UserService userService;
    @Inject
    private UserRoleRepository userRoleRepository;
    @Inject
    private SsoUserRepository ssoUserRepository;
    @Inject
    private RequestContext requestContext;

    private List<SelectItem> userStatusList = new ArrayList<>();
    private List<UserRole> roles;
    private SsoUser selectedUser;

    private String username;
    private String fullName;
    private String email;
    private String password;
    private String passwordConfirm;
    private List<String> roleIds;

    @PostConstruct
    public void init() {
        roles = userRoleRepository.findAll();
        userStatusList.clear();
        userStatusList.add(new SelectItem(UserStatus.ACTIVE, "ACTIVE"));
        userStatusList.add(new SelectItem(UserStatus.BLOCKED, "BLOCKED"));
        userStatusList.add(new SelectItem(UserStatus.EXPIRED, "EXPIRED"));
        userStatusList.add(new SelectItem(UserStatus.LOCKED, "LOCKED"));
        userStatusList.add(new SelectItem(UserStatus.PENDING, "PENDING"));
        userStatusList.add(new SelectItem(UserStatus.REJECTED, "REJECTED"));
        selectedUser = new SsoUser(null, null, null, null, null);
    }

    public void registerUser() {
        SsoUser existingUser = ssoUserRepository.findUserByEmail(email);
        boolean valid = true;
        if (existingUser != null) {
            facesContext.addMessage("add-user-form:email", new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "E-mail already exists"));
            valid = false;
        }
        if (valid) {
            List<UserRole> userRoles = new ArrayList<>();
            if (roleIds != null && !roleIds.isEmpty()) {
                for (String roleId : roleIds) {
                    UserRole r = null;
                    for (UserRole role : roles) {
                        if (role.getId().equals(roleId)) {
                            r = role;
                            break;
                        }
                    }
                    if (r != null) {
                        userRoles.add(r);
                    }
                }
            }
            SsoUser user = new SsoUser(username, fullName, email, userRoles, new Date());
            try {
                userService.registerUser(user);
                resetRegisterUser();
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "User has been registered"));
            } catch (EndpointException | JsonProcessingException e) {
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Register User", e.getMessage()));
            }
        }
    }

    public void resetRegisterUser() {
        requestContext.reset("add-user-form");
        username = null;
        fullName = null;
        email = null;
        password = null;
        passwordConfirm = null;
        roleIds = null;
        init();
    }

    public void updateUser() {
        SsoUser existingUser = ssoUserRepository.findUserByEmail(selectedUser.getEmail());
        boolean valid = true;
        if (existingUser != null && !existingUser.equals(selectedUser)) {
            facesContext.addMessage("editUserForm:email", new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "E-mail already exists"));
            valid = false;
        }

        if (valid) {
            if (roleIds != null && !roleIds.isEmpty()) {
                List<UserRole> userRoles = new ArrayList<>();
                for (String roleId : roleIds) {
                    UserRole r = null;
                    for (UserRole role : roles) {
                        if (role.getId().equals(roleId)) {
                            r = role;
                            break;
                        }
                    }
                    if (r != null) {
                        userRoles.add(r);
                    }
                }
                selectedUser.setRoles(userRoles);
            }
            try {
                userService.updateUser(selectedUser);
                resetUpdateUser();
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "User has been updated"));
            } catch (Exception e) {
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Update User", e.getMessage()));
            }
        }
    }

    public void resetUpdateUser() {
        requestContext.reset("editUserForm");
        selectedUser = null;
        username = null;
        fullName = null;
        email = null;
        password = null;
        passwordConfirm = null;
        roleIds = null;
    }

    @Transactional
    public void active() {
        SsoUser existingUser = ssoUserRepository.find(selectedUser.getId());
        if (existingUser == null) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "User tidak ada"));
        } else {
            try {
                existingUser.setStatus(UserStatus.ACTIVE);
                ssoUserRepository.update(existingUser);

                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Berhasil update data"));
            } catch (Exception e) {
                LOG.debug("Error : " + e.getMessage(), e);
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Gagal update data"));
            }

        }
    }

    @Transactional
    public void block() {
        SsoUser existingUser = ssoUserRepository.find(selectedUser.getId());
        if (existingUser == null) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "User tidak ada"));
        } else {
            try {
                existingUser.setStatus(UserStatus.BLOCKED);
                ssoUserRepository.update(existingUser);

                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Berhasil update data"));
            } catch (Exception e) {
                LOG.debug("Error : " + e.getMessage(), e);
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Gagal update data"));
            }

        }
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        roles.clear();
        roleIds.forEach((roleId) -> {
            roles.add(userRoleRepository.find(roleId));
        });
        this.roleIds = roleIds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public SsoUser getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(SsoUser selectedUser) {
        this.selectedUser = selectedUser;
        roleIds = new ArrayList<>();
        selectedUser.getRoles().forEach((role) -> {
            roleIds.add(role.getId());
        });
    }

    public String userRolesToString(SsoUser user) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        if (user.getRoles() != null && !user.getRoles().isEmpty()) {
            for (UserRole role : user.getRoles()) {
                if (first) {
                    first = false;
                } else {
                    sb.append(", ");
                }
                sb.append(role.getName());
            }
        }
        return sb.toString();
    }

    public List<SelectItem> getUserStatusList() {
        return userStatusList;
    }

    public void setUserStatusList(List<SelectItem> userStatusList) {
        this.userStatusList = userStatusList;
    }

}
