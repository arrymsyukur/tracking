package com.daksa.tracking.dashboard.infrastructure;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan
 */
@ApplicationScoped
@Named(value = "web")
public class WebConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(WebConfig.class);

    private TimeZone timeZone;
    private Locale locale;
    private String decimalFormat;
    private String dateOutputFormat;
    private String dateInputFormat;
    private String timestampOutputFormat;
    private String timestampInputFormat;
    private String editorControls;
    private String tablePaginatorPosition;
    private String tablePaginatorTemplate;
    private String tableRowPerPageTemplate;
    private final int decimalPlaces = 0;
    private final int maxFractionDigit = 2;
    private final int minFractionDigit = 0;
    private final String thousandSeparator = ",";
    private final String decimalSeparator = ".";
    private String version;
    private String buildTimestamp;
    private Date lastDateInYear;
    private Date tomorow;      

    //private static final Map<String, CaptchaAccess> captchaMap = new HashMap<>();
    @PostConstruct
    public void init() {
        LOG.info("init");
        Properties properties = new Properties();
        try (InputStream input = getClass().getClassLoader().getResourceAsStream("web.properties")) {
            properties.load(input);

            String timeZoneProp = properties.getProperty("timeZone");
            if (timeZoneProp != null && !timeZoneProp.isEmpty()) {
                timeZone = TimeZone.getTimeZone(timeZoneProp);
            } else {
                timeZone = TimeZone.getDefault();
            }

            String localeProp = properties.getProperty("locale", "en_US");
            String[] localePropParts = localeProp.split("_");
            locale = new Locale(localePropParts[0], localePropParts[1]);
            version = properties.getProperty("build.version");
            buildTimestamp = properties.getProperty("build.timestamp");

            decimalFormat = properties.getProperty("decimalFormat", "#,###.##");
            dateOutputFormat = properties.getProperty("dateOutputFormat", "dd-MMM-yyyy");
            dateInputFormat = properties.getProperty("dateInputFormat", "dd-MMM-yyyy");
            timestampOutputFormat = properties.getProperty("timestampOutputFormat", "dd-MMM-yyyy HH:mm");
            timestampInputFormat = properties.getProperty("timestampInputFormat", "dd-MMM-yyyy HH:mm");

            String defaultEditorControls = "bold"
                    + " italic"
                    + " underline"
                    + " strikethrough"
                    + " font"
                    + " size"
                    + " color"
                    + " highlight"
                    + " bullets"
                    + " numbering"
                    + " alignleft"
                    + " center"
                    + " alignright"
                    + " justify"
                    + " image"
                    + " link"
                    + " unlink"
                    + " outdent"
                    + " indent";
            editorControls = properties.getProperty("editorControls", defaultEditorControls);

            tablePaginatorPosition = properties.getProperty("tablePaginatorPosition", "bottom");
            tablePaginatorTemplate = properties.getProperty("tablePaginatorTemplate", "{CurrentPageReport} {FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown}");
            tableRowPerPageTemplate = properties.getProperty("tableRowPerPageTemplate", "20,30,40,50,100");

            lastDateInYear = new DateTime().dayOfYear().withMaximumValue().toDate();
            tomorow = new DateTime().plusDays(1).toDate();
        } catch (IOException e) {
            LOG.error("Cannot load properties: {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getDecimalFormat() {
        return decimalFormat;
    }

    public String getDateOutputFormat() {
        return dateOutputFormat;
    }

    public String getDateInputFormat() {
        return dateInputFormat;
    }

    public String getTimestampOutputFormat() {
        return timestampOutputFormat;
    }

    public String getTimestampInputFormat() {
        return timestampInputFormat;
    }

    public String getEditorControls() {
        return editorControls;
    }

    public String getTablePaginatorPosition() {
        return tablePaginatorPosition;
    }

    public int getDecimalPlaces() {
        return decimalPlaces;
    }

    public int getMaxFractionDigit() {
        return maxFractionDigit;
    }

    public int getMinFractionDigit() {
        return minFractionDigit;
    }

    public String getThousandSeparator() {
        return thousandSeparator;
    }

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public String getVersion() {
        return version;
    }

    public String getBuildTimestamp() {
        return buildTimestamp;
    }

    public Date getLastDateInYear() {
        return lastDateInYear;
    }

    public Date getTomorow() {
        return tomorow;
    }

    public String getTablePaginatorTemplate() {
        return tablePaginatorTemplate;
    }

    public String getTableRowPerPageTemplate() {
        return tableRowPerPageTemplate;
    }
        
    public String getYearCopiright() {
        Integer year = 2017;
        DateTime dateTime = new DateTime();
        if (year == dateTime.getYear()) {
            return year.toString();
        } else {
            return year + " - " + dateTime.getYear();
        }
    }
}
