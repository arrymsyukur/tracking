package com.daksa.tracking.dashboard.portal.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Cacheable(true)
@Table(schema = "portal", name = "config")
@Entity
public class Config implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(length = 32)
	@Enumerated(EnumType.STRING)
	private ConfigKey id;

	@Column(columnDefinition = "TEXT")
	private String value;

	public Config() {
	}

	public Config(ConfigKey id, String value) {
		this.id = id;
		this.value = value;
	}

	public ConfigKey getId() {
		return id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 89 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Config other = (Config) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

}
