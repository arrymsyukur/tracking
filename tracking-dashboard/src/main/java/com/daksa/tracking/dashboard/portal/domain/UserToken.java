package com.daksa.tracking.dashboard.portal.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.codec.digest.HmacUtils;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Table(schema = "portal", name = "user_token")
@Entity
public class UserToken implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 32)
	private String id;

	@ManyToOne(optional = false)
	@JoinColumn(name = "sso_user_id", nullable = false)
	private SsoUser user;

	@Column(name = "token_hash", length = 64, nullable = false)
	private String tokenHash;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_timestamp", nullable = false)
	private Date createdTimestamp;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expiry_timestamp", nullable = false)
	private Date expiryTimestamp;

	@Column(nullable = false)
	private boolean available;

	@Enumerated(EnumType.STRING)
	@Column(name = "token_type", length = 32)
	private UserTokenType tokenType;

	protected UserToken() {
	}

	public UserToken(SsoUser user, Date createdTimestamp, Date expiryTimestamp, String entryCode, UserTokenType tokenType) {
		this.id = UUID.randomUUID().toString().replace("-", "");
		this.user = user;
		this.createdTimestamp = createdTimestamp;
		this.expiryTimestamp = expiryTimestamp;
		this.tokenHash = HmacUtils.hmacSha256Hex(entryCode, id);
		this.tokenType = tokenType;
		this.available = true;
	}

	public void invalidate() {
		this.available = false;
	}

	public boolean isExpired(Date now) {
		return now.getTime() > expiryTimestamp.getTime();
	}

	public boolean isEntryCodeMatch(String entryCode) {
		String tokenHashGenerated = HmacUtils.hmacSha256Hex(entryCode, id);
		boolean match = tokenHashGenerated.equals(tokenHash);
		return match;
	}

	public String getId() {
		return id;
	}

	public SsoUser getUser() {
		return user;
	}

	public String getTokenHash() {
		return tokenHash;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public Date getExpiryTimestamp() {
		return expiryTimestamp;
	}

	public boolean isAvailable() {
		return available;
	}

	public UserTokenType getTokenType() {
		return tokenType;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 41 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UserToken other = (UserToken) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

}
