package com.daksa.tracking.dashboard.portal.repository;

import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@RequestScoped
public class SsoSessionRepository extends JpaRepository<SsoSession, String> {
	
	@Inject
	private EntityManager entityManager;
	
	public SsoSessionRepository() {
		super(SsoSession.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public void invalidateExpirySession(Date timestamp) {
		Query query = entityManager.createNamedQuery("BO.SsoSession.invalidateExpiredSession");
		query.setParameter("timestamp", timestamp);
		query.executeUpdate();
	}
	
}
