package com.daksa.tracking.dashboard.portal.application;

import com.daksa.tracking.dashboard.portal.domain.UserToken;
import com.daksa.tracking.dashboard.portal.exception.EmailNotFoundException;
import com.daksa.tracking.dashboard.portal.exception.EntryCodeNotValidException;
import com.daksa.tracking.dashboard.portal.repository.UserTokenRepository;
import com.daksa.tracking.dashboard.portal.service.UserService;
import java.io.Serializable;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class ResetPasswordController implements Serializable {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(ResetPasswordController.class);

    @Inject
    private FacesContext facesContext;
    @Inject
    private UserService userService;
    @Inject
    private UserTokenRepository userTokenRepository;

    private String email;
    private String token;
    private String entryCode;
    private UserToken userToken;
    private String password;
    private String passwordConfirm;

    public void init() {
        if (token != null) {
            UserToken t = userTokenRepository.find(token);
            if (t != null && t.isAvailable() && !t.isExpired(new Date())) {
                userToken = t;
            }
        }
    }

    public void resetPassword() {
        try {
            userService.resetPassword(email, new Date());
            facesContext.addMessage(null, new FacesMessage(
                    FacesMessage.SEVERITY_INFO, "Sucecss", "Password reset request has been sent. Please check your email!"));
            email = null;
        } catch (EmailNotFoundException e) {
            facesContext.addMessage("resetPasswordForm:email", new FacesMessage(
                    FacesMessage.SEVERITY_WARN, "Warning", "Email not found"));
        } catch (Exception e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
        }
    }

    public void completeResetPassword() {
        boolean valid = true;
        if (!passwordConfirm.equals(password)) {
            facesContext.addMessage("resetPasswordForm:passwordConfirm", new FacesMessage(
                    FacesMessage.SEVERITY_WARN, "Warn", "Password confirm is not match"));
            valid = false;
        }
        if (valid) {
            try {
                userService.completeResetPassword(userToken, entryCode, new Date(), password);
                userToken = null;
                entryCode = null;
                password = null;
                passwordConfirm = null;
                facesContext.addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_INFO, "Reset Password Success", "Your passsword has been reset successfully."));
            } catch (EntryCodeNotValidException e) {
                facesContext.addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_WARN, "Failed", "Verification failed. Please check your entry code!"));
            } catch (Exception e) {
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            }
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserToken getUserToken() {
        return userToken;
    }

    public String getEntryCode() {
        return entryCode;
    }

    public void setEntryCode(String entryCode) {
        this.entryCode = entryCode;
    }

    public boolean isShowResetPassword() {
        return userToken == null;
    }

    public boolean isShowCompleteResetPassword() {
        return userToken != null;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
