/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.dashboard.model.web;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author DS-PRG-13
 */
public class ShiftResponseModel {

    private String id;
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date startDateShift;
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date endDateShift;
    private String branchId;
    private BranchResponseModel branch;

    public ShiftResponseModel() {
    }

    public String getId() {
	return id;
    }

    public Date getStartDateShift() {
	return startDateShift;
    }

    public void setStartDateShift(Date startDateShift) {
	this.startDateShift = startDateShift;
    }

    public Date getEndDateShift() {
	return endDateShift;
    }

    public void setEndDateShift(Date endDateShift) {
	this.endDateShift = endDateShift;
    }

    public String getBranchId() {
	return branchId;
    }

    public void setBranchId(String branchId) {
	this.branchId = branchId;
    }

    public BranchResponseModel getBranch() {
	return branch;
    }

    public void setBranch(BranchResponseModel branch) {
	this.branch = branch;
	this.branchId = branch.getId();
    }

}
