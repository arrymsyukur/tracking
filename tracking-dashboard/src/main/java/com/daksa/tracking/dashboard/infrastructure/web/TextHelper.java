package com.daksa.tracking.dashboard.infrastructure.web;

import com.daksa.tracking.webutil.util.FormatStringUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Zulkhair Abdullah Daim
 */
@Named
@RequestScoped
public class TextHelper {

    private final static String PAGINATOR_TEMPLATE_ADVANCE = "{CurrentPageReport} {FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink} {RowsPerPageDropdown}";
    private final static String PAGINATOR_TEMPLATE_SIMPLE = "{FirstPageLink} {PreviousPageLink} {PageLinks} {NextPageLink} {LastPageLink}";

    public String toHourMinutes(Double hour) {
        if (hour != null) {
            long totalMinutes = Math.round(hour * 60);

            long h = totalMinutes / 60;
            long m = totalMinutes % 60;
            return h + " hours" + ((m > 0) ? ", " + m + " minutes" : "");
        } else {
            return "";
        }
    }

    public String toHourMinutes2(Double hour) {
        if (hour != null) {
            long totalMinutes = Math.round(hour * 60);

            long h = totalMinutes / 60;
            long m = totalMinutes % 60;
            return String.format("%02d", h) + ":" + String.format("%02d", m);
        } else {
            return "";
        }
    }

    public String toHourMinutes3(Double hour) {
        if (hour != null) {
            long totalSeconds = Math.round(hour * 3600);

            long h = totalSeconds / 3600;
            long m = (totalSeconds % 3600) / 60;
            long s = (totalSeconds % 3600) % 60;
            return String.format("%02d", h) + ":" + String.format("%02d", m) + ":" + String.format("%02d", s);
        } else {
            return "";
        }
    }

    public String booleanToCloseOpen(boolean b) {
        if (b) {
            return "Closed";
        } else {
            return "Opened";
        }
    }

    public String getPaginatorTemplateAdvance() {
        return PAGINATOR_TEMPLATE_ADVANCE;
    }

    public String getPaginatorTemplateSimple() {
        return PAGINATOR_TEMPLATE_SIMPLE;
    }

    public String getRowsPerPageTemplateBig() {
        int i = 5, delta = 5;
        boolean firstOption = true;
        StringBuilder sb = new StringBuilder();
        while (i <= 100) {
            if (firstOption) {
                firstOption = false;
            } else {
                sb.append(", ");
            }
            sb.append(i);
            i += delta;
        }
        return sb.toString();
    }

    public String getRowsPerPageTemplate() {
        int[] rows = new int[]{5, 10, 20, 30, 40, 100, 200};
        boolean firstOption = true;
        StringBuilder sb = new StringBuilder();
        for (int row : rows) {
            if (firstOption) {
                firstOption = false;
            } else {
                sb.append(", ");
            }
            sb.append(row);
        }
        return sb.toString();
    }

    public String booleanToYesNo(boolean bo) {

        if (bo) {
            return "Yes";
        } else {
            return "No";
        }
    }

    public String booleanToGender(boolean bo) {

        if (bo) {
            return "Laki Laki";
        } else {
            return "Perempuan";
        }
    }

    public SelectItem[] getYesNoOptions() {
        SelectItem[] options = new SelectItem[3];
        options[0] = new SelectItem("", "All");
        options[1] = new SelectItem("T", "Yes");
        options[2] = new SelectItem("F", "No");

        return options;
    }

    public SelectItem[] getCloseOpenOptions() {
        SelectItem[] options = new SelectItem[3];
        options[0] = new SelectItem("", "All");
        options[1] = new SelectItem("T", "Closed");
        options[2] = new SelectItem("F", "Opened");

        return options;
    }

    public static String generateLabelEnumFee(String fieldName) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < fieldName.length(); i++) {
            if (i == 0) { // huruf pertama di Upper Case
                sb.append(String.valueOf(fieldName.charAt(i)).toUpperCase());
            } else if (fieldName.charAt(i) >= 'A' && fieldName.charAt(i) <= 'Z') { // huruf yang camel Case
                sb.append(" ").append(String.valueOf(fieldName.charAt(i)).toUpperCase());
            } else { // huruf biasa
                sb.append(fieldName.charAt(i));
            }
        }
        return sb.toString().toUpperCase();
    }

    public String getInputNumeric() {
        return "if (event.which< 48 || event.which> 57) if (event.which != 8) if (event.which != 13) if(event.which != 0) return false;";
    }
    
    public String getInputReferenceNumber() {
        return "if (event.which< 48 || event.which> 57) if (event.which != 8) if (event.which != 13) if(event.which != 0) if(event.which !="+((int) 'c')+") if(event.which !="+((int) 'C')+") return false;";
    }
    
    public String getInputNumericSpace() {
        return "if (event.which< 48 || event.which> 57) if (event.which != 8) if (event.which != 13) if(event.which != 0) if (event.which != 32) return false;";
    }

    public String getInputDecimal() {
        return "if (event.which< 48 || event.which> 57) if (event.which != 8) if (event.which != 13) if(event.which != 0) if(event.which != 46) return false;";
    }

    public String getInputTelp() {
        return "if (event.which< 48 || event.which> 57) if (event.which != 8) if (event.which != 13) if(event.which != 0) if(event.which != 43) return false;";
    }

    public String getInputAlpha() {
        return "if (event.which< 65 || event.which> 90) if(event.which< 97 || event.which> 122) "
                + "if (event.which != 8) if(event.which != 0) if (event.which != 13) return false;";
    }

    public String getInputAlphaSpace() {
        return "if (event.which< 65 || event.which> 90) if(event.which< 97 || event.which> 122) "
                + "if (event.which != 32) if (event.which != 8) if(event.which != 0) if (event.which != 13) return false;";
    }

    public String getInputAlphaNumeric() {
        return "if (event.which< 48 || event.which> 57) if (event.which< 65 || event.which> 90) "
                + "if(event.which< 97 || event.which> 122) if (event.which != 8) if(event.which != 0) if (event.which != 13) return false;";
    }
    
    public String getInputAlphaNumericUppercase() {
        return "if (event.which< 48 || event.which> 57) if (event.which< 65 || event.which> 90) "
                + "if (event.which != 8) if(event.which != 0) if (event.which != 13) return false;";
    }
	
	public String getInputAlphaNumericUnderscore() {
        return "if (event.which< 48 || event.which> 57) if (event.which< 65 || event.which> 90) "
                + "if(event.which< 97 || event.which> 122) if (event.which != 8) if(event.which != 0) "
				+ "if (event.which != 95) if (event.which != 13) return false;";
    }

    public String getInputAlphaNumericSpace() {
        return "if (event.which< 48 || event.which> 57) if (event.which< 65 || event.which> 90) "
                + "if(event.which< 97 || event.which> 122) if (event.which != 8) if(event.which != 0) if (event.which != 32) if (event.which != 13) return false;";
    }

    public String getInputAlphaNumericSymbolSpace() {
        return "if (event.which< 48 || event.which> 57) if (event.which< 65 || event.which> 90) "
                + "if(event.which< 97 || event.which> 122) if (event.which != 8) if(event.which != 0) "
                + "if (event.which != 32) if (event.which != 40) if (event.which != 41) "
                + "if (event.which != 45) if (event.which != 46) if (event.which != 47) "
                + "if (event.which != 95) if (event.which != 44) if (event.which != 13) if (event.which != 38) return false;";
    }
    
    public String getInputAlphaNumericSymbolSpaceQuote() {
        return "if (event.which< 48 || event.which> 57) if (event.which< 65 || event.which> 90) "
                + "if(event.which< 97 || event.which> 122) if (event.which != 8) if(event.which != 0) "
                + "if (event.which != 32) if (event.which != 40) if (event.which != 41) "
                + "if (event.which != 45) if (event.which != 46) if (event.which != 47) "
                + "if (event.which != 95) if (event.which != 44) if (event.which != 13) if (event.which != 38) if (event.which != 39) return false;";
    }

    public String getInputEmail() {
        return "if (event.which< 48 || event.which> 57) if (event.which< 65 || event.which> 90) "
                + "if(event.which< 97 || event.which> 122) if (event.which != 8) "
                + "if(event.which != 0) if (event.which != 32) if (event.which != 45) "
                + "if (event.which != 46) if (event.which != 64) if (event.which != 95) if (event.which != 13) return false;";
    }

    public String getInputIpAddress() {
        return "if (event.which< 48 || event.which> 57) if (event.which != 8) if (event.which != 13) if(event.which != 0) if(event.which != 46) return false;";
    }

    public static boolean isInputNumeric(String string) {
        return StringUtils.isNumeric(string);
    }
    
     public static boolean isInputNumericSpace(String string) {
        return StringUtils.isNumericSpace(string);
    }

    public static boolean isInputTelp(String string) {
        return string.matches("^\\+[\\d]*$|^[\\d]*$");
    }

    public static boolean isInputAlpha(String string) {
        return StringUtils.isAlpha(string);
    }

    public static boolean isInputAlphaSpace(String string) {
        return StringUtils.isAlphaSpace(string);
    }

    public static boolean isInputAlphaNumeric(String string) {
        return StringUtils.isAlphanumeric(string);
    }
	
	 public static boolean isInputAlphaNumericUnderscore(String string) {
        return string.matches("[\\w\\d_]*");
    }

    public static boolean isInputAlphaNumericSpace(String string) {
        return StringUtils.isAlphanumericSpace(string);
    }
	
	 public static boolean isInputAlphaNumericSymbol(String string) {
        return string.matches("[\\w\\d()\\-_,./&]*");
    }

    public static boolean isInputAlphaNumericSymbolSpace(String string) {
        return string.matches("[\\w\\d()\\-_,./ &]*");
    }
    
    public static boolean isInputAlphaNumericSymbolSpaceQuote(String string) {
        return string.matches("[\\w\\d()\\-_,./ &']*");
    }

    public static boolean isInputEmail(String string) {
        return string.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    public static boolean isInputIpAddress(String string) {
        return string.matches("^\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}$");
    }
    
    public static int countUpperCase(String text) {
        int count = 0;
        if (text != null && !text.isEmpty()) {
            for (char c : text.toCharArray()) {
                if (Character.isUpperCase(c)) {
                    count++;
                }
            }
        }
        return count;
    }

    public static int countLowerCase(String text) {
        int count = 0;
        if (text != null && !text.isEmpty()) {
            for (char c : text.toCharArray()) {
                if (Character.isLowerCase(c)) {
                    count++;
                }
            }
        }
        return count;
    }

    public static int countDigit(String text) {
        int count = 0;
        if (text != null && !text.isEmpty()) {
            for (char c : text.toCharArray()) {
                if (Character.isDigit(c)) {
                    count++;
                }
            }
        }
        return count;
    }
    
    public static String toPrettyFormat(String jsonString) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(jsonString).getAsJsonObject();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyJson = gson.toJson(json);

        return prettyJson;
    }
    
    public String separateDash(String text) {
        return FormatStringUtil.separateDashString(text);
    }
}
