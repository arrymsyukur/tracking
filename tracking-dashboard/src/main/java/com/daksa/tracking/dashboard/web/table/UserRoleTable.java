package com.daksa.tracking.dashboard.web.table;

import com.daksa.tracking.dashboard.portal.domain.UserRole;
import com.daksa.tracking.webutil.persistence.LocalDataTable;
import java.util.List;
import java.util.Map;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class UserRoleTable extends LocalDataTable<UserRole> {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;

    public UserRoleTable() {
        super(UserRole.class);
    }

    @Override
    public List<UserRole> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        return super.load(first, pageSize, sortField, sortOrder, filters);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Object getRowKey(UserRole object) {
        return object.getId();
    }

    @Override
    public UserRole getRowData(String rowKey) {
        UserRole role = entityManager.find(UserRole.class, rowKey);
        return role;
    }
}
