package com.daksa.tracking.dashboard.application;

import com.daksa.tracking.dashboard.infrastructure.ConfigFileKey;
import com.daksa.tracking.dashboard.infrastructure.ConfigFileRepository;
import com.daksa.tracking.webutil.endpoint.RestEndpoint;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import org.eclipse.jetty.client.HttpClient;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Named("core")
@Dependent
public class CoreEndpoint extends RestEndpoint {

    private static final long serialVersionUID = 1L;
    @Inject
    private ConfigFileRepository configFileRepository;
    @Inject
    private HttpClient httpClient;

    @PostConstruct
    public void init() {
        super.initialize(httpClient, configFileRepository.getConfigValue(ConfigFileKey.TRACKING_CORE_URL), null);
    }

}
