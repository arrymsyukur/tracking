package com.daksa.tracking.dashboard.portal.application;

import com.daksa.tracking.dashboard.portal.exception.ActivationCodeNotValidException;
import com.daksa.tracking.dashboard.portal.exception.EmailNotFoundException;
import com.daksa.tracking.dashboard.portal.service.UserService;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class UserActivationController implements Serializable {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(UserActivationController.class);

    @Inject
    private FacesContext facesContext;
    @Inject
    private UserService userService;

    private String email;
    private String activationCode;
    private String password;
    private String passwordConfirm;

    public void activateUser() {
        boolean valid = true;
        if (!passwordConfirm.equals(password)) {
            facesContext.addMessage("userActivationForm:passwordConfirm", new FacesMessage(
                    FacesMessage.SEVERITY_WARN, "Warn", "Password confirm is not match"));
            valid = false;
        }
        if (valid) {
            try {
                userService.activateUser(email, activationCode, password);
                activationCode = null;
                password = null;
                passwordConfirm = null;
                facesContext.addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_INFO, "Sucecss", "Your account has been activated"));
            } catch (ActivationCodeNotValidException e) {
                facesContext.addMessage(null, new FacesMessage(
                        FacesMessage.SEVERITY_WARN, "Warning", "Activation code is not valid"));
            } catch (EmailNotFoundException e) {
                facesContext.addMessage("userActivationForm:email", new FacesMessage(
                        FacesMessage.SEVERITY_WARN, "Warning", "Email not found"));
            } catch (Exception e) {
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            }
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

}
