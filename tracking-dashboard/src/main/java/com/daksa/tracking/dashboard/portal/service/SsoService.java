package com.daksa.tracking.dashboard.portal.service;



import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import com.daksa.tracking.dashboard.portal.model.AccessToken;
import com.daksa.tracking.dashboard.portal.repository.SsoSessionRepository;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Dependent
public class SsoService implements Serializable {

	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(SsoService.class);

	@Inject
	private SsoSessionRepository ssoSessionRepository;
	
	public AccessToken createAccessToken(String callback) {
		LOG.info("createAccessToken");
		AccessToken accessToken = new AccessToken();
		accessToken.setCallback(callback);
		accessToken.setToken(generateToken());
		//accessTokenCache.put(accessToken.getToken(), accessToken);
		return accessToken;
	}

	public AccessToken getAccessToken(String token) {
		LOG.info("getAccessToken (token: '{}')", token);
		//AccessToken accessToken = accessTokenCache.get(token);
		return null;
	}

	@Transactional
	public void createSession(SsoSession ssoSession, Date timestamp) {
		LOG.info("createSession (ssoSessionId: '{}')", ssoSession.getId());
		ssoSessionRepository.invalidateExpirySession(timestamp);
		ssoSessionRepository.store(ssoSession);
	}
	
	@Transactional
	public void invalidateSession(String ssoSessionId) {
		LOG.info("invalidateSession (ssoSessionId: '{}')", ssoSessionId);
		SsoSession ssoSession = ssoSessionRepository.find(ssoSessionId);
		if (ssoSession != null) {
			ssoSessionRepository.delete(ssoSession);
		}
	}

	@Transactional
	public SsoSession getSession(String ssoSessionId, Date timestamp) {
		LOG.info("getSession (ssoSessionId: '{}')", ssoSessionId);
		SsoSession ssoSession = ssoSessionRepository.find(ssoSessionId);
		if (ssoSession != null) {
			if (ssoSession.isExpiry(timestamp)) {
				ssoSessionRepository.delete(ssoSession);
				ssoSession = null;
			} else {
				ssoSession.setLastAccessTimestamp(new Date());
				ssoSessionRepository.update(ssoSession);
			}
		}
		return ssoSession;
	}

	private String generateToken() {
		return UUID.randomUUID().toString().replace("-", "");
	}
}
