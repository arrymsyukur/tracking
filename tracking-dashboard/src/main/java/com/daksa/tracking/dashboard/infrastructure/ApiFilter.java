package com.daksa.tracking.dashboard.infrastructure;


import com.daksa.tracking.dashboard.portal.domain.Host;
import com.daksa.tracking.dashboard.portal.domain.HostStatus;
import com.daksa.tracking.dashboard.portal.repository.HostRepository;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.codec.digest.HmacUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@RequestScoped
public class ApiFilter implements Filter {

    private final static Logger LOG = LoggerFactory.getLogger(ApiFilter.class);
    private final static String AUTH_MODE_DA01 = "DA01";

    @Inject
    private HostRepository hostRepository;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        String resource = getResource(httpRequest);
        String method = httpRequest.getMethod();
        LOG.info("doFilter {} {}", method, resource);

        Enumeration headerNames = httpRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            LOG.debug("header: {} - {}", headerName, httpRequest.getHeader(headerName));
        }

        boolean authorize = false;
        String authorization = httpRequest.getHeader("Authorization");
        LOG.debug("Authorization: {}", authorization);
        String apiKey = null, signature = null;
        if (authorization != null) {
            String[] authPart = authorization.split("\\s");
            if (authPart.length >= 2) {
                String[] authPart2 = authPart[1].split(":");
                String authMode = authPart[0];
                if (authMode.equals(AUTH_MODE_DA01) && authPart2 != null && authPart2.length >= 2) {
                    apiKey = authPart2[0];
                    signature = authPart2[1];
                }
            }
        }
        Host host = hostRepository.findByApiKey(apiKey);
        String hostId = null;
        if (signature != null && host != null && host.getStatus() == HostStatus.ACTIVE && host.getSecretKey() != null) {
            String generatedSignature = generateSignature(httpRequest, resource, host.getSecretKey());
            authorize = generatedSignature.equals(signature);
            hostId = host.getId();
        } else {
            LOG.warn("No signature or host for apiKey '{}'", apiKey);
        }

        if (authorize) {
            httpRequest.setAttribute("hostId", hostId);
            chain.doFilter(request, response);
        } else {
            httpResponse.setHeader("RC", "63");
            httpResponse.setStatus(403);
        }
    }

    private String getResource(HttpServletRequest servletRequest) {
        if (servletRequest.getContextPath() != null && !servletRequest.getContextPath().equals("/")) {
            return servletRequest.getRequestURI().substring(servletRequest.getContextPath().length());
        } else {
            return servletRequest.getRequestURI();
        }
    }

    private String generateSignature(HttpServletRequest httpRequest, String canonicalPath, String password) throws IOException {
        StringBuilder signatureBuilder = new StringBuilder();
        signatureBuilder.append(httpRequest.getMethod()).append("\n");
        if (httpRequest.getContentLength() > 0) {
            signatureBuilder.append(contentMD5(httpRequest)).append("\n");
        }
        if (httpRequest.getContentType() != null) {
            signatureBuilder.append(httpRequest.getContentType()).append("\n");
        }
        signatureBuilder.append(httpRequest.getHeader("Date")).append("\n");
        signatureBuilder.append(canonicalPath);
        String digest = signatureBuilder.toString();
        String signature = Base64.encodeBase64String(HmacUtils.hmacSha1(password, digest));
        return signature;
    }

    private String contentMD5(HttpServletRequest httpRequest) throws IOException {
        try (InputStream input = httpRequest.getInputStream()) {
            String contentHashed = DigestUtils.md5Hex(input);
            return contentHashed;
        }
    }

    @Override
    public void destroy() {
    }
}
