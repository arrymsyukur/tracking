package com.daksa.tracking.dashboard.portal.service;


import com.daksa.tracking.dashboard.portal.domain.UserPermission;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Stateless
public class UserPermissionService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager entityManager;
	
	public void updatePermission(UserPermission permission) {
		entityManager.merge(permission);
	}
	
	public void createPermission(UserPermission permission) {
		entityManager.persist(permission);
	}
	
	public void deletePermission(UserPermission permission) {
		entityManager.remove(entityManager.merge(permission));
	}
}
