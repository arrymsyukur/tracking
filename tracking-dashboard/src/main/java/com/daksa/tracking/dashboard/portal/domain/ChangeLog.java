package com.daksa.tracking.dashboard.portal.domain;

import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Table(schema = "portal", name = "change_log",  indexes = {
	@Index(name = "change_log_user_activity_id", columnList = "user_activity_id"),
	@Index(name = "change_log_user_label", columnList = "label")
})
@Entity
public class ChangeLog implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 32)
	private String id;
	
	@XmlTransient
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "user_activity_id", nullable = false)
	private UserActivity userActivity;
	
	@Column(name = "user_activity_id", insertable = false, updatable = false)
	private String userActivityId;
	
	@Column(name = "label", length = 64)
	private String label;
	
	@Column(name = "old_value", columnDefinition = "TEXT")
	private String oldValue;
	
	@Column(name = "new_value", columnDefinition = "TEXT")
	private String newValue;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "value_type", length = 64)
	private ValueType valueType;

	protected ChangeLog() {
	}
	
	public <T> ChangeLog(UserActivity userActivity, String label, T oldValueObj, T newValueObj) {
		this.id = IDGen.generate();
		this.userActivity = userActivity;
		this.userActivityId = userActivity.getId();
		this.label = label;
		
		T valueObj = null;
		if (oldValueObj != null) {
			valueObj = oldValueObj;
		} else if (newValueObj != null) {
			valueObj = newValueObj;
		}
		
		if (valueObj != null) {
			if (valueObj instanceof Number) {
				this.valueType = ValueType.NUMBER;
				this.oldValue = toStringIfAny(oldValueObj);
				this.newValue = toStringIfAny(newValueObj);
			} else if (valueObj instanceof Boolean) {
				this.valueType = ValueType.BOOLEAN;
				this.oldValue = toStringIfAny(oldValueObj);
				this.newValue = toStringIfAny(newValueObj);
			} else if (valueObj instanceof Date) {
				this.valueType = ValueType.DATE;
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				if (oldValueObj != null) {
					this.oldValue = dateFormat.format((Date) oldValueObj);
				} 
				if (newValueObj != null) {
					this.newValue = dateFormat.format((Date) newValueObj);
				} 
			} else {
				this.valueType = ValueType.STRING;
				this.oldValue = toStringIfAny(oldValueObj);
				this.newValue = toStringIfAny(newValueObj);
			}
		}
	}
	
	private String toStringIfAny(Object obj) {
		if (obj != null) {
			return obj.toString();
		} else {
			return null;
		}
	}

	public String getId() {
		return id;
	}

	public UserActivity getUserActivity() {
		return userActivity;
	}

	public String getUserActivityId() {
		return userActivityId;
	}

	public String getLabel() {
		return label;
	}

	public String getOldValue() {
		return oldValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public ValueType getValueType() {
		return valueType;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 17 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ChangeLog other = (ChangeLog) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}
	
}
