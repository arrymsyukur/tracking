package com.daksa.tracking.dashboard.infrastructure;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public enum ConfigFileKey {
    TRACKING_CORE_URL("core.url");

    private final String configId;

    private ConfigFileKey(String configId) {
        this.configId = configId;
    }

    public String getConfigId() {
        return configId;
    }
}
