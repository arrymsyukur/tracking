package com.daksa.tracking.dashboard.web.controller;

import com.daksa.tracking.dashboard.application.CoreService;
import com.daksa.tracking.dashboard.model.web.OfficerTypeModel;
import com.daksa.tracking.dashboard.model.web.SecurityOfficerRegistrationModel;
import com.daksa.tracking.dashboard.model.web.SecurityOfficerResponseModel;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Named
@ViewScoped
public class OfficerWebController implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(OfficerWebController.class);
    @Inject
    private CoreService coreService;
    @Inject
    private FacesContext facesContext;

    private SecurityOfficerRegistrationModel registrationModel;
    private SecurityOfficerResponseModel editModel;
    private String reinputPassword;
    private Boolean safe;
    private final Date today = new Date();
    private List<OfficerTypeModel> officerType;

    public void init() {
        try {
            clear();
            officerType = coreService.findAllOfficerType();
        } catch (IOException | EndpointException ex) {
            LOG.debug(ex.getMessage());
        }
    }

    public void clear() {
        registrationModel = new SecurityOfficerRegistrationModel();
        editModel = new SecurityOfficerResponseModel();
        reinputPassword = null;
    }

    public void checkPassword() {
        if (registrationModel.getPassword() != null && !registrationModel.getPassword().equals(reinputPassword)) {
            facesContext.addMessage("main-form:inputPw", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Password not same"));
            safe = false;
        } else {
            safe = true;
        }
    }

    public void addOfficer() {
        try {
            coreService.addOfficer(registrationModel);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Add Officer Success"));
            clear();
        } catch (EndpointException ex) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
        } catch (JsonProcessingException ex) {
            LOG.error("Error Parsing Json : " + ex.getMessage());
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Add Officer Failed"));
        }
    }

    public void updateOfficer() {
        try {
            coreService.updateOfficer(editModel);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Edit Officer Success"));
            clear();
        } catch (EndpointException ex) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
        } catch (JsonProcessingException ex) {
            LOG.error("Error Parsing Json : " + ex.getMessage());
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Edit Officer Failed"));
        }
    }

    public void setSelectedOfficer(SecurityOfficerResponseModel model) {
        try {
            editModel = coreService.findOfficerById(model.getId());
        } catch (EndpointException | IOException e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
            init();
        }

    }

    public SecurityOfficerRegistrationModel getRegistrationModel() {
        return registrationModel;
    }

    public void setRegistrationModel(SecurityOfficerRegistrationModel registrationModel) {
        this.registrationModel = registrationModel;
    }

    public SecurityOfficerResponseModel getEditModel() {
        return editModel;
    }

    public void setEditModel(SecurityOfficerResponseModel editModel) {
        this.editModel = editModel;
    }

    public Date getToday() {
        return today;
    }

    public String getReinputPassword() {
        return reinputPassword;
    }

    public void setReinputPassword(String reinputPassword) {
        this.reinputPassword = reinputPassword;
    }

    public Boolean getSafe() {
        return safe;
    }

    public void setSafe(Boolean safe) {
        this.safe = safe;
    }

    public List<OfficerTypeModel> getOfficerType() {
        return officerType;
    }
}
