package com.daksa.tracking.dashboard.portal.application;



import com.daksa.tracking.dashboard.portal.domain.Host;
import com.daksa.tracking.dashboard.portal.domain.HostStatus;
import com.daksa.tracking.dashboard.portal.repository.HostRepository;
import com.daksa.tracking.dashboard.portal.service.HostService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class HostController implements Serializable {

	private static final long serialVersionUID = 1L;
	private final static Logger LOG = LoggerFactory.getLogger(HostController.class);

	private static final char[] CODES = new char[10 + 26 + 26]; // numeric + alpha (smallcaps) + alpha (uppercaps)

	static {
		int j = 0;
		for (char i = '0'; i <= '9'; i++) {
			CODES[j++] = i;
		}
		for (char i = 'A'; i <= 'Z'; i++) {
			CODES[j++] = i;
		}
		for (char i = 'a'; i <= 'z'; i++) {
			CODES[j++] = i;
		}
	}

	@Inject
	private FacesContext facesContext;	
	@Inject
	private HostService hostService;
	@Inject
	private HostRepository hostRepository;

	private Host selectedHost;
	private boolean editorVisible;

	private String hostId;
	private String hostName;
	private String hostUrl;
	private String apiKey;
	private String secretKey;
	private int hostOrderNumber;

	@PostConstruct
	public void init() {
		editorVisible = false;
	}

	public void showHostEditor(Host host) {
		selectedHost = host;
		editorVisible = true;
	}

	public void editHost() {
		LOG.info("updateHost");
		boolean valid = true;
		Host existingHost = hostRepository.findByApiKey(selectedHost.getApiKey());
		if (existingHost != null && !existingHost.equals(selectedHost)) {
			facesContext.addMessage("hostForm:apiKey", new FacesMessage(
					FacesMessage.SEVERITY_WARN, "Warning", "API Key already exists"));
			valid = false;
		}
		if (valid) {
			try {
				selectedHost = hostService.editHost(selectedHost);
				facesContext.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO, "Success", "Host '" + selectedHost.getName() + "' has been updated successfully."));
				closeHostEditor();
			} catch (Exception e) {
				facesContext.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Error Update Host", e.getMessage()));
				LOG.error(e.getMessage(), e);
			}
		}
	}

	public void resetHostCreator() {
		hostId = null;
		hostName = null;
		hostUrl = null;
		apiKey = null;
		secretKey = null;
		hostOrderNumber = 0;
		//requestContext.reset("hostForm");
	}

	public void closeHostEditor() {
		selectedHost = null;
		editorVisible = false;
	}

	public void createHost() {
		LOG.info("createHost");
		boolean valid = true;
		Host hostByApiKey = hostRepository.findByApiKey(apiKey);
		if (hostByApiKey != null) {
			facesContext.addMessage("hostForm:apiKey", new FacesMessage(
					FacesMessage.SEVERITY_WARN, "Warning", "API Key already exists"));
			valid = false;
		}
		Host hostById = hostRepository.find(hostId);
		if (hostById != null) {
			facesContext.addMessage("hostForm:hostId", new FacesMessage(
					FacesMessage.SEVERITY_WARN, "Warning", "Host ID already exists"));
			valid = false;
		}
		if (valid) {
			try {
				Host host = new Host();
				host.setId(hostId);
				host.setName(hostName);
				host.setApiKey(apiKey);
				host.setSecretKey(secretKey);
				host.setStatus(HostStatus.ACTIVE);
				host.setOrderNumber(hostOrderNumber);
				host.setUrl(hostUrl);
				hostService.createHost(host);

				resetHostCreator();
				facesContext.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_INFO, "Success", "Host '" + host.getName() + "' has been created successfully."));
			} catch (Exception e) {
				facesContext.addMessage(null, new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Error Create Host", e.getMessage()));
				LOG.error(e.getMessage(), e);
			}
		}
	}

	public void deleteHost(Host host) {
		LOG.info("deleteHost");
		try {
			hostService.deleteHost(host);
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_INFO, "Success", "Host '" + host.getName() + "' has been deleted successfully."));
		} catch (Exception e) {
			facesContext.addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Error Create Host", e.getMessage()));
			LOG.error(e.getMessage(), e);
		}
	}

	public List<SelectItem> getHostStatusOptions() {
		List<SelectItem> items = new ArrayList<>();
		items.add(new SelectItem("", "--- Select ---"));
		for (HostStatus status : HostStatus.values()) {
			items.add(new SelectItem(status));
		}
		return items;
	}

	public Host getSelectedHost() {
		return selectedHost;
	}

	public boolean isEditorVisible() {
		return editorVisible;
	}

	public boolean isTableVisible() {
		return !isEditorVisible();
	}

	private String randomizeKey(int length) {
		Random random = new Random();
		StringBuilder key = new StringBuilder(length);
		int n = CODES.length;
		for (int i = 0; i < length; i++) {
			char code = CODES[random.nextInt(n)];
			key.append(code);
		}
		return key.toString();
	}

	public void generateApiKey() {
		if (selectedHost != null) {
			selectedHost.setApiKey(randomizeKey(32));
		} else {
			apiKey = randomizeKey(32);
		}
	}

	public void generateSecretKey() {
		if (selectedHost != null) {
			selectedHost.setSecretKey(randomizeKey(64));
		} else {
			secretKey = randomizeKey(64);
		}
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostUrl() {
		return hostUrl;
	}

	public void setHostUrl(String hostUrl) {
		this.hostUrl = hostUrl;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public int getHostOrderNumber() {
		return hostOrderNumber;
	}

	public void setHostOrderNumber(int hostOrderNumber) {
		this.hostOrderNumber = hostOrderNumber;
	}

}
