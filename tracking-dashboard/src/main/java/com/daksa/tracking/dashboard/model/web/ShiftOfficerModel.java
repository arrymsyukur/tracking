package com.daksa.tracking.dashboard.model.web;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public class ShiftOfficerModel {

    private String id;
    private SecurityOfficerResponseModel securityOfficer;
    private String officerId;
    private String shiftId;
    private String branchName;
    private Boolean checked;
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date createdTimestamp;
    @XmlJavaTypeAdapter(JsonTimestampAdapter.class)
    private Date checkingTimestamp;

    public ShiftOfficerModel() {
    }

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public SecurityOfficerResponseModel getSecurityOfficer() {
	return securityOfficer;
    }

    public void setSecurityOfficer(SecurityOfficerResponseModel securityOfficer) {
	this.securityOfficer = securityOfficer;
    }

    public String getOfficerId() {
	return officerId;
    }

    public void setOfficerId(String officerId) {
	this.officerId = officerId;
    }

    public String getShiftId() {
	return shiftId;
    }

    public void setShiftId(String shiftId) {
	this.shiftId = shiftId;
    }

    public String getBranchName() {
	return branchName;
    }

    public void setBranchName(String branchName) {
	this.branchName = branchName;
    }

    public Boolean getChecked() {
	return checked;
    }

    public void setChecked(Boolean checked) {
	this.checked = checked;
    }

    public Date getCreatedTimestamp() {
	return createdTimestamp;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
	this.createdTimestamp = createdTimestamp;
    }

    public Date getCheckingTimestamp() {
	return checkingTimestamp;
    }

    public void setCheckingTimestamp(Date checkingTimestamp) {
	this.checkingTimestamp = checkingTimestamp;
    }

}
