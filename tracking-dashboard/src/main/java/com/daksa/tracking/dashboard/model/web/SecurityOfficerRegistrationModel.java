package com.daksa.tracking.dashboard.model.web;

import com.daksa.tracking.webutil.json.JsonDateAdapter;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public class SecurityOfficerRegistrationModel {

    private String name;
    private String officerTypeId;
    @XmlJavaTypeAdapter(JsonDateAdapter.class)
    private Date birthDate;
    private BigDecimal salary;
    private String username;
    private String password;
    private String email;

    public SecurityOfficerRegistrationModel() {
    }

    public String getName() {
	return name;
    }

    public Date getBirthDate() {
	return birthDate;
    }

    public BigDecimal getSalary() {
	return salary;
    }

    public String getUsername() {
	return username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getEmail() {
	return email;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setBirthDate(Date birthDate) {
	this.birthDate = birthDate;
    }

    public void setSalary(BigDecimal salary) {
	this.salary = salary;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getOfficerTypeId() {
	return officerTypeId;
    }

    public void setOfficerTypeId(String officerTypeId) {
	this.officerTypeId = officerTypeId;
    }

}
