package com.daksa.tracking.dashboard.portal.repository;


import com.daksa.tracking.dashboard.portal.domain.Config;
import com.daksa.tracking.dashboard.portal.domain.ConfigKey;
import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Dependent
public class ConfigRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;

	public String getConfigValue(ConfigKey configKey) {
		Config config = entityManager.find(Config.class, configKey);
		if (config != null) {
			return config.getValue();
		} else {
			return null;
		}
	}

	public String getSsoCookieName() {
		return getConfigValue(ConfigKey.SSO_COOKIE_NAME);
	}

	public int getSsoCookieTimeoutMinutes() {
		return Integer.parseInt(getConfigValue(ConfigKey.SSO_COOKIE_TIMEOUT_MINUTES));
	}

	public String getHostId() {
		return getConfigValue(ConfigKey.HOST_ID);
	}

	public int getUserTokenExpiryMinutes() {
		return Integer.parseInt(getConfigValue(ConfigKey.USER_TOKEN_EXPIRY_MINUTES));
	}

	public String getBaseUrl() {
		return getConfigValue(ConfigKey.BASE_URL);
	}

}
