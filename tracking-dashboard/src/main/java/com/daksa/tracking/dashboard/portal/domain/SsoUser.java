package com.daksa.tracking.dashboard.portal.domain;

import com.daksa.tracking.webutil.json.JsonTimestampAdapter;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Table(schema = "portal", name = "sso_user", indexes = {
	@Index(name = "sso_user_username_idx", columnList = "username", unique = true),
	@Index(name = "sso_user_full_name_idx", columnList = "full_name", unique = false),
	@Index(name = "sso_user_email_idx", columnList = "email", unique = true)
})
@Entity
@NamedQueries({
	@NamedQuery(name = "BO.SsoUser.findByUsername", query = "SELECT s FROM SsoUser s WHERE s.username = :username"),
	@NamedQuery(name = "BO.SsoUser.findByEmail", query = "SELECT s FROM SsoUser s WHERE s.email = :email"),
	@NamedQuery(name = "BO.SsoUser.getUserHostList", query = "SELECT DISTINCT h FROM SsoUser u JOIN u.roles r JOIN r.permissions p JOIN p.host h WHERE u.id = :userId ORDER BY h.orderNumber ASC"),
	@NamedQuery(name = "BO.SsoUser.getUserPermissions", query = "SELECT p FROM SsoUser u JOIN u.roles r JOIN r.permissions p WHERE u.id = :userId"),
	@NamedQuery(name = "BO.SsoUser.getUserRoles", query = "SELECT r FROM SsoUser u JOIN u.roles r WHERE u.id = :userId"),
	@NamedQuery(name = "BO.SsoUser.getUserPermissionsByHost", query = "SELECT p FROM SsoUser u JOIN u.roles r JOIN r.permissions p WHERE u.id = :userId AND p.hostId = :hostId"),
	@NamedQuery(name = "BO.SsoUser.findByMenuAndHost", query = "SELECT u FROM SsoUser u JOIN u.roles r JOIN r.permissions p WHERE p.menu = :menu AND p.hostId = :hostId ")
})
public class SsoUser implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final int HASH_ITERATION = 2;
	private static final String HASH_ALGO = "SHA-256";

	@Id
	@Column(length = 32)
	private String id;

	@Column(name = "username", length = 64, nullable = false)
	private String username;

	@XmlTransient
	@Column(name = "password_hash", length = 64, nullable = false)
	private String passwordHash;

	@XmlTransient
	@Column(name = "password_salt", length = 32, nullable = false)
	private String passwordSalt;

	@XmlTransient
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(schema = "portal", name = "user_role_member",
			joinColumns = @JoinColumn(name = "sso_user_id"),
			inverseJoinColumns = @JoinColumn(name = "user_role_id"),
			indexes = {
				@Index(name = "user_role_member_ux", columnList = "sso_user_id, user_role_id", unique = true)})
	private List<UserRole> roles;

	@Column(name = "full_name", length = 64, nullable = false)
	private String fullName;

	@Column(name = "email", length = 64, nullable = false)
	private String email;

	@Enumerated(EnumType.STRING)
	@Column(name = "status", length = 16)
	private UserStatus status;
	
	@XmlJavaTypeAdapter(JsonTimestampAdapter.class)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registration_timestamp")
	private Date registrationTimestamp;

	protected SsoUser() {
		this.id = UUID.randomUUID().toString().replace("-", "");
	}

	public SsoUser(String username, 
			String fullName, 
			String email,
			List<UserRole> roles, 
			Date registrationTimestamp) {
		this();
		this.username = username;
		this.roles = roles;
		this.fullName = fullName;
		this.email = email;
		this.registrationTimestamp = registrationTimestamp;
		this.status = UserStatus.PENDING;
	}
	
	public SsoUser(String username, 
			String fullName, 
			String email, 
			String password, 
			List<UserRole> roles,
			Date registrationTimestamp) {
		this();
		this.username = username;
		this.roles = roles;
		this.fullName = fullName;
		this.email = email;
		this.registrationTimestamp = registrationTimestamp;
		this.status = UserStatus.PENDING;
		setPassword(password);
	}
	
	public boolean verifyPassword(String password) {        
		return hashPassword(password).equals(passwordHash);
	}

	private String hashPassword(String password) {
		try {
			byte[] saltBytes = Hex.decodeHex(passwordSalt.toCharArray());
			return hassPassword(password, saltBytes);
		} catch (DecoderException e) {
			throw new RuntimeException(e);
		}
	}

	private String hassPassword(String password, byte[] saltBytes) {
		try {
			byte[] passwordBytes = password.getBytes();
			MessageDigest digest = MessageDigest.getInstance(HASH_ALGO);
			digest.update(saltBytes);
			byte[] tokenHash = digest.digest(passwordBytes);
			int iterations = HASH_ITERATION - 1;
			for (int i = 0; i < iterations; i++) {
				digest.reset();
				tokenHash = digest.digest(tokenHash);
			}
			String tokenHashHex = Hex.encodeHexString(tokenHash);
			return tokenHashHex;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public final void setPassword(String password) {
		Random random = new Random();
		byte[] saltBytes = new byte[16];
		random.nextBytes(saltBytes);

		this.passwordHash = hassPassword(password, saltBytes);
		this.passwordSalt = Hex.encodeHexString(saltBytes);
	}

	public String getPasswordSalt() {
		return passwordSalt;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getId() {
		return id;
	}

	public List<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UserRole> roles) {
		this.roles = roles;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UserStatus getStatus() {
		return status;
	}

	public void setStatus(UserStatus status) {
		this.status = status;
	}

	public Date getRegistrationTimestamp() {
		return registrationTimestamp;
	}
	
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 17 * hash + Objects.hashCode(this.id);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final SsoUser other = (SsoUser) obj;
		if (!Objects.equals(this.id, other.id)) {
			return false;
		}
		return true;
	}

}
