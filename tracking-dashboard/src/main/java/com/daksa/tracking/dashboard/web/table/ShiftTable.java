/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.dashboard.web.table;

import com.daksa.tracking.dashboard.model.web.ShiftResponseModel;
import com.daksa.tracking.webutil.endpoint.Endpoint;
import com.daksa.tracking.webutil.endpoint.NamedLiteral;
import com.daksa.tracking.webutil.persistence.RestDataTable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.inject.Instance;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.model.SortOrder;

/**
 *
 * @author DS-PRG-13
 */
@Named
@ViewScoped
public class ShiftTable extends RestDataTable<ShiftResponseModel> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String CORE_ENDPOINT = "core";

    @Inject
    private Instance<Endpoint> endpointInstance;

    public ShiftTable() {
	super(ShiftResponseModel.class);
    }

    @Override
    public List<ShiftResponseModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
	if (sortField == null) {
	    sortField = "startDateShift";
	    sortOrder = SortOrder.DESCENDING;
	}

	return super.load(first, pageSize, sortField, sortOrder, filters);
    }

    @Override
    protected Endpoint getEndpoint() {
	return endpointInstance.select(new NamedLiteral(CORE_ENDPOINT)).get();
    }

    @Override
    protected String getResource() {
	return "/shift";
    }

    @Override
    protected Map<String, Object> getAdditionalParams() {
	return new HashMap<>();
    }

    @Override
    protected Map<String, Object> getAdditionalHeaders() {
	return null;
    }

}
