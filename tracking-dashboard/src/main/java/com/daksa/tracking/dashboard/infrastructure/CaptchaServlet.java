/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.dashboard.infrastructure;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Berry
 */
public class CaptchaServlet extends HttpServlet {
    private static final long serialVersionUID = 1490947492185481844L;
    
    @Inject
    private CaptchaManageBean captchaManageBean;
    
    @Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		final HttpSession session = req.getSession(false);
		final String token = session != null ? captchaManageBean.getToken(session) : null;
		if (token == null || captchaManageBean.isTokenUsed(session)) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND,
					"Captcha not found.");

			return;
		}
		captchaManageBean.setResponseHeaders(resp);
		captchaManageBean.markTokenUsed(session, true);
	}
}
