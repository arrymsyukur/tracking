package com.daksa.tracking.dashboard.portal.application;


import com.daksa.tracking.dashboard.portal.domain.SessionData;
import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import java.util.Iterator;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@RequestScoped
@SuppressWarnings("unchecked")
public class PageController {

    private final static Logger LOG = LoggerFactory.getLogger(PageController.class);

    @Inject
    private FacesContext facesContext;

    public boolean isActive(String page) {
        String viewId = facesContext.getViewRoot().getViewId();
        return viewId.startsWith(page);
    }

    public SsoSession getSsoSession() {
        return (SsoSession) facesContext.getExternalContext().getRequestMap().get("ssoSession");
    }

    public List<SessionData.Permission> getUserPermissions() {
        return (List<SessionData.Permission>) facesContext.getExternalContext().getRequestMap().get("userPermissions");
    }

    public List<SessionData.Host> getHostList() {
        return (List<SessionData.Host>) facesContext.getExternalContext().getRequestMap().get("hostList");
    }

    public String menuStyle(String page) {
        if (isActive(page)) {
            return "active";
        } else {
            return "";
        }
    }

    public boolean hasMenu(String menu) {
        boolean found = false;
        Iterator<SessionData.Permission> iterator = getUserPermissions().iterator();
        while (!found && iterator.hasNext()) {
            SessionData.Permission permission = iterator.next();
            if (permission.getMenu().equals("*")) {
                found = true;
            } else {
                found = permission.getMenu().equals(menu);
            }
        }
        return found;
    }

    public boolean hasOneMenu(List<String> menus) {
        boolean allow = false;
        Iterator<String> menuIterator = menus.iterator();
        while (!allow && menuIterator.hasNext()) {
            String menu = menuIterator.next();
            Iterator<SessionData.Permission> iterator = getUserPermissions().iterator();
            boolean hasMenu = false;
            while (!hasMenu && iterator.hasNext()) {
                SessionData.Permission permission = iterator.next();
                hasMenu = menu.equals(permission.getMenu());
            }
            if (hasMenu) {
                allow = true;
            }
        }
        return allow;
    }

    public boolean hasAllMenu(List<String> menus) {
        boolean allow = true;
        Iterator<String> menuIterator = menus.iterator();
        while (allow && menuIterator.hasNext()) {
            String menu = menuIterator.next();
            Iterator<SessionData.Permission> iterator = getUserPermissions().iterator();
            boolean hasMenu = false;
            while (!hasMenu && iterator.hasNext()) {
                SessionData.Permission permission = iterator.next();
                hasMenu = menu.equals(permission.getMenu());
            }
            if (!hasMenu) {
                allow = false;
            }
        }
        return allow;
    }

    public String avatarUrl(int size) {
        String url = null;
        SsoSession ssoSession = getSsoSession();
        if (ssoSession != null) {
            String emailMd5 = DigestUtils.md5Hex(ssoSession.getEmail());
            url = "https://www.gravatar.com/avatar/" + emailMd5 + "?s=" + size;
        }
        return url;
    }
}
