package com.daksa.tracking.dashboard.portal.model;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class ChangeLogModel<T> {
	
	private String label;
	private T oldValue;
	private T newValue;

	public ChangeLogModel() {
	}

	public ChangeLogModel(String label, T oldValue, T newValue) {
		this.label = label;
		this.oldValue = oldValue;
		this.newValue = newValue;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public T getOldValue() {
		return oldValue;
	}

	public void setOldValue(T oldValue) {
		this.oldValue = oldValue;
	}

	public T getNewValue() {
		return newValue;
	}

	public void setNewValue(T newValue) {
		this.newValue = newValue;
	}
	
}
