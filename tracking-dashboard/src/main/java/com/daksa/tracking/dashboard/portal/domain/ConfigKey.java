package com.daksa.tracking.dashboard.portal.domain;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
public enum ConfigKey {
    SSO_COOKIE_NAME,
    SSO_COOKIE_TIMEOUT_MINUTES,
    HOST_ID,
    USER_TOKEN_EXPIRY_MINUTES,
    BASE_URL,
    MANUAL_TRX_CODE,
    DOWNLOAD_LOCATION,
  
}
