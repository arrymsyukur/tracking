package com.daksa.tracking.dashboard.portal.domain;

import com.daksa.tracking.webutil.util.IDGen;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Table(schema = "portal", name = "user_role")
@NamedQueries({
    @NamedQuery(name = "BO.UserRole.getRolePermissions", query = "SELECT p FROM UserRole r JOIN r.permissions p WHERE r.id = :roleId")
    ,@NamedQuery(name = "BO.UserRole.findAll", query = "SELECT r FROM UserRole r")

})
@Entity
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(length = 32)
    private String id;

    @Column(length = 64)
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(schema = "portal", name = "user_role_permission",
            joinColumns = @JoinColumn(name = "user_role_id"),
            inverseJoinColumns = @JoinColumn(name = "user_permission_id"),
            indexes = {
                @Index(name = "user_role_permission_ux", columnList = "user_role_id, user_permission_id", unique = true)})
    private List<UserPermission> permissions;

    public UserRole() {
        this.id = IDGen.generate();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPermissions(List<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public List<UserPermission> getPermissions() {
        return permissions;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserRole other = (UserRole) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
