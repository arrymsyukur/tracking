package com.daksa.tracking.dashboard.portal.exception;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
public class ActivationCodeNotValidException extends Exception {
	
    private static final long serialVersionUID = 1L;
	
}
