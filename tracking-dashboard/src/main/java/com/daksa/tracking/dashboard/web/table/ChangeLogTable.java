package com.daksa.tracking.dashboard.web.table;

import com.daksa.tracking.dashboard.portal.domain.ChangeLog;
import com.daksa.tracking.webutil.persistence.LocalDataTable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class ChangeLogTable extends LocalDataTable<ChangeLog> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;
    private String userActivityId;
    private Boolean showData;

    public ChangeLogTable() {
        super(ChangeLog.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public Object getRowKey(ChangeLog object) {
        return object.getId();
    }

    @Override
    public ChangeLog getRowData(String rowKey) {
        ChangeLog changeLog = entityManager.find(ChangeLog.class, rowKey);
        return changeLog;
    }

    @Override
    public List<ChangeLog> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (showData) {
            if (userActivityId != null) {
                if (filters == null) {
                    filters = new HashMap<>();
                }
                filters.put("userActivityId", userActivityId);
            }
            return super.load(first, pageSize, sortField, sortOrder, filters);
        }
        return null;
    }

    public void setUserActivityId(String userActivityId) {
        this.userActivityId = userActivityId;
    }

    public void setShowData(Boolean showData) {
        this.showData = showData;
    }

}
