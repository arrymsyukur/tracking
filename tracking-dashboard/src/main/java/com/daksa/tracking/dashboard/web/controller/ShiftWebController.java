package com.daksa.tracking.dashboard.web.controller;

import com.daksa.tracking.dashboard.application.CoreService;
import com.daksa.tracking.dashboard.model.web.BranchResponseModel;
import com.daksa.tracking.dashboard.model.web.OfficerTypeModel;
import com.daksa.tracking.dashboard.model.web.SecurityOfficerResponseModel;
import com.daksa.tracking.dashboard.model.web.ShiftAssignModel;
import com.daksa.tracking.dashboard.model.web.ShiftOfficerModel;
import com.daksa.tracking.dashboard.model.web.ShiftResponseModel;
import com.daksa.tracking.dashboard.web.table.ShiftOfficerTable;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Named
@ViewScoped
public class ShiftWebController implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(ShiftWebController.class);

    @Inject
    private CoreService coreService;
    @Inject
    private FacesContext facesContext;
    @Inject
    private ShiftOfficerTable shiftOfficerTable;

    private ShiftResponseModel registrationModel;
    private ShiftResponseModel editModel;
    private List<BranchResponseModel> branches;
    private BranchResponseModel branchDetailModel;
    private List<SecurityOfficerResponseModel> officers;
    private List<OfficerTypeModel> officerType;
    private final Date today = new Date();
    private String selectedShiftId;
    private Date startDate;
    private Date endDate;

    public void init() {
	try {
	    clear();
	    branches = coreService.findAllBranch();
	    officerType = coreService.findAllOfficerType();
	} catch (IOException ex) {
	    LOG.error("Error Parsing Json : " + ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Load Branch Failed"));
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
	}

    }

    public void clear() {
	registrationModel = new ShiftResponseModel();
	editModel = new ShiftResponseModel();
	branchDetailModel = new BranchResponseModel();
	officers = new ArrayList<>();
	startDate = null;
	endDate = null;
    }

    public void addShift() {
	try {
	    coreService.addShift(registrationModel);
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Add Shift Success"));
	    clear();
	} catch (EndpointException ex) {
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
	} catch (JsonProcessingException ex) {
	    LOG.error("Error Parsing Json : " + ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Add Shift Failed"));
	}
    }

    public void updateShift() {
	try {
	    coreService.addShift(editModel);
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Update Shift Success"));
	    clear();
	} catch (EndpointException ex) {
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
	} catch (JsonProcessingException ex) {
	    LOG.error("Error Parsing Json : " + ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Update Shift Failed"));
	}
    }

    public void setSelectedShift(ShiftResponseModel model) {
	editModel = model;
	editModel.setBranch(findBranch(model.getId()));

    }

    public BranchResponseModel findBranch(String id) {
	try {
	    branchDetailModel = coreService.findBranchById(id);
	} catch (IOException ex) {
	    LOG.error("Error Parsing Json : " + ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Load Branch Failed"));
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
	}
	return branchDetailModel;
    }

    public void findOfficer(String shiftId) {
	try {
	    officers = coreService.findOfficerShift(shiftId);
	    selectedShiftId = shiftId;
	} catch (IOException ex) {
	    LOG.error("Error Parsing Json : " + ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Load Shift Officer Failed"));
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
	}
    }

    public void findShiftOfficer(String shiftId) {
	selectedShiftId = shiftId;
	startDate = null;
	endDate = null;
	shiftOfficerTable.setShiftId(shiftId);
    }

    public void searchShiftOfficer() {
	shiftOfficerTable.setShiftId(selectedShiftId);
	if (startDate != null && endDate == null) {
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Please Select End Date"));
	} else if (startDate.after(endDate)) {
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Wrong Start Date"));
	} else {
	    shiftOfficerTable.setStartDate(startDate);
	    shiftOfficerTable.setEndDate(endDate);
	}
    }

    public void assignOfficer() {
	try {
	    ShiftAssignModel model = new ShiftAssignModel();
	    model.setShiftId(selectedShiftId);
	    List<String> officerIds = new ArrayList<>();
	    officers.forEach((officer) -> {
		officerIds.add(officer.getId());
	    });
	    model.setOfficerId(officerIds);
	    coreService.assignShift(model);
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Assign Shift Success"));
	    clear();
	} catch (IOException ex) {
	    LOG.error("Error Parsing Json : " + ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Assign Shift Failed"));
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
	}
    }

    public void removeOfficer(SecurityOfficerResponseModel officerResponseModel) {
	try {
	    coreService.removeOfficerShift(officerResponseModel.getId(), selectedShiftId);
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Remove Officer Success"));
	    findOfficer(selectedShiftId);
	} catch (IOException ex) {
	    LOG.error("Error Parsing Json : " + ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Load Shift Officer Failed"));
	} catch (EndpointException ex) {
	    LOG.error(ex.getMessage());
	    facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ex.getMessage()));
	}
    }

    public ShiftResponseModel getRegistrationModel() {
	return registrationModel;
    }

    public void setRegistrationModel(ShiftResponseModel registrationModel) {
	this.registrationModel = registrationModel;
    }

    public ShiftResponseModel getEditModel() {
	return editModel;
    }

    public void setEditModel(ShiftResponseModel editModel) {
	this.editModel = editModel;
    }

    public List<BranchResponseModel> getBranches() {
	return branches;
    }

    public void setBranches(List<BranchResponseModel> branches) {
	this.branches = branches;
    }

    public BranchResponseModel getBranchDetailModel() {
	return branchDetailModel;
    }

    public void setBranchDetailModel(BranchResponseModel branchDetailModel) {
	this.branchDetailModel = branchDetailModel;
    }

    public List<SecurityOfficerResponseModel> getOfficers() {
	return officers;
    }

    public void setOfficers(List<SecurityOfficerResponseModel> officers) {
	this.officers = officers;
    }

    public Date getToday() {
	return today;
    }

    public List<OfficerTypeModel> getOfficerType() {
	return officerType;
    }

    public void setOfficerType(List<OfficerTypeModel> officerType) {
	this.officerType = officerType;
    }

    public String getSelectedShiftId() {
	return selectedShiftId;
    }

    public void setSelectedShiftId(String selectedShiftId) {
	this.selectedShiftId = selectedShiftId;
    }

    public ShiftOfficerTable getShiftOfficerTable() {
	return shiftOfficerTable;
    }

    public void setShiftOfficerTable(ShiftOfficerTable shiftOfficerTable) {
	this.shiftOfficerTable = shiftOfficerTable;
    }

    public Date getStartDate() {
	return startDate;
    }

    public void setStartDate(Date startDate) {
	this.startDate = startDate;
    }

    public Date getEndDate() {
	return endDate;
    }

    public void setEndDate(Date endDate) {
	this.endDate = endDate;
    }
}
