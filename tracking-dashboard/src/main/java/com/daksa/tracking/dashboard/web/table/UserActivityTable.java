package com.daksa.tracking.dashboard.web.table;

import com.daksa.tracking.dashboard.portal.domain.UserActivity;
import com.daksa.tracking.dashboard.portal.repository.UserActivityRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class UserActivityTable extends LazyDataModel<UserActivity> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private UserActivityRepository userActivityRepository;
    @Inject
    private EntityManager entityManager;

    private Date startDate;
    private Date endDate;
    private String name;

    @Override
    public Object getRowKey(UserActivity object) {
        return object.getId();
    }

    @Override
    public UserActivity getRowData(String rowKey) {
        UserActivity user = entityManager.find(UserActivity.class, rowKey);
        return user;
    }

    @Override
    public List<UserActivity> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if (startDate != null && endDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            filters.put("startActivityTimestamp", sdf.format(startDate));
            filters.put("endActivityTimestamp", sdf.format(endDate));
        }
        if (!Strings.isNullOrEmpty(name)) {
            filters.put("userFullName", name);
        }
        if (StringUtils.isEmpty(sortField)) {
            sortField = "activityTimestamp";
            sortOrder = SortOrder.DESCENDING;
        }

        TableParam.SortOrder tableSort;
        if (sortOrder.equals(SortOrder.ASCENDING)) {
            tableSort = TableParam.SortOrder.ASCENDING;
        } else {
            tableSort = TableParam.SortOrder.DESCENDING;
        }

        TableResult<UserActivity> result = userActivityRepository.findByFilters(new TableParam(pageSize, first, sortField, tableSort, filters));
        setRowCount(result.getRowCount().intValue());
        return result.getData();
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setName(String name) {
        this.name = name;
    }
}
