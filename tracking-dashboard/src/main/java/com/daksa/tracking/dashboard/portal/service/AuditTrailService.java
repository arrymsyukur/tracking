package com.daksa.tracking.dashboard.portal.service;


import com.daksa.tracking.dashboard.portal.domain.ChangeLog;
import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import com.daksa.tracking.dashboard.portal.domain.UserActivity;
import com.daksa.tracking.dashboard.portal.model.ChangeLogModel;
import com.daksa.tracking.dashboard.portal.model.UserActivityEvent;
import com.daksa.tracking.dashboard.portal.repository.ChangeLogRepository;
import com.daksa.tracking.dashboard.portal.repository.UserActivityRepository;
import java.util.Date;
import java.util.Objects;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@Dependent
public class AuditTrailService {

    private static final Logger LOG = LoggerFactory.getLogger(AuditTrailService.class);

    @Inject
    private UserActivityRepository userActivityRepository;
    @Inject
    private ChangeLogRepository changeLogRepository;

    private SsoSession ssoSession;

    @Transactional
    public void createAuditTrail(@Observes UserActivityEvent event) {
        LOG.info("createAuditTrail");
        SsoSession session = ssoSession;
        if (session == null) {
            LOG.debug("ssoSession is NULL, get from event");
            session = event.getSsoSession();
        } else {
            LOG.debug("ssoSession (userId: {})", session.getSsoUserId());
        }

        if (session != null) {
            UserActivity userActivity = new UserActivity(
                    session.getSsoUserId(),
                    session.getFullName(),
                    event.getActivityType(),
                    new Date());
            userActivity.setReferenceName(event.getReferenceName());
            userActivity.setReferenceValue(event.getReferenceValue());
            userActivityRepository.store(userActivity);
            if (event.getChangeLogs() != null) {
                for (ChangeLogModel<?> changeLogModel : event.getChangeLogs()) {
                    if (!Objects.equals(changeLogModel.getOldValue(), changeLogModel.getNewValue())) {
                        ChangeLog changeLog = new ChangeLog(userActivity,
                                changeLogModel.getLabel(),
                                changeLogModel.getOldValue(),
                                changeLogModel.getNewValue());
                        changeLogRepository.store(changeLog);
                    }
                }
            }
        }
    }
}
