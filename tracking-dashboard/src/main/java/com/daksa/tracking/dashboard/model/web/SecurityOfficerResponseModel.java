package com.daksa.tracking.dashboard.model.web;

import com.daksa.tracking.webutil.json.JsonDateAdapter;
import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public class SecurityOfficerResponseModel {

    private String id;
    private String name;
    private String officerTypeId;
    @XmlJavaTypeAdapter(JsonDateAdapter.class)
    private Date birthDate;
    private BigDecimal salary;
    private String username;
    private String email;
    private String status;

    public SecurityOfficerResponseModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOfficerTypeId() {
        return officerTypeId;
    }

    public void setOfficerTypeId(String officerTypeId) {
        this.officerTypeId = officerTypeId;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
