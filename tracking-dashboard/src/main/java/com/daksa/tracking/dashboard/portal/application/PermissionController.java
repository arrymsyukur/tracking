package com.daksa.tracking.dashboard.portal.application;

import com.daksa.tracking.dashboard.portal.domain.Host;
import com.daksa.tracking.dashboard.portal.domain.UriMatchingStrategy;
import com.daksa.tracking.dashboard.portal.domain.UserPermission;
import com.daksa.tracking.dashboard.portal.repository.ConfigRepository;
import com.daksa.tracking.dashboard.portal.repository.HostRepository;
import com.daksa.tracking.dashboard.portal.service.UserPermissionService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Named
@ViewScoped
public class PermissionController implements Serializable {

    private static final long serialVersionUID = 1L;
    private final static Logger LOG = LoggerFactory.getLogger(PermissionController.class);

    @Inject
    private FacesContext facesContext;
    @Inject
    private RequestContext requestContext;
    @Inject
    private UserPermissionService userPermissionService;
    @Inject
    private HostRepository hostRepository;
    @Inject
    private ConfigRepository configRepository;

    private UriMatchingStrategy[] uriMatchingStrategyList;
    private List<Host> hostList;
    private UserPermission selectedPermission;
    private String permissioId;
    private String permissionName;
    private String uriPattern;
    private UriMatchingStrategy uriMatchingStrategy;
    private String menu;
    private Host host;

    @PostConstruct
    public void init() {
        uriMatchingStrategyList = UriMatchingStrategy.values();
        hostList = hostRepository.findAll();
    }

    public void updatePermission() {
        try {
            boolean valid = true;
            if (host != null) {
                selectedPermission.setHost(host);
                if (host.getId().equals(configRepository.getHostId())) {
                    facesContext.addMessage("permissionForm:host", new FacesMessage(
                            FacesMessage.SEVERITY_WARN, "Warning", "Cannot update permission with host '" + host.getName() + "'"));
                    valid = false;
                }
            }

            if (valid) {
                userPermissionService.updatePermission(selectedPermission);
                resetUpdatePermission();
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Permission has been updated"));
            }
        } catch (Exception e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Update Permission", e.getMessage()));
        }
    }

    public void deletePermission(UserPermission role) {
        try {
            userPermissionService.deletePermission(role);
            resetUpdatePermission();
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Permission has been deleted"));
        } catch (Exception e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Delete Permission", e.getMessage()));
        }
    }

    public void resetUpdatePermission() {
        requestContext.reset("permissionForm");
        selectedPermission = null;

    }

    public void createPermission() {
        try {
            boolean valid = true;
            if (host != null && host.getId().equals(configRepository.getHostId())) {
                facesContext.addMessage("permissionForm:host", new FacesMessage(
                        FacesMessage.SEVERITY_WARN, "Warning", "Cannot create permission with host '" + host.getName() + "'"));
                valid = false;
            }

            if (valid) {
                UserPermission userPermission = new UserPermission();
                userPermission.setId(permissioId);
                userPermission.setName(permissionName);
                userPermission.setMatchingStrategy(uriMatchingStrategy);
                userPermission.setMenu(menu);
                userPermission.setUriPattern(uriPattern);
                userPermission.setHost(host);
                userPermissionService.createPermission(userPermission);
                resetCreatePermission();
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Permission has been created"));
            }
        } catch (Exception e) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cannot Create Permission", e.getMessage()));
        }
    }

    public void resetCreatePermission() {
        //requestContext.reset("permissionForm");
        permissioId = null;
        permissionName = null;
        uriMatchingStrategy = null;
        menu = null;
        uriPattern = null;
        host = null;
    }

    public boolean isPermissionLocked(UserPermission permission) {
        return permission.getHostId().equals(configRepository.getHostId());
    }

    public UserPermission getSelectedPermission() {
        return selectedPermission;
    }

    public void setSelectedPermission(UserPermission permission) {
        this.selectedPermission = permission;
        this.host = selectedPermission.getHost();
    }

    public String getPermissioId() {
        return permissioId;
    }

    public void setPermissioId(String permissioId) {
        this.permissioId = permissioId;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public String getUriPattern() {
        return uriPattern;
    }

    public void setUriPattern(String uriPattern) {
        this.uriPattern = uriPattern;
    }

    public UriMatchingStrategy getUriMatchingStrategy() {
        return uriMatchingStrategy;
    }

    public void setUriMatchingStrategy(UriMatchingStrategy uriMatchingStrategy) {
        this.uriMatchingStrategy = uriMatchingStrategy;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public UriMatchingStrategy[] getUriMatchingStrategyList() {
        return uriMatchingStrategyList;
    }

    public String getHostId() {
        return host != null ? host.getId() : null;
    }

    public void setHostId(String hostId) {
        this.host = hostRepository.find(hostId);
    }

    public List<Host> getHostList() {
        return hostList;
    }

}
