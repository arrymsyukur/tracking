package com.daksa.tracking.dashboard.infrastructure;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class CacheManagerProducer implements Serializable {
	private static final long serialVersionUID = 1L;
    
	private static final Logger LOG = LoggerFactory.getLogger(CacheManagerProducer.class);
	private static final String DEFAULT_CACHE_NAME = "general";
	
	private CacheManager cacheManager;
	
	@PostConstruct
	public void initialize() {
		LOG.info("Initialize cache");
		cacheManager = CacheManager.newInstance(getClass().getClassLoader().getResource("ehcache.xml"));
	}
	
    @Produces
	@Dependent
	public Cache getCache() {
		String cacheName = DEFAULT_CACHE_NAME;
		return cacheManager.getCache(cacheName);
	}
    
	@Produces
	@Dependent
	public CacheManager getCacheManager() {
		return cacheManager;
	}
}
