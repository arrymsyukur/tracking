package com.daksa.tracking.dashboard.portal.model;


import com.daksa.tracking.dashboard.portal.domain.ActivityType;
import com.daksa.tracking.dashboard.portal.domain.SsoSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public class UserActivityEvent {

	private SsoSession ssoSession;
	private String referenceName;
	private String referenceValue;
	private ActivityType activityType;
	private List<ChangeLogModel<?>> changeLogs = new ArrayList<>();

	private UserActivityEvent() {

	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public List<ChangeLogModel<?>> getChangeLogs() {
		return changeLogs;
	}

	public String getReferenceName() {
		return referenceName;
	}

	public String getReferenceValue() {
		return referenceValue;
	}

	public SsoSession getSsoSession() {
		return ssoSession;
	}

	public static class Builder {

		private UserActivityEvent event;

		public Builder() {
			event = new UserActivityEvent();
		}

		public UserActivityEvent build() {
			return event;
		}

		public Builder activityType(ActivityType activityType) {
			event.activityType = activityType;
			return this;
		}

		public <T> Builder changeLog(String label, T oldValue, T newValue) {
			event.changeLogs.add(new ChangeLogModel<T>(label, oldValue, newValue));
			return this;
		}

		public Builder referenceName(String referenceName) {
			event.referenceName = referenceName;
			return this;
		}
		
		public Builder referenceValue(String referenceValue) {
			event.referenceValue = referenceValue;
			return this;
		}
		
		public Builder ssoSession(SsoSession ssoSession) {
			event.ssoSession = ssoSession;
			return this;
		}
	}

}
