package com.daksa.tracking.dashboard.infrastructure.web;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named
@RequestScoped
public class TemplateBacking implements Serializable {

	private static final long serialVersionUID = 1L;
	private int activeMenu = 0;

	@PostConstruct
	public void postConstruct() {
		if (isActiveView("/user-management")) {
			activeMenu = 0;
		} else if (isActiveView("/role-management")) {
			activeMenu = 1;
		} else if (isActiveView("/company-management")) {
			activeMenu = 2;
		}
	}

	public String getViewId() {
		return FacesContext.getCurrentInstance().getViewRoot().getViewId();
	}

	/**
	 * Check whether the current viewId is match at least one view in parameter.
	 *
	 * @param views comma separated of views, <br />
	 * eg. <code>/foo1.xhtml, /foo2/a.xhtml, /foo3.xhtml</code>
	 * @return
	 */
	public boolean isActiveView(String views) {
		String currentViewId = getViewId();
		String[] viewParts = views.split(",");
		boolean found = false;
		int i = 0;
		while (!found && i < viewParts.length) {
			found = currentViewId.startsWith(viewParts[i].trim());
			i++;
		}

		return found;
	}

	public int getActiveMenu() {
		return activeMenu;
	}

	public void setActiveMenu(int activeMenu) {
		this.activeMenu = activeMenu;
	}
}
