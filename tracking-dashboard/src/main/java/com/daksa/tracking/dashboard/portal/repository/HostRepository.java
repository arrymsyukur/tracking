package com.daksa.tracking.dashboard.portal.repository;

import com.daksa.tracking.dashboard.portal.domain.Host;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@RequestScoped
public class HostRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;

	public Host find(String hostId) {
		if (hostId != null) {
			return entityManager.find(Host.class, hostId);
		} else {
			return null;
		}
	}

	public List<Host> findAll() {
		TypedQuery<Host> query = entityManager.createNamedQuery("BO.Host.findAll", Host.class);
		return query.getResultList();
	}

	public Host findByApiKey(String apiKey) {
		TypedQuery<Host> query = entityManager.createNamedQuery("BO.Host.findByApiKey", Host.class);
		query.setParameter("apiKey", apiKey);
		query.setMaxResults(1);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
