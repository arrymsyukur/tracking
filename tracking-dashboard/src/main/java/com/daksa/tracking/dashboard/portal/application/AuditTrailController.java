package com.daksa.tracking.dashboard.portal.application;



import com.daksa.tracking.dashboard.infrastructure.web.TextHelper;
import com.daksa.tracking.dashboard.portal.domain.UserActivity;
import com.daksa.tracking.dashboard.web.table.ChangeLogTable;
import com.daksa.tracking.dashboard.web.table.UserActivityTable;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@ViewScoped
@Named
public class AuditTrailController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = LoggerFactory.getLogger(AuditTrailController.class);
	
	@Inject
	private ChangeLogTable changeLogTable;
    @Inject
    private UserActivityTable userActivityTable;
    @Inject
    private FacesContext facesContext;
    
	private UserActivity userActivity;	
    
    private Date startDate;
    private Date endDate;
    private String name;
    
    @PostConstruct
    public void init() {
        startDate = new Date();
        endDate = new Date();        
        userActivityTable.setStartDate(startDate);
        userActivityTable.setEndDate(endDate);      
        changeLogTable.setShowData(Boolean.FALSE);
    }
    
    public void search() {
        userActivityTable.setStartDate(null);
        userActivityTable.setEndDate(null);
        userActivityTable.setName(null);
        changeLogTable.setShowData(Boolean.FALSE);
        if (startDate == null || endDate == null) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", 
                    "Tanggal mulai & akhir harus diisi dengan maksimal 30 hari"));      
        } else if (endDate.before(startDate)) {
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", 
                    "Tanggal akhir lebih awal dari tanggal mulai"));      
        } else {
            DateTime dtStart = new DateTime(startDate);
            DateTime dtEnd = new DateTime(endDate);
            if (Days.daysBetween(dtStart, dtEnd).getDays() <= 30) {                
                userActivityTable.setStartDate(startDate);
                userActivityTable.setEndDate(endDate);
                if (!Strings.isNullOrEmpty(name)) {
                    userActivityTable.setName(name);
                }                
            } else {
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Pencarian data hanya dapat 30 hari"));
            }
        }        
    }
    
    public void viewChangeLog(String userActivityId) {
        changeLogTable.setShowData(Boolean.TRUE);
        changeLogTable.setUserActivityId(userActivityId);
    }
    
    public String writePrettyJson(String json) {
        if (Strings.isNullOrEmpty(json)) {
            return "-";
        }
        try {
            return TextHelper.toPrettyFormat(json);
        } catch (Exception e) {
            return json;
        }
    }		
	
	public UserActivity getUserActivity() {
		return userActivity;
	}

	public ChangeLogTable getChangeLogTable() {
		return changeLogTable;
	}	

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserActivityTable getUserActivityTable() {
        return userActivityTable;
    }

    public void setUserActivityTable(UserActivityTable userActivityTable) {
        this.userActivityTable = userActivityTable;
    }
	
}
