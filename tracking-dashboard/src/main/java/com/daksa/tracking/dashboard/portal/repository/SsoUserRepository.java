package com.daksa.tracking.dashboard.portal.repository;


import com.daksa.tracking.dashboard.portal.domain.Host;
import com.daksa.tracking.dashboard.portal.domain.SsoUser;
import com.daksa.tracking.dashboard.portal.domain.UserPermission;
import com.daksa.tracking.dashboard.portal.domain.UserRole;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import com.daksa.tracking.webutil.persistence.TableResult;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@RequestScoped
public class SsoUserRepository extends JpaRepository<SsoUser, Object> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;
    @Inject
    private ConfigRepository configRepository;

    public SsoUserRepository() {
        super(SsoUser.class);
    }

    public SsoUser findUserByUsername(String username) {
        TypedQuery<SsoUser> query = entityManager.createNamedQuery("BO.SsoUser.findByUsername", SsoUser.class)
                .setParameter("username", username)
                .setMaxResults(1);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public SsoUser findUserByEmail(String email) {
        TypedQuery<SsoUser> query = entityManager.createNamedQuery("BO.SsoUser.findByEmail", SsoUser.class)
                .setParameter("email", email)
                .setMaxResults(1);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<SsoUser> findByMenu(String menu) {
        String hostId = configRepository.getHostId();
        TypedQuery<SsoUser> query = entityManager.createNamedQuery("BO.SsoUser.findByMenuAndHost", SsoUser.class)
                .setParameter("menu", menu)
                .setParameter("hostId", hostId);
        return query.getResultList();
    }

    public List<UserRole> getUserRoles(String userId) {
        TypedQuery<UserRole> query = entityManager.createNamedQuery("BO.SsoUser.getUserRoles", UserRole.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    public List<Host> getUserHostList(String userId) {
        TypedQuery<Host> query = entityManager.createNamedQuery("BO.SsoUser.getUserHostList", Host.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    public List<UserPermission> getUserPermissions(String userId) {
        TypedQuery<UserPermission> query = entityManager.createNamedQuery("BO.SsoUser.getUserPermissions", UserPermission.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    public List<UserPermission> getUserPermissionsByHost(String userId, String hostId) {
        TypedQuery<UserPermission> query = entityManager.createNamedQuery("BO.SsoUser.getUserPermissionsByHost", UserPermission.class);
        query.setParameter("userId", userId);
        query.setParameter("hostId", hostId);
        return query.getResultList();
    }

    public SsoUser find(String userId) {
        if (userId != null && !userId.isEmpty()) {
            SsoUser user = entityManager.find(SsoUser.class, userId);
            return user;
        } else {
            return null;
        }
    }

    public TableResult<SsoUser> findByFilter(UserFilter filter, Integer limit, Integer offset) {
        StringBuilder querySelectBuilder = new StringBuilder("SELECT u FROM SsoUser u WHERE TRUE = TRUE");
        StringBuilder queryCountBuilder = new StringBuilder("SELECT COUNT(u) FROM SsoUser u WHERE TRUE = TRUE");

        if (StringUtils.isNotEmpty(filter.getEmail())) {
            querySelectBuilder.append(" AND u.email = :email");
            queryCountBuilder.append(" AND u.email = :email");
        }
        if (StringUtils.isNotEmpty(filter.getUsername())) {
            querySelectBuilder.append(" AND u.username = :username");
            queryCountBuilder.append(" AND u.username = :username");
        }
        if (StringUtils.isNotEmpty(filter.getUserId())) {
            querySelectBuilder.append(" AND u.id = :userId");
            queryCountBuilder.append(" AND u.id = :userId");
        }

        TypedQuery<SsoUser> querySelect = entityManager.createQuery(querySelectBuilder.toString(), SsoUser.class);
        TypedQuery<Long> queryCount = entityManager.createQuery(queryCountBuilder.toString(), Long.class);

        if (StringUtils.isNotEmpty(filter.getEmail())) {
            querySelect.setParameter("email", filter.getEmail());
            queryCount.setParameter("email", filter.getEmail());
        }
        if (StringUtils.isNotEmpty(filter.getUsername())) {
            querySelect.setParameter("username", filter.getUsername());
            queryCount.setParameter("username", filter.getUsername());
        }
        if (StringUtils.isNotEmpty(filter.getUserId())) {
            querySelect.setParameter("userId", filter.getUserId());
            queryCount.setParameter("userId", filter.getUserId());
        }

        if (limit != null && limit > 0) {
            querySelect.setMaxResults(limit);
        }
        if (offset != null && offset > 0) {
            querySelect.setFirstResult(offset);
        }

        List<SsoUser> resultList = querySelect.getResultList();
        Long count = queryCount.getSingleResult();

        return new TableResult<>(resultList, count);

    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
