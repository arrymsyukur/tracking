package com.daksa.tracking.dashboard.model.messaging;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
public class ActivationEmailModel {

    private String receiverEmail;
    private String username;
    private String fullname;
    private String password;

    public ActivationEmailModel() {
    }

    public ActivationEmailModel(String receiverEmail, String username, String fullname, String password) {
        this.receiverEmail = receiverEmail;
        this.username = username;
        this.fullname = fullname;
        this.password = password;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
