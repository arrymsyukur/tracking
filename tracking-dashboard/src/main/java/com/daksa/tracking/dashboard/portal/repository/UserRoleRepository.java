package com.daksa.tracking.dashboard.portal.repository;


import com.daksa.tracking.dashboard.portal.domain.UserPermission;
import com.daksa.tracking.dashboard.portal.domain.UserRole;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@RequestScoped
public class UserRoleRepository extends JpaRepository<UserRole, String> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager entityManager;

    public UserRoleRepository() {
        super(UserRole.class);
    }

    @Override
    public List<UserRole> findAll() {
        TypedQuery<UserRole> query = entityManager.createNamedQuery("BO.UserRole.findAll", UserRole.class);
        return query.getResultList();
    }

    public List<UserPermission> getRolePermissions(String roleId) {
        TypedQuery<UserPermission> query = entityManager.createNamedQuery("BO.UserRole.getRolePermissions", UserPermission.class);
        query.setParameter("roleId", roleId);
        return query.getResultList();
    }

    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

}
