package com.daksa.tracking.dashboard.portal.domain;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public enum MaritalStatus {
	SINGLE, MARRIED, DIVORCED, WIDOWED
}
