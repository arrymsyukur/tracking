package com.daksa.tracking.dashboard.portal.domain;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
public enum HostStatus {
	ACTIVE, BLOCKED
}
