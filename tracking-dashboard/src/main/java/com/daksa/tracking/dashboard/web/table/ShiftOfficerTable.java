/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.daksa.tracking.dashboard.web.table;

import com.daksa.tracking.dashboard.model.web.ShiftOfficerModel;
import com.daksa.tracking.webutil.endpoint.Endpoint;
import com.daksa.tracking.webutil.endpoint.NamedLiteral;
import com.daksa.tracking.webutil.persistence.RestDataTable;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Arry Muhammad Syukur <arry.muhammad@daksa.co.id>
 */
@Dependent
public class ShiftOfficerTable extends RestDataTable<ShiftOfficerModel> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String CORE_ENDPOINT = "core";
    private String shiftId;
    private Date startDate;
    private Date endDate;

    @Inject
    private Instance<Endpoint> endpointInstance;

    public ShiftOfficerTable() {
	super(ShiftOfficerModel.class);
    }

    @Override
    public List<ShiftOfficerModel> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
	if (StringUtils.isNotEmpty(shiftId)) {
	    filters.put("shiftId", shiftId);
	}
	if (startDate != null) {
	    filters.put("startDate", startDate);
	    if (endDate != null) {
		filters.put("endDate", endDate);
	    } else {
		filters.put("endDate", new Date());
	    }
	}

	return super.load(first, pageSize, sortField, sortOrder, filters);
    }

    @Override
    protected Endpoint getEndpoint() {
	return endpointInstance.select(new NamedLiteral(CORE_ENDPOINT)).get();
    }

    @Override
    protected String getResource() {
	return "/shiftOfficer";
    }

    @Override
    protected Map<String, Object> getAdditionalParams() {
	return new HashMap<>();
    }

    @Override
    protected Map<String, Object> getAdditionalHeaders() {
	return null;
    }

    @Override
    public Object getRowKey(ShiftOfficerModel object) {
	return object.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ShiftOfficerModel getRowData(String rowKey) {
	Map<String, Object> filters = new HashMap<>();
	filters.put("id", rowKey);
	List<ShiftOfficerModel> result = load(0, 1, null, null, filters);
	return result.get(0);
    }

    public String getShiftId() {
	return shiftId;
    }

    public void setShiftId(String shiftId) {
	this.shiftId = shiftId;
    }

    public Date getStartDate() {
	return startDate;
    }

    public void setStartDate(Date startDate) {
	this.startDate = startDate;
    }

    public Date getEndDate() {
	return endDate;
    }

    public void setEndDate(Date endDate) {
	this.endDate = endDate;
    }

}
