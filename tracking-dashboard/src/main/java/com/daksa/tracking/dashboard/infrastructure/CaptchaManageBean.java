package com.daksa.tracking.dashboard.infrastructure;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.captcha.Captcha;
import nl.captcha.backgrounds.FlatColorBackgroundProducer;
import nl.captcha.text.producer.DefaultTextProducer;
import nl.captcha.text.renderer.DefaultWordRenderer;

/**
 *
 * @author Berry
 */
@Stateless
public class CaptchaManageBean implements Serializable {

    private static final long serialVersionUID = 1L;

	private Captcha captcha;
    
    /**
	 * Generates a captcha token and stores it in the session.
	 *
	 * @param session where to store the captcha.
	 */
	public void generateToken(HttpSession session) {
		final char[] token = generateCaptchaToken();
		List<Color> colorsList = new ArrayList<>();
		Color qnbColor = new Color(137, 0, 80);
		colorsList.add(qnbColor);
		List<Font> fontsList = new ArrayList<>();
		fontsList.add(new Font(Font.SERIF, Font.PLAIN, 46));
		captcha = new Captcha.Builder(160, 60)                
				.addText(new DefaultTextProducer(4, token), new DefaultWordRenderer(colorsList, fontsList))
				.addBackground(new FlatColorBackgroundProducer(Color.WHITE))
				.build();
		session.setAttribute("captchaToken", captcha.getAnswer());
		markTokenUsed(session, false);
	}

	/**
	 * Marks token as used/unused for image generation.
	 *
	 * @param session where the token usage flag is possibly stored.
	 * @param used false if the token is not yet used for image generation
	 */
	public void markTokenUsed(HttpSession session, boolean used) {
		session.setAttribute("captchaTokenUsed", used);
	}

	/**
	 * Helper method, disables HTTP caching.
	 *
	 * @param resp response object to be modified
	 * @throws java.io.IOException
	 */
	public void setResponseHeaders(HttpServletResponse resp) throws IOException {
		OutputStream out = resp.getOutputStream();
		resp.setContentType("image/jpeg");
		resp.setHeader("Cache-Control", "no-cache, no-store");
		resp.setHeader("Pragma", "no-cache");
		final long time = System.currentTimeMillis();
		resp.setDateHeader("Last-Modified", time);
		resp.setDateHeader("Date", time);
		resp.setDateHeader("Expires", time);
		out.write(convert(captcha.getImage()));

	}

	private byte[] convert(BufferedImage image) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;
	}

	/**
	 * Used to retrieve previously stored captcha token from session.
	 *
	 * @param session where the token is possibly stored.
	 * @return token or null if there was none
	 */
	public String getToken(HttpSession session) {
		final Object val = session.getAttribute("captchaToken");

		return val != null ? val.toString() : null;
	}

	/**
	 * Checks if the token was used/unused for image generation.
	 *
	 * @param session where the token usage flag is possibly stored.
	 * @return true if the token was marked as unused in the session
	 */
	public boolean isTokenUsed(HttpSession session) {
		return !Boolean.FALSE.equals(session.getAttribute("captchaTokenUsed"));
	}

	private char[] generateCaptchaToken() {
		String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		char[] charArray = SALTCHARS.toCharArray();
		return charArray;
	}
}
