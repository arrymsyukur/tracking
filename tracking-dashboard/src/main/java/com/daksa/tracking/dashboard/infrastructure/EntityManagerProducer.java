package com.daksa.tracking.dashboard.infrastructure;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Ginan
 */
@ApplicationScoped
public class EntityManagerProducer {
	
	@PersistenceContext(name = "tracking_pu")
	@Produces
	private EntityManager entityManager;
    
    @Produces
	public Cache getCache(){
		return entityManager.getEntityManagerFactory().getCache();
	}
}
