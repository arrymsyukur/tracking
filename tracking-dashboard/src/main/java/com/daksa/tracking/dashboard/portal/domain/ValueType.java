package com.daksa.tracking.dashboard.portal.domain;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
public enum ValueType {
	NUMBER, STRING, BOOLEAN, DATE
}
