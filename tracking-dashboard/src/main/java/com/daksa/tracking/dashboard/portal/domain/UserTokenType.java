package com.daksa.tracking.dashboard.portal.domain;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
public enum UserTokenType {
	ACTIVATION, RESET_PASSWORD
}
