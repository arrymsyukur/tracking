package com.daksa.tracking.dashboard.portal.repository;


import com.daksa.tracking.dashboard.portal.domain.ChangeLog;
import com.daksa.tracking.webutil.persistence.DataTable;
import com.daksa.tracking.webutil.persistence.FilterItem;
import com.daksa.tracking.webutil.persistence.JpaRepository;
import com.daksa.tracking.webutil.persistence.TableParam;
import com.daksa.tracking.webutil.persistence.TableResult;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjarpramadita@gmail.com>
 */
@Dependent
public class ChangeLogRepository extends JpaRepository<ChangeLog, String>  {

	@Inject
	private EntityManager entityManager;
    private DataTable<ChangeLog> dataTable;
    
    @PostConstruct
	public void init() {
		dataTable = new DataTable<>(entityManager, ChangeLog.class);
	}

	public ChangeLogRepository() {
		super(ChangeLog.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}
    
    public TableResult<ChangeLog> findByFilters(TableParam param) {            
        return dataTable.load(param, (String fieldName, Object valueStr) -> {
			FilterItem result = null;
			switch (fieldName) {				
                case "userActivityId":
                    result = FilterItem.stringMatchFilter(fieldName, valueStr.toString());
					break;    
			}
			return result;
		});
    }        
}
