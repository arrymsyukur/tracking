package com.daksa.tracking.dashboard.portal.service;


import com.daksa.tracking.dashboard.portal.domain.UserRole;
import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Stateless
public class UserRoleService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private EntityManager entityManager;
	
	public void updateRole(UserRole role) {
		entityManager.merge(role);
	}
	
	public void createRole(UserRole role) {
		entityManager.persist(role);
	}
	
	public void deleteRole(UserRole role) {
		entityManager.remove(entityManager.merge(role));
	}
}
