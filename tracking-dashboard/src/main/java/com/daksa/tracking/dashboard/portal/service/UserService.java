package com.daksa.tracking.dashboard.portal.service;


import com.daksa.tracking.dashboard.model.messaging.ActivationEmailModel;
import com.daksa.tracking.dashboard.model.messaging.EmailType;
import com.daksa.tracking.dashboard.model.messaging.ForgotPasswordEmail;
import com.daksa.tracking.dashboard.model.messaging.SendEmailParam;
import com.daksa.tracking.dashboard.portal.domain.SsoUser;
import com.daksa.tracking.dashboard.portal.domain.UserStatus;
import com.daksa.tracking.dashboard.portal.domain.UserToken;
import com.daksa.tracking.dashboard.portal.domain.UserTokenType;
import com.daksa.tracking.dashboard.portal.exception.ActivationCodeNotValidException;
import com.daksa.tracking.dashboard.portal.exception.EmailNotFoundException;
import com.daksa.tracking.dashboard.portal.exception.EntryCodeNotValidException;
import com.daksa.tracking.dashboard.portal.model.UserTokenResult;
import com.daksa.tracking.dashboard.portal.repository.ConfigRepository;
import com.daksa.tracking.dashboard.portal.repository.SsoUserRepository;
import com.daksa.tracking.webutil.endpoint.Endpoint;
import com.daksa.tracking.webutil.endpoint.EndpointException;
import com.daksa.tracking.webutil.endpoint.EndpointRequestBuilder;
import com.daksa.tracking.webutil.endpoint.NamedLiteral;
import com.daksa.tracking.webutil.json.Json;
import com.daksa.tracking.webutil.json.JsonResponseReader;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ginan <ginanjar.pramadita@daksa.co.id>
 */
@Stateless
public class UserService implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private static final int ACTIVATION_CODE_LENGTH = 6;
    private static final char[] CODES = new char[10 + 26 + 26]; // numeric + alpha (smallcaps) + alpha (uppercaps)

    static {
        int j = 0;
        for (char i = '0'; i <= '9'; i++) {
            CODES[j++] = i;
        }
        for (char i = 'A'; i <= 'Z'; i++) {
            CODES[j++] = i;
        }
        for (char i = 'a'; i <= 'z'; i++) {
            CODES[j++] = i;
        }
    }

    @Inject
    private SsoUserRepository ssoUserRepository;
    @Inject
    private ConfigRepository configRepository;
    @Inject
    private EntityManager entityManager;
    private static final String SWITCHING_ENDPOINT = "switching";

    @Inject
    private Instance<Endpoint> endpointInstance;
    private Endpoint endpoint;

    @PostConstruct
    public void init() {
        endpoint = endpointInstance.select(new NamedLiteral(SWITCHING_ENDPOINT)).get();
    }

    public void registerUser(SsoUser user) throws EndpointException, JsonProcessingException {
        String temporaryPassword = generateEntryCode();
        user.setPassword(temporaryPassword);
        user.setStatus(UserStatus.ACTIVE);
        entityManager.persist(user);
        ObjectMapper mapper = new ObjectMapper();

        //Send Email
        ActivationEmailModel model = new ActivationEmailModel(user.getEmail(), user.getUsername(), user.getFullName(), temporaryPassword);
        SendEmailParam param = new SendEmailParam();
        param.setEmailType(EmailType.WEB_USER_ACTIVATION);
        param.setData(mapper.writeValueAsString(model));
        sendEmail(param);
    }

    public void activateUser(String email, String activationCode, String newPassword)
            throws EmailNotFoundException, ActivationCodeNotValidException {
        SsoUser user = ssoUserRepository.findUserByEmail(email);
        if (user == null) {
            throw new EmailNotFoundException();
        }
        if (user.verifyPassword(activationCode)) {
            user.setStatus(UserStatus.ACTIVE);
            user.setPassword(newPassword);
            entityManager.merge(user);
        } else {
            throw new ActivationCodeNotValidException();
        }
    }

    public SsoUser updateUser(SsoUser user) {
        return entityManager.merge(user);
    }

    public UserTokenResult resetPassword(String email, Date createdTimestamp) throws EmailNotFoundException {
        SsoUser user = ssoUserRepository.findUserByEmail(email);
        if (user == null) {
            throw new EmailNotFoundException();
        }

        Calendar expiryCal = Calendar.getInstance();
        expiryCal.add(Calendar.MINUTE, configRepository.getUserTokenExpiryMinutes());
        Date expiryTimestamp = expiryCal.getTime();

        UserTokenResult userTokenResult = createUserToken(user,
                createdTimestamp,
                expiryTimestamp,
                UserTokenType.RESET_PASSWORD);

//		String company = appResource.getString("company");
//		String link = configRepository.getBaseUrl() 
//				+ "auth/reset-password.xhtml?token=" 
//				+ userTokenResult.getUserToken().getId();
//		String subject = company + " - Password Reset";
//		String content = "<p>"
//				+ "  Hello, this e-mail has been sent to you so you can reset your login account password."
//				+ "  Please click the link below to complete the password reset process."
//				+ "</p>"
//				+ "<p>"
//				+ "	  <a href=\"" + link + "\">" + link + "</a>"
//				+ "</p>"
//				+ "<p>"
//				+ "    Username: <b>" + user.getUsername()+ "</b><br />"
//				+ "    Entry Code: <b>" + userTokenResult.getEntryCode() + "</b>"
//				+ "</p>"
//				+ "<p>"
//				+ "  Received this message by mistake?<br />"
//				+ "  You may have received this email in error because another customer entered this email address by mistake.<br />"
//				+ "  If you received this message by mistake, please delete this email."
//				+ "</p>";
//		emailSenderService.sendEmail(email, subject, content);
        return userTokenResult;
    }

    public void completeResetPassword(UserToken userToken, String entryCode, Date now, String password)
            throws EntryCodeNotValidException {
        if (userToken.getTokenType() == UserTokenType.RESET_PASSWORD) {
            verifyEntryCode(userToken, entryCode, now);
            SsoUser user = userToken.getUser();
            user.setPassword(password);
            entityManager.merge(user);
        } else {
            throw new EntryCodeNotValidException();
        }
    }

    private UserTokenResult createUserToken(SsoUser user, Date createdTimestamp, Date expiryTimestamp,
            UserTokenType tokenType) {
        String entryCode = generateEntryCode();
        UserToken userToken = new UserToken(user, createdTimestamp, expiryTimestamp, entryCode, tokenType);
        entityManager.persist(userToken);

        UserTokenResult result = new UserTokenResult();
        result.setEntryCode(entryCode);
        result.setUserToken(userToken);
        return result;
    }

    private void verifyEntryCode(UserToken userToken, String entryCode, Date now) throws EntryCodeNotValidException {
        if (userToken.isAvailable() && !userToken.isExpired(now)
                && userToken.isEntryCodeMatch(entryCode)) {
            userToken.invalidate();
            entityManager.merge(userToken);
        } else {
            throw new EntryCodeNotValidException();
        }
    }

    private static String generateEntryCode() {
        Random random = new Random();
        StringBuilder activationCodeBuilder = new StringBuilder(ACTIVATION_CODE_LENGTH);
        int n = CODES.length;
        for (int i = 0; i < ACTIVATION_CODE_LENGTH; i++) {
            char code = CODES[random.nextInt(n)];
            activationCodeBuilder.append(code);
        }
        return activationCodeBuilder.toString();
    }

    private void sendEmail(SendEmailParam param) throws EndpointException {
        try {
            String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
                    .resource("/web/user/email")
                    .method("POST")
                    .content(Json.getWriter().writeValueAsBytes(param), MediaType.APPLICATION_JSON)
                    .build())).getContentAsString();

        } catch (EndpointException e) {
            LOG.error(e.getMessage(), e);
            throw new EndpointException(e.getResponseCode(), e.getMessage());
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            throw new EndpointException("99", "Failed parse json");
        }
    }

    private void sendPasswordEmail(SendEmailParam param) throws EndpointException {
        try {
            String response = new JsonResponseReader(endpoint.send(new EndpointRequestBuilder()
                    .resource("/web/forgotPassword")
                    .method("POST")
                    .content(Json.getWriter().writeValueAsBytes(param), MediaType.APPLICATION_JSON)
                    .build())).getContentAsString();

        } catch (EndpointException e) {
            LOG.error(e.getMessage(), e);
            throw new EndpointException(e.getResponseCode(), e.getMessage());
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            throw new EndpointException("99", "Failed parse json");
        }
    }

    public void forgotPassword(String email) throws JsonProcessingException, EndpointException {
        SsoUser ssoUser = ssoUserRepository.findUserByEmail(email);
        String password = generateEntryCode();
        ssoUser.setPassword(password);
        ssoUserRepository.store(ssoUser);

        //Send Email
        ForgotPasswordEmail fpe = new ForgotPasswordEmail(email, password);
        SendEmailParam param = new SendEmailParam();
        param.setEmailType(EmailType.FORGOT_PASSWORD);
        param.setData(Json.getWriter().writeValueAsString(fpe));
        sendPasswordEmail(param);

    }
}
