/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  arrym
 * Created: Mar 21, 2019
 */

INSERT INTO portal.user_permission (id, host_id, matching_strategy, name, menu, uri_pattern) VALUES 
 ('ADD_BRANCH', 'tracking-dashboard', 'REGEX', 'BRANCH MANAGEMENT', 'ADD_BRANCH', '(/)|(/index[.]xhtml)|(/branch-management/add-branch.xhtml)'),
 ('VIEW_BRANCH', 'tracking-dashboard', 'REGEX', 'BRANCH MANAGEMENT', 'VIEW_BRANCH', '(/)|(/index[.]xhtml)|(/branch-management/view-branch.xhtml)'),
 ('EDIT_BRANCH', 'tracking-dashboard', 'REGEX', 'BRANCH MANAGEMENT', 'EDIT_BRANCH', '(/)|(/index[.]xhtml)|(/branch-management/edit-branch.xhtml)'),

 ('ADD_SECURITY_OFFICER', 'tracking-dashboard', 'REGEX', 'SECURITY OFFICER MANAGEMENT', 'ADD_SECURITY_OFFICER', '(/)|(/index[.]xhtml)|(/officer-management/add-officer.xhtml)'),
 ('VIEW_SECURITY_OFFICER', 'tracking-dashboard', 'REGEX', 'SECURITY OFFICER MANAGEMENT', 'VIEW_SECURITY_OFFICER', '(/)|(/index[.]xhtml)|(/officer-management/view-officer.xhtml)'),
 ('EDIT_SECURITY_OFFICER', 'tracking-dashboard', 'REGEX', 'SECURITY OFFICER MANAGEMENT', 'EDIT_SECURITY_OFFICER', '(/)|(/index[.]xhtml)|(/officer-management/edit-officer.xhtml)'),
 ('ASSIGN_SHIFT', 'tracking-dashboard', 'REGEX', 'SECURITY OFFICER MANAGEMENT', 'ASSIGN_SHIFT', '(/)|(/index[.]xhtml)|(/officer-management/assign-shift.xhtml)'),
   
 ('ADD_SHIFT', 'tracking-dashboard', 'REGEX', 'SHIFT MANAGEMENT', 'ADD_SHIFT', '(/)|(/index[.]xhtml)|(/shift-management/add-shift.xhtml)'),
 ('VIEW_SHIFT', 'tracking-dashboard', 'REGEX', 'SHIFT MANAGEMENT', 'VIEW_SHIFT', '(/)|(/index[.]xhtml)|(/shift-management/view-shift.xhtml)'),
 ('EDIT_SHIFT', 'tracking-dashboard', 'REGEX', 'SHIFT MANAGEMENT', 'EDIT_SHIFT', '(/)|(/index[.]xhtml)|(/shift-management/edit-shift.xhtml)');
   
